import 'dart:ui' as prefix0;

import 'package:flutter/material.dart';

// const kPrimaryColor = const Color.fromRGBO(255, 77, 77, 1); //red pink

// Color.fromRGBO(73, 187, 255, 1) - light blue
const kPrimaryColor = const Color.fromRGBO(91, 176, 91, 1);
const kPrimaryLight = const Color.fromRGBO(141, 227, 137, 1);
const kPrimaryDark = const Color.fromRGBO(39, 128, 47, 1);


const kSecondaryColor = const Color.fromRGBO(33, 33, 33, 1);
const kSecondaryLight = const Color.fromRGBO(72, 72, 72, 1);
const kSecondaryDark = const Color.fromRGBO(0, 0, 0, 1);

ThemeData buildThemeData() {
  final baseTheme = ThemeData.light();
  return baseTheme.copyWith(
      appBarTheme: AppBarTheme(
        color: kPrimaryColor,
      ),
      accentColor: kPrimaryColor,
      
      accentTextTheme: TextTheme().copyWith(
        title: TextStyle(
          color: Colors.white,
        ),
        headline: TextStyle(color: Colors.white, fontSize: 16),
        subtitle:
            TextStyle(color: Colors.white, fontWeight: prefix0.FontWeight.w400),
        display1:
            TextStyle(color: Color.fromRGBO(136, 136, 136, 1), fontSize: 10),
        display2:
            TextStyle(color: Color.fromRGBO(136, 136, 136, 1), fontSize: 10),
        display3:
            TextStyle(color: Color.fromRGBO(178, 178, 178, 1), fontSize: 12),
        display4: TextStyle(
            color: Colors.white, fontSize: 13, fontWeight: FontWeight.w600),
      ),
      primaryColor: kPrimaryColor,
      primaryColorLight: kPrimaryLight,
      primaryColorDark: kPrimaryDark,//Colors.white,
      scaffoldBackgroundColor: Colors.white,
      splashColor: kPrimaryColor,
      buttonColor: kPrimaryColor,
      textTheme: TextTheme(
        title: TextStyle(
          color: Colors.black,
        ),
        headline: TextStyle(color: Colors.black, fontSize: 16),
        subtitle:
            TextStyle(color: Colors.black, fontWeight: prefix0.FontWeight.w400),
        display1:
            TextStyle(color: Color.fromRGBO(136, 136, 136, 1), fontSize: 10),
        display2:
            TextStyle(color: Color.fromRGBO(136, 136, 136, 1), fontSize: 10),
        display3:
            TextStyle(color: Color.fromRGBO(178, 178, 178, 1), fontSize: 12),
        display4: TextStyle(
            color: Colors.white, fontSize: 13, fontWeight: FontWeight.w600),
      ));
}
