import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_auth_buttons/flutter_auth_buttons.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:gl_zoo/services/login_services.dart';
import 'package:gl_zoo/services/user_services.dart';
import 'package:gl_zoo/models/fb_profile.dart';

import 'package:geolocator/geolocator.dart';
import 'package:location/location.dart' as loc;
import 'package:permission_handler/permission_handler.dart';
import 'package:app_settings/app_settings.dart';

String lorem = "Lorem Ipsum dolor si amet. Dolor sit amet lorem Amet dolor";

String baseUrl = "http://www.glzoobackend.xyz/backend-gl-zoo/public/api";

int detailAnimaId = 1;

int detailActivityId = 0;

int detailFeedingId = 0;

int detailPromoId = 0;

int detailSpotLightId = 0;

int userId = 1;

bool isLogin = false;
String welcomeName = "Guess";
String urlProfile;

String wifiPass = "";

String lat = "";
String long = "";

bool isNewUser = true;

ImageProvider currentGaleryImage;

String galeryImageCaption;

String currentMenu = "Home";

String getBaseUrl() {
  return baseUrl;
}

/// Global Function to return Screen Height
double mh(BuildContext context) {
  return MediaQuery.of(context).size.height;
}

/// Global Function to return Screen Width
double mw(BuildContext context) {
  return MediaQuery.of(context).size.width;
}

Widget isLoading() {
  return Center(
    child: CircularProgressIndicator(),
  );
}

/// get color by Style Name : `primary`,`unprime`,`secondary`,`active`,`warnig`,`danger`,`disabled`.
Color myColor([String color = "default"]) {
  Color returnedColor;
  switch (color) {
    case "primary":
      // red pink
      // returnedColor = Color.fromRGBO(255, 77, 77, 1);

      // light blue
      // returnedColor = Color.fromRGBO(73, 187, 255, 1);

      // blue navy
      returnedColor = Color.fromRGBO(60, 90, 153, 1);
      break;
    case "mimosa":
      // for current bid
      returnedColor = Color.fromRGBO(239, 192, 80, 1);
      break;
    case "danger":
      returnedColor = Colors.red;
      break;
    case "warning":
      returnedColor = Colors.deepOrange;
      break;
    case "secondary":
      returnedColor = Colors.deepOrange;
      break;
    case "active":
      returnedColor = Colors.black;
      break;
    case "unprime":
      returnedColor = Color.fromRGBO(136, 136, 136, 1);
      break;
    case "disabled":
      returnedColor = Color.fromRGBO(178, 178, 178, 1);
      break;
    case "light":
      returnedColor = Colors.white;
      break;
    case "dark":
      returnedColor = Colors.grey[700];
      break;
    case "unprime2":
      returnedColor = Color.fromRGBO(0, 0, 32, 1);
      break;
    default:
      returnedColor = Colors.black;
      break;
  }
  return returnedColor;
}

/// get weight by Style Name : `T`/`XL`/`L`/`N`/`M`/`SB`/`B`/`XB`/`TB`
///
/// `T` : Thin.
/// `XL` : Extra Light.
/// `L`  : Light.
/// `N`  : Normal.
/// `M`  : Medium.
/// `SB` : Semi-Bold.
/// `B`  : Bold.
/// `XB` : Extra Bold.
/// `TB` : True Bold/ Real Black.
FontWeight myFontWeight([String weight = 'N']) {
  FontWeight returnedFW;
  switch (weight) {
    case 'T':
      returnedFW = FontWeight.w100;
      break;
    case 'XL':
      returnedFW = FontWeight.w200;
      break;
    case 'L':
      returnedFW = FontWeight.w300;
      break;
    case 'N':
      returnedFW = FontWeight.w400;
      break;
    case 'M':
      returnedFW = FontWeight.w500;
      break;
    case 'SB':
      returnedFW = FontWeight.w600;
      break;
    case 'B':
      returnedFW = FontWeight.w700;
      break;
    case 'XB':
      returnedFW = FontWeight.w800;
      break;
    case 'TB':
      returnedFW = FontWeight.w900;
      break;
  }
  return returnedFW;
}

/// Global Function to return Text with [color] as in [myColor] and [weight] as in [myFontWeight]
Text myText(
    {@required String text,
    String color = "default",
    double size = 14,
    String weight = "N",
    TextDecoration decoration = TextDecoration.none,
    TextAlign align = TextAlign.start,
    TextOverflow textOverflow = TextOverflow.visible,
    double letterSpacing = 0,
    double wordSpacing = 0,
    bool withContext = false,
    BuildContext context}) {
  if (text == null || text == "") text = "-";
  Color accentColor;
  switch (color) {
    case "accentText":
      accentColor = Theme.of(context).primaryColorLight;
      break;
    case "accentTextDark":
      accentColor = Theme.of(context).primaryColorDark;
      break;
    default:
  }
  return Text(
    text,
    softWrap: true,
    textAlign: align,
    overflow: textOverflow,
    style: TextStyle(
      wordSpacing: wordSpacing,
      letterSpacing: letterSpacing,
      decoration: decoration,
      decorationColor: myColor(color),
      color: (accentColor != null ? accentColor : myColor(color) ),
      fontSize: size,
      fontWeight: myFontWeight(weight),
    ),
  );
}

Future<bool> confirmDialog(String content, context,
    [String title = "Perhatian"]) {
  return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(title),
            content: myText(text: content),
            actions: <Widget>[
              FlatButton(
                child: Text("Ya",
                    style: TextStyle(color: Colors.red[300], fontSize: 18)),
                onPressed: () {
                  Navigator.of(context).pop(true);
                },
              ),
              FlatButton(
                child: Text(
                  "Tidak",
                  style: TextStyle(color: Colors.blue[600], fontSize: 18),
                ),
                onPressed: () {
                  Navigator.of(context).pop(false);
                },
              ),
              VerticalDivider(
                width: 10,
                color: Colors.white,
              ),
            ],
          );
        },
      ) ??
      false;
}

/// Global Function to return Alert Dialog
Future<bool> showDialogs(String content, BuildContext context,
    {String title = "Perhatian",
    String route = "",
    bool isDouble = false,
    Function openSetting,
    String text = "Tutup"}) {
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text(title, style: TextStyle(color: Colors.black)),
        content: myText(size: 15, text: content, letterSpacing: 1, wordSpacing: 1.5),
        actions: <Widget>[
          FlatButton(
            child: Text(text),
            onPressed: () {
              if (text != "Tutup") {
                Navigator.pop(context);
                if(openSetting is Function){
                  openSetting();
                }
              } else {
                if (route == "") {
                  Navigator.of(context).pop(true);
                } else {
                  Navigator.popUntil(context, ModalRoute.withName(route));
                  Navigator.of(context).pop();
                  Navigator.pushNamed(context, '$route');
                }

                if (isDouble) {
                  Navigator.of(context).pop();
                }
              }
            },
          ),
        ],
      );
    },
  );
}

Widget appBar(BuildContext context, String title){
  return AppBar(
    // actions: <Widget>[
    //   Icon(
    //     Icons.search,
    //     size: 35,
    //   ),
    //   SizedBox(
    //     width: 10,
    //   ),
    // ],
    title: myText(text: title, color: "light", size: 20),
    leading: IconButton(
        icon: Icon(Icons.arrow_back),
        onPressed: () {
          Navigator.pop(context);
        }),
  );
}

Geolocator geolocator = Geolocator();
Future<bool> checkPermission(context) async {
    bool status = false;
    
    GeolocationStatus geolocationStatus =
        await geolocator.checkGeolocationPermissionStatus();
    print(geolocationStatus);
    if (geolocationStatus == GeolocationStatus.disabled || geolocationStatus == GeolocationStatus.unknown) {
      //status = false;
      showDialogs("Please Enable Your GPS", context, 
        openSetting: AppSettings.openLocationSettings, 
        text: "Open Setting",);
    } else if (geolocationStatus == GeolocationStatus.denied) {
      //status = false;
      showDialogs("Please Allow Location Permission", context,
        openSetting: PermissionHandler().openAppSettings, 
        text: "Open Setting");
    }else{
      bool isServiceEnable = await geolocator.isLocationServiceEnabled();
      bool serviceStatus = true;
      // await location.serviceEnabled().catchError((onError){
      //   print(onError.toString());
      // });
      if (!serviceStatus) {
          showDialogs("Please Enable Your GPS", context, 
            openSetting: AppSettings.openLocationSettings, 
            text: "Open Setting",);
      }
      if (!isServiceEnable) {
        //status = false;
        showDialogs(
            "Google Service Location is Unavailable, Please Restart Your Phone",
            context);
      }else{
        status = true;
      }
    }
    return status;
  }

Map<String, double> currentLocation = new Map();
loc.LocationData currentLocation2;
var location = loc.Location();

Future<void> getThatLocation() async {
  try {
    currentLocation2 = await location.getLocation();
  } catch (e) {
    if (e.code == 'PERMISSION_DENIED') {
      //error = 'Permission denied';
    } 
    currentLocation2 = null;
  }
}

Future<void> getThisLocation() async {
  currentLocation['latitude'] = 0.0;
  currentLocation['longitude'] = 0.0;

  try {
    await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .timeout(new Duration(seconds: 10))
        .then((gps) {
      currentLocation['latitude'] = gps.latitude;
      currentLocation['longitude'] = gps.longitude;
    });
  } catch (e) {}
}

void openMap(context) async {
  //showDialogs("Zoo Map is Under Development, please be patient", context);
  // await getThisLocation();
  bool gpsOk = await checkPermission(context);
  
  if(gpsOk){
    if(!isLogin){
      loginDialogs(context).then((login){
        if(login != null && login){
          print("Login Success");
          isLogin = true;
          openMapWithLogin(context);
        }else{
          showDialogs("Login Failed", context);
          print("Login Fail");
        }
      });
    }else{
      openMapWithLogin(context);
    }
  }
}

void openMapWithLogin(context){
  final map = "/map";
  final promo = "/reminder";
  final home = "/";
  bool isNewRouteSameAsCurrent = false;

  bool sibling = false;
  bool notMap = false;
  if(currentMenu != "Map"){
    print("is not Map");
    notMap = true;
  }
  currentMenu = "Map";
  isNewRouteSameAsCurrent = false;
  if(Navigator.canPop(context)){
    print("can Pop");
    Navigator.popUntil(context, (route) {
      if (route.settings.name == map) {
        print("Name is Map");
        isNewRouteSameAsCurrent = true;
      }
      if(route.settings.name == home || route.settings.name == promo){
        sibling = true;
      }
      return isNewRouteSameAsCurrent || sibling;
    });
  }else{
    if(notMap){
      print("Not map indeed");
      sibling = true;
    }
  }
  
  if (sibling) {
    print("sibling");
    Navigator.pushReplacementNamed(context, "/map");
  }
}

Widget floatingAppBar(BuildContext context){
  return FloatingActionButton(
    onPressed: () => openMap(context),
    backgroundColor: Theme.of(context).primaryColor,
    child: Icon(
      Icons.location_on,
      color: currentMenu == "Map" ? Colors.white : Colors.grey[400],
      size: 40,
    ),
  );
}

Widget bottomAppBar(BuildContext context){
  final promo = "/reminder";
  final home = "/";
  final map = "/map";
  bool isNewRouteSameAsCurrent = false;
  return BottomAppBar(
          child: Container(
            color: Theme.of(context).primaryColor,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                  child: IconButton(
                    icon: Icon(
                      Icons.home,
                      color: currentMenu == "Home" ? Colors.white : Colors.grey[400],
                      size: 35,
                    ),
                    onPressed: () {
                      isNewRouteSameAsCurrent = false;
                      bool sibling = false;
                      bool notHome = false;
                      if(currentMenu != "Home"){
                        notHome = true;
                      }
                      currentMenu = "Home";
                      if(Navigator.canPop(context)){
                          Navigator.popUntil(context, (route) {
                          print("route.settings.name");
                          print(route.settings.name);
                          
                          if (route.settings.name == home) {
                            isNewRouteSameAsCurrent = true;
                          }
                          if(route.settings.name == promo || route.settings.name == map){
                            sibling = true;
                          }
                          return route.settings.name == "/" || sibling;
                        });
                      }else{
                        if(notHome){
                          sibling = true;
                        }
                      }
                      
                      if (sibling) {
                        print("sibling");
                        Navigator.pushReplacementNamed(context, "/");
                      }
                      // else if (!isNewRouteSameAsCurrent) {
                      //   print("!isNewRouteSameAsCurrent");
                      //   Navigator.popUntil(context, ModalRoute.withName('/'));
                      // }
                    },
                    
                  ),
                ),
                Expanded(
                  child: Text(""),
                ),
                Expanded(
                  child: IconButton(
                    icon: Icon(
                      Icons.local_play,
                      color: currentMenu == "Promo" ? Colors.white : Colors.grey[400],
                      size: 35,
                    ),
                    onPressed: () {
                      // if(currentMenu != "Promo"){
                      //   print("Pressed Promo");
                        isNewRouteSameAsCurrent = false;
                        // currentMenu = "Promo";
                        bool sibling = false;
                        bool notActive = false;
                        if(currentMenu != "Promo"){
                          notActive = true;
                        }
                        currentMenu = "Promo";
                        isNewRouteSameAsCurrent = false;
                        if(Navigator.canPop(context)){
                          Navigator.popUntil(context, (route) {
                            if (route.settings.name == promo) {
                              print("Name is Promo");
                              isNewRouteSameAsCurrent = true;
                            }
                            if(route.settings.name == home || route.settings.name == map){
                              sibling = true;
                            }
                            return isNewRouteSameAsCurrent || sibling;
                          });
                        }else{
                          if(notActive){
                            sibling = true;
                          }
                        }
                        
                        if (sibling) {
                          print("sibling");
                          Navigator.pushReplacementNamed(context, "/reminder");
                        }
                        // Navigator.popUntil(context, (route) {
                        //   if (route.settings.name == promo) {
                        //     print("Name is Promo");
                        //     isNewRouteSameAsCurrent = true;
                        //   }
                        //   if(route.settings.name == home || route.settings.name == map){
                        //     sibling = true;
                        //   }
                        //   return true;
                        // });

                        // if (!isNewRouteSameAsCurrent) {
                        //   if(!sibling){
                        //     Navigator.popUntil(context, ModalRoute.withName('/'));
                        //   }
                        //   Navigator.pushReplacementNamed(context, "/reminder");
                        // }
                      // }
                      
                    },
                  ),
                )
              ],
            ),
          ),
        );
}

Future<bool> loginDialogs(BuildContext context, [String menu = 'map']) {
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Container(
          color: Colors.white,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset("assets/images/need_login.png",
                width: 80,
                height: 80,
                fit: BoxFit.fill
              ),
              Text("Ups! Kamu harus login terlebih dahulu")
            ],
          ),
        ),
        actions: <Widget>[
          Container(
            padding: const EdgeInsets.only(right: 10.0),
            width: 250,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              
              children: <Widget>[
                // GoogleSignInButton(
                //   text: "Google Login",
                //   onPressed: () {
                //     /**
                //      * saveLocalData("loginType", "Google");
                //      *  */
                //   }, 
                //   darkMode: true, // default: false
                // ),

                FacebookSignInButton(onPressed: () async {
                  // call authentication logic
                  final facebookLogin = FacebookLogin();
                  final result = await facebookLogin.logInWithReadPermissions(['email']);

                  switch (result.status) {
                    case FacebookLoginStatus.loggedIn:
                      final token = result.accessToken.token;

                      FbProfile profile = await getFacebookProfile(token);//json.decode(graphResponse.body);
                      print(profile.toJson());
                      welcomeName = profile.name;
                      urlProfile = profile?.picture?.data?.url;
                      saveLocalData("userLogin", profile.toJson());
                      saveLocalData("loginType", "Facebook");
                      Navigator.of(context).pop(true);
                      break;
                    case FacebookLoginStatus.cancelledByUser:
                      print("Canceled by user");
                      Navigator.of(context).pop(false);
                      //_showCancelledMessage();
                      break;
                    case FacebookLoginStatus.error:
                      print("Error");
                      print(result.errorMessage);
                      Navigator.of(context).pop(false);
                      //_showErrorOnUI(result.errorMessage);
                      break;
                  }
                },
                  text: "Facebook Login",
                ),
                SizedBox(
                  height: 5,
                ),
                menu == 'map' ? LayoutBuilder(
                  builder: (context, constraints) {
                    BorderSide bs = BorderSide(
                      color: Theme.of(context).primaryColor,
                    );

                    return ButtonTheme(
                      height: 40.0,
                      padding: EdgeInsets.all(8.0),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5),
                        side: bs,
                      ),
                      child: RaisedButton(
                        onPressed: () {
                          Navigator.of(context).pop(true);
                        },
                        color: Colors.white,
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Icon(Icons.pin_drop, color: Theme.of(context).primaryColor,),
                            Padding(
                              padding: const EdgeInsets.only(left: 14.0, right: 10.0),
                              child: Text(
                                "Lewati",
                                style: TextStyle(
                                  // default to the application font-style
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.bold,
                                  color: Theme.of(context).primaryColor,
                                ),
                              ),
                            ),
                            Spacer(),
                          ],
                        ),
                      ),
                    );
                  },
                ): Container(),
                SizedBox(
                  height: 10,
                ),
              ],
            ),
          ),
          
        ],
      );
    },
  );
}

Future<bool> willExit(BuildContext context,
    {String titleText = "Perhatian",
    String contentText = "Apakah anda yakin akan keluar?",
    String cancelText = "Batal",
    String exitText = "Keluar"}) {
  return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(titleText),
            content: myText(text: contentText),
            actions: <Widget>[
              FlatButton(
                child: myText(text:exitText,color: 'danger'),
                onPressed: () {
                  Navigator.of(context).pop(true);
                },
              ),
              FlatButton(
                child: myText(text:cancelText,color: 'primary'),
                onPressed: () {
                  Navigator.of(context).pop(false);
                },
              ),
              VerticalDivider(
                width: 10,
                color: Colors.white,
              ),
            ],
          );
        },
      ) ??
      false;
}
