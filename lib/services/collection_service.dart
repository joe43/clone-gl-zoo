import 'package:gl_zoo/globals.dart';
import 'package:gl_zoo/models/animal_api.dart';
import 'package:gl_zoo/models/my_collection_api.dart';
import 'package:gl_zoo/models/total_collection_api.dart';
import 'package:http/http.dart' as http;

Future<TotalCollectionApi> getTotalCollection(String token) async {
  final header = {"Content-Type": "application/json"};

  final url = getBaseUrl() + "/getcountanimal";
  print(url);

  http.Response res = await http.get(url, headers: header);
  if (res.statusCode == 200) {
    return TotalCollectionApi.fromJson(res.body);
  } else {
    throw Exception(res.body);
  }
}

Future<MyCollectionApi> getMyCollection(String token) async {
  final header = {"Content-Type": "application/json"};

  final url = getBaseUrl() + "/getcollectionbyuser/$userId";
  print(url);

  http.Response res = await http.get(url, headers: header);
  print(res.body);
  if (res.statusCode == 200) {
    print(res.body);
    return MyCollectionApi.fromJson(res.body);
    //throw Exception(res.body);
  } else {
    throw Exception(res.body);
  }
}

Future<AnimalApi> getAnimalDetail(String token, [int animaId = 1]) async {
  final header = {"Content-Type": "application/json"};

  final url = getBaseUrl() + "/getanimaldetail/$animaId";
  print(url);

  http.Response res = await http.get(url, headers: header);
  if (res.statusCode == 200) {
    return AnimalApi.fromJson(res.body);
  } else {
    throw Exception(res.body);
  }
}