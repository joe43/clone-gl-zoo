import 'dart:convert';

import 'package:gl_zoo/globals.dart';
import 'package:gl_zoo/models/promo_api.dart';
import 'package:http/http.dart' as http;

Future<PromoApi> getPromo(String token) async {
  final header = {"Content-Type": "application/json"};

  final url = getBaseUrl() + "/getpromos";
  print(url);

  http.Response res = await http.get(url, headers: header);
  if (res.statusCode == 200) {
    return PromoApi.fromJson(res.body);
  } else {
    throw Exception(res.body);
  }
}

Future<PromoApi> getPromoDetail(String token, [int id=0]) async {
  final header = {"Content-Type": "application/json"};

  final url = getBaseUrl() + "/promosdetail/"+id.toString()+"/"+userId.toString();
  print(url);

  http.Response res = await http.get(url, headers: header);
  if (res.statusCode == 200) {
    if(PromoApi.fromJson(res.body).data2.length > 0){
      return PromoApi.fromJson(res.body);
    }else{
      throw Exception("Data dengan ID bersangkutan tidak ditemukan!");
    }
  } else {
    throw Exception(res.body);
  }
}

Future<bool> tukarPromo(String token, [int id=0, int minus=0]) async {
  
  final header = {"Content-Type": "application/json"};
  final url = getBaseUrl() + "/tukarpromo";
  print(url);
  Map<String, dynamic> body ={
    "userid": userId,
    "id": id,
    "minus": minus,
  };
  http.Response res = await http.post(url, headers: header, body: json.encode(body));
  if (res.statusCode == 200) {
    print(jsonDecode(res.body)["data"] == true);
    if(jsonDecode(res.body)["data"] == true ){
      return true;
    }else{
      throw Exception("Gagal!");
    }
  } else {
    throw Exception(res.body);
  }
}