import 'package:gl_zoo/models/user_api.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

import 'package:gl_zoo/globals.dart';
import 'package:http/http.dart' as http;
/**
 * User data access from/to local storage
 */
/// Save Key-Value Pair to local storage
saveLocalData(String key, String value) async {
  final prefs = await SharedPreferences.getInstance();
  await prefs.setString(key, value);
}

/// Return Value using Key from local storage
Future<String> readLocalData(String key) async {
  final prefs = await SharedPreferences.getInstance();
  String a = prefs.getString(key);
  return a;
}

/// Remove Key-Value Pair from local storage
Future<bool> deleteLocalData(String key) async {
  final prefs = await SharedPreferences.getInstance();
  return prefs.remove(key);
}

Future<UserApi> getUser(String token) async {
  final header = {"Content-Type": "application/json"};

  final url = getBaseUrl() + "/getuserfull/"+userId.toString();
  print(url);

  http.Response res = await http.get(url, headers: header);
  if (res.statusCode == 200) {
    return UserApi.fromJson(res.body);
  } else {
    throw Exception(res.body);
  }
}

Future<bool> postMessage(String token, Map<String, dynamic> _data) async {
  final header = {"Content-Type": "application/json"};
  final url = getBaseUrl() + "/postinbox";
  print(url);
  print(json.encode(_data));
  http.Response res = await http.post(url, headers: header, body: json.encode(_data));
  if (res.statusCode == 200) {
    print(jsonDecode(res.body)["data"] == true);
    if(jsonDecode(res.body)["data"] == true ){
      return true;
    }else{
      throw Exception("Gagal!");
    }
  } else {
    throw Exception(res.body);
  }
}
