import 'dart:convert';

import 'package:gl_zoo/globals.dart';
import 'package:gl_zoo/models/activity_type_api.dart';
import 'package:gl_zoo/models/activity_api.dart';
import 'package:gl_zoo/models/daily_price_api.dart';
import 'package:http/http.dart' as http;

Future<ActivityTypeApi> getActivityType(String token) async {
  final header = {"Content-Type": "application/json"};

  final url = getBaseUrl() + "/getactivitytype";
  print(url);

  http.Response res = await http.get(url, headers: header);
  if (res.statusCode == 200) {
    return ActivityTypeApi.fromJson(res.body);
  } else {
    throw Exception(res.body);
  }
}

Future<ActivityApi> getActivityByType(String token, String typeId) async {
  final header = {"Content-Type": "application/json"};

  final url = getBaseUrl() + "/getactivitybyType/$typeId";
  print(url);

  http.Response res = await http.get(url, headers: header);
  if (res.statusCode == 200) {
    return ActivityApi.fromJson(res.body);
  } else {
    throw Exception(res.body);
  }
}

Future<ActivityApi> getAllActivity(String token) async {
  final header = {"Content-Type": "application/json"};

  final url = getBaseUrl() + "/getactivity";
  print(url);

  http.Response res = await http.get(url, headers: header);
  if (res.statusCode == 200) {
    return ActivityApi.fromJson(res.body);
  } else {
    throw Exception(res.body);
  }
}

Future<DailyPriceApi> getDailyPrice(String token) async {
  final header = {"Content-Type": "application/json"};

  final url = getBaseUrl() + "/getDailyPrice";
  print(url);

  http.Response res = await http.get(url, headers: header);
  if (res.statusCode == 200) {
    return DailyPriceApi.fromJson(res.body);
  } else {
    throw Exception(res.body);
  }
}

Future<Activity> getAtraksiDetail(String token, int id) async {
  final header = {"Content-Type": "application/json"};

  final url = getBaseUrl() + "/getactivitydetail/$id";
  print(url);

  http.Response res = await http.get(url, headers: header);
  if (res.statusCode == 200) {
    ActivityApi a = ActivityApi.fromJson(res.body);
    
    if(a.data.length > 0){
      return a.data[0];
    }else{
      throw Exception("Detail Data Not Found!!");
    }
  } else {
    throw Exception(res.body);
  }
}

Future<ActivityApi> getFeeding(String token, [String menu='0']) async {
  final header = {"Content-Type": "application/json"};
  String url;
  if(menu=='0'){
    url = getBaseUrl() + "/getfeedingstoday";
  }else{
    url = getBaseUrl() + "/getfeedingsthisweek";
    // url = getBaseUrl() + "/getfeedings";
  }
  print(url);

  http.Response res = await http.get(url, headers: header);
  if (res.statusCode == 200) {
    return ActivityApi.fromJson(res.body);
  } else {
    throw Exception(res.body);
  }
}

Future<Activity> getFeedingDetail(String token, int id) async {
  final header = {"Content-Type": "application/json"};

  final url = getBaseUrl() + "/getfeedingsdetail/$id";
  print(url);

  http.Response res = await http.get(url, headers: header);
  if (res.statusCode == 200) {
    ActivityApi a = ActivityApi.fromJson(res.body);
    
    if(a.data.length > 0){
      return a.data[0];
    }else{
      throw Exception("Detail Data Not Found!!");
    }
  } else {
    throw Exception(res.body);
  }
}

Future<bool> followActivity(String token, int id) async{
  final header = {"Content-Type": "application/json"};
  final url = getBaseUrl() + "/followactivity";
  Map<String, dynamic> _data = {
    "userid" : userId,
    "id" : id,
  };
  print(url);
  print(json.encode(_data));
  http.Response res = await http.post(url, headers: header, body: json.encode(_data));
  if (res.statusCode == 200) {
    print(jsonDecode(res.body)["data"] == true);
    if(jsonDecode(res.body)["data"] == true ){
      return true;
    }else{
      throw Exception("Gagal!");
    }
  } else {
    throw Exception(res.body);
  }
}