import 'package:http/http.dart' as http;
import 'package:gl_zoo/models/fb_profile.dart';

Future<FbProfile> getFacebookProfile(String token) async {
  //final header = {"Content-Type": "application/json"};

  final url = 'https://graph.facebook.com/v2.12/me?fields=name,picture.width(800).height(800),first_name,last_name,email&access_token=$token';
  print(url);

  http.Response res = await http.get(url);
  if (res.statusCode == 200) {
    print(res.body);
    return FbProfile.fromJson(res.body);
  } else {
    throw Exception(res.body);
  }
}