import 'package:gl_zoo/globals.dart';
import 'package:gl_zoo/models/template_api.dart';
import 'package:http/http.dart' as http;

Future<TemplateApi> getTemplateData(String token) async {
  final header = {"Content-Type": "application/json"};

  final url = getBaseUrl() + "/templateApi";
  print(url);

  http.Response res = await http.get(url, headers: header);
  if (res.statusCode == 200) {
    return TemplateApi.fromJson(res.body);
  } else {
    throw Exception(res.body);
  }
}