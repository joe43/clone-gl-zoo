import 'package:gl_zoo/globals.dart';
import 'package:gl_zoo/models/facility_api.dart';
import 'package:http/http.dart' as http;

Future<FacilityApi> getFacility(String token) async {
  final header = {"Content-Type": "application/json"};

  final url = getBaseUrl() + "/getfacilities";
  print(url);

  http.Response res = await http.get(url, headers: header);
  if (res.statusCode == 200) {
    return FacilityApi.fromJson(res.body);
  } else {
    throw Exception(res.body);
  }
}