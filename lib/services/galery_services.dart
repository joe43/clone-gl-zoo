import 'package:gl_zoo/globals.dart';
import 'package:gl_zoo/models/galery_api.dart';
import 'package:http/http.dart' as http;

Future<GaleryApi> getGaleryCategories(String token) async {
  final header = {"Content-Type": "application/json"};

  final url = getBaseUrl() + "/getgalleriescategories";
  print(url);

  http.Response res = await http.get(url, headers: header);
  if (res.statusCode == 200) {
    return GaleryApi.fromJson(res.body);
  } else {
    throw Exception(res.body);
  }
}

Future<GaleryApi> getGaleryDatas(String token, int id) async {
  final header = {"Content-Type": "application/json"};

  final url = getBaseUrl() + "/getgallerybycategory/"+ id.toString();
  print(url);

  http.Response res = await http.get(url, headers: header);
  if (res.statusCode == 200) {
    return GaleryApi.fromJson(res.body);
  } else {
    throw Exception(res.body);
  }
}