import 'package:gl_zoo/globals.dart';
import 'package:gl_zoo/models/logo_home.dart';
import 'package:gl_zoo/models/slider_api.dart';
import 'package:gl_zoo/models/spotlight_api.dart';
import 'package:gl_zoo/models/static_api.dart';
import 'package:http/http.dart' as http;

Future<LogoHome> getIconHome(String token) async {
  final header = {"Content-Type": "application/json"};

  final url = getBaseUrl() + "/getmenus";
  print(url);

  http.Response res = await http.get(url, headers: header);
  if (res.statusCode == 200) {
    return logoHomeFromJson(res.body);
  } else {
    throw Exception(res.body);
  }
}


Future<SliderApi> getSlider(String token) async {
  final header = {"Content-Type": "application/json"};

  final url = getBaseUrl() + "/getslider";
  print(url);

  http.Response res = await http.get(url, headers: header);
  if (res.statusCode == 200) {
    return SliderApi.fromJson(res.body);
  } else {
    throw Exception(res.body);
  }
}

Future<StaticApi> getStatic(String token) async {
  final header = {"Content-Type": "application/json"};

  final url = getBaseUrl() + "/getstaticdatas";
  print(url);

  http.Response res = await http.get(url, headers: header);
  if (res.statusCode == 200) {
    return StaticApi.fromJson(res.body);
  } else {
    throw Exception(res.body);
  }
}

Future<SpotlightApi> getSpotlight(String token) async {
  final header = {"Content-Type": "application/json"};

  final url = getBaseUrl() + "/getspotlights";
  print(url);

  http.Response res = await http.get(url, headers: header);
  if (res.statusCode == 200) {
    return SpotlightApi.fromJson(res.body);
  } else {
    throw Exception(res.body);
  }
}

Future<SpotLight> getSpotLightDetail(String token, [int id = 0]) async {
  final header = {"Content-Type": "application/json"};

  final url = getBaseUrl() + "/spotlightdetail/"+id.toString();
  print(url);

  http.Response res = await http.get(url, headers: header);
  if (res.statusCode == 200) {
    if(SpotlightApi.fromJson(res.body).data.length > 0){
      return SpotlightApi.fromJson(res.body).data[0];
    }else{
      throw Exception("Data dengan ID bersangkutan tidak ditemukan!");
    }
  } else {
    throw Exception(res.body);
  }
}