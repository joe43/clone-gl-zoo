import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:gl_zoo/globals.dart' as globals;
import 'package:intl/date_symbol_data_local.dart';

import 'package:gl_zoo/models/facility_api.dart';
import 'package:gl_zoo/services/facility_service.dart';


class FacilityPage extends StatefulWidget {
  @override
  _FacilityPageState createState() => _FacilityPageState();
}

class _FacilityPageState extends State<FacilityPage> with SingleTickerProviderStateMixin{
  TabController controller1;
  int needToLoad = 0;
  bool isLoading = false;
  bool isLoadingActivity = true;
  int totalCollection = 0;
  
  List<Facility> typeList = [];

  String publicLink = "http://www.glzoobackend.xyz/backend-gl-zoo/public/";
  // List<Widget> collections;
  // @override
  // void initState() {
  //   super.initState();
  //   controller1 = new TabController(vsync: this, length: 3);
  // }

  @override
  void dispose() {
    controller1.dispose();
    super.dispose();
  }

  _FacilityPageState() {
    needToLoad = 1;
    isLoading = true;
    initializeDateFormatting("id_ID");

    getFacility("token").then((resp){
      if(resp.data.length > 0){
        setState(() {
          typeList = resp.data;
          print("JUMLAH FASILITAS");
          print(typeList.length);
          print((typeList.length/2).ceil());
          controller1 = TabController(length: typeList.length, vsync: this);
        });
      }else{
        typeList = [Facility(
          id: 0,
          nama: "No Data"
        )];
      }
      setState(() {
        needToLoad--;
        if(needToLoad == 0)isLoading = false;
      });
    });

  }

  Widget _cardCont(String title, String subtitle, [bool no_icon = false]) {
    double elevate = 5;
    return GestureDetector(
      onTap: () {
        if(subtitle == "gembiralokawifi"){
          globals.showDialogs("Password Wifi: " + globals.wifiPass, context);
        }
      },
      child: Container(
        margin: EdgeInsets.fromLTRB(10, 5, 10, 5),
        child: Card(
          elevation: elevate,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          child: no_icon ?
          ListTile(
            title: globals.myText(text: title, weight: "B", color: "accentTextDark", context: context),
            subtitle:
                globals.myText(text: subtitle, size: 11, color: "unprime"),
          ):
          ListTile(
            leading: Icon(Icons.wifi, size: 40),
            title: globals.myText(text: title, weight: "B", color: "accentTextDark", context: context),
            subtitle:
                globals.myText(text: subtitle, size: 11, color: "unprime"),
          ),
        ),
      ),
    );
  }

  Widget _buildGridItems(BuildContext context, int index) {
    bool firstRow = false;
    bool firstCol = false;
    bool lastCol = false;
    bool lastRow = false;
    BoxBorder bb;
    if(index < 4){
      firstRow = true;
    }
    if(index % 4 == 0){
      firstCol = true;
    }
    if(index % 4 == 3){
      lastCol = true;
    }
    if(index >= (typeList.length/4).floor() * 4){
      lastRow = true;
    }
    if(!lastRow){
      if(firstCol){
        bb= Border(
          right: BorderSide(color: Colors.grey[350], width: 1.0),
          bottom: BorderSide(color: Colors.grey[350], width: 1.0),
        );
      }else if(lastCol){
        bb= Border(
          bottom: BorderSide(color: Colors.grey[350], width: 1.0),
        );
      }else{
        bb= Border(
          right: BorderSide(color: Colors.grey[350], width: 1.0),
          bottom: BorderSide(color: Colors.grey[350], width: 1.0),
        );
      }
    }else{
      if(!lastCol){
        bb= Border(
          right: BorderSide(color: Colors.grey[350], width: 1.0),
        );
      }
    }
    
    return GestureDetector(
      onTap: () => _gridItemTapped(index),
      child: GridTile(
        child: Container(
          decoration: BoxDecoration(
            border: bb
          ),
          padding: EdgeInsets.all(0),
          margin: EdgeInsets.all(0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Center(
                child: _buildGridItem(index),
              ),
              globals.myText(text: typeList[index].nama, align: TextAlign.center, size: 12),
            ],
          )
        ),
      ),
    );
  }

  Widget _buildGridItem(int idx) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(0),
      child: CachedNetworkImage(
        height: (globals.mw(context)/(2*(typeList.length/2).ceil()).ceilToDouble()),
        width: (globals.mw(context)/(2*(typeList.length/2).ceil()).ceilToDouble()),
        fit: BoxFit.fill,
        imageUrl: publicLink + typeList[idx].thumbnail,
        placeholder: (context, url) => Container(
          width: (globals.mw(context)/(2*(typeList.length/2).ceil()).ceilToDouble()),
          height: (globals.mw(context)/(2*(typeList.length/2).ceil()).ceilToDouble()),
          child: globals.isLoading(),
        ),
        errorWidget: (context, url, error){
          print(error.toString());
          return Container(
            child: Image.asset('assets/images/broken_image.png',
              height: (globals.mw(context)/(2*(typeList.length/2).ceil()).ceilToDouble()),
              width: (globals.mw(context)/(2*(typeList.length/2).ceil()).ceilToDouble()),
              colorBlendMode: BlendMode.saturation,
              color: Colors.grey,
            )
          );
        },
      )
    );
  }
  

  void _gridItemTapped(idx){
    if(idx < typeList.length){
      print(typeList[idx].id);
      globals.detailAnimaId = typeList[idx].id;
      // Navigator.of(context).pushNamed('/facility/detail');
      globals.showDialogs(typeList[idx].deskripsi, context, title: typeList[idx].nama);
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.grey[200],
        bottomNavigationBar: globals.bottomAppBar(context),
        floatingActionButton: globals.floatingAppBar(context),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        appBar: globals.appBar(context, "Fasilitas"),
        body: ListView(
          padding: EdgeInsets.all(0),
          children: <Widget>[
            _cardCont("Gembira Loka Zona A", 'gembiralokawifi'),
            // isLoading? globals.isLoading() : 
            // Container(
            //   padding: const EdgeInsets.all(0),
            //   margin: const EdgeInsets.all(15),
            //   // decoration: BoxDecoration(
            //   //   border: Border.all(color: Colors.black, width: 2.0)
            //   // ),
            //   height: globals.mh(context) - 300,
            //   child: GridView.builder(
            //     scrollDirection: Axis.vertical,
            //     shrinkWrap: true,
            //     gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            //       crossAxisCount: (typeList.length/2).ceil(),
            //     ),
            //     itemBuilder: _buildGridItems,
            //     itemCount: typeList.length,
            //   ),
            // ),
            isLoading? globals.isLoading() : 
            Container(
              margin: EdgeInsets.fromLTRB(10, 5, 10, 5),
              child: Card(
                elevation: 5,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: GridView.builder(
                  padding: EdgeInsets.all(10),
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 4,
                  ),
                  itemBuilder: _buildGridItems,
                  itemCount: typeList.length,
                ),
              ),
            ),
            //_cardCont("Fasilitas Terdekat", 'Anda tidak sedang berada pada Area Gembira Loka atau Data Lokasi Fasilitas/Gembira-Loka tidak ditemukan', true),
          ],
        ),
      ),
    );
  }
}
