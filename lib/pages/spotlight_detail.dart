import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:gl_zoo/globals.dart' as globals;
import 'package:gl_zoo/models/spotlight_api.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:gl_zoo/services/static_services.dart';
import 'package:intl/intl.dart';

class SpotLightDetailPage extends StatefulWidget {
  @override
  _SpotLightDetailPageState createState() => _SpotLightDetailPageState();
}

class _SpotLightDetailPageState extends State<SpotLightDetailPage> {
  SpotLight spotlight;
  String publicLink = "http://www.glzoobackend.xyz/backend-gl-zoo/public/";

  bool isLoading;
  int needToLoad;
  final oCcy = new NumberFormat("#,##0.00", "id_ID");
  _SpotLightDetailPageState() {
    needToLoad = 1;
    isLoading = true;

    getSpotLightDetail("token", globals.detailSpotLightId).then((resp) {
      setState(() {
        spotlight = resp;
        needToLoad--;
        if(needToLoad == 0)isLoading = false;
      });
    }).catchError((onError) {
      globals.showDialogs(onError.toString(), context);
    });
    
  }

  Widget _buildCardTop() {
    return Container(
        //height: 400,
        width: globals.mw(context)-10,
        margin: EdgeInsets.fromLTRB(10, 10, 10, 30),
        child: Card(
          elevation: 5,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: isLoading ? Column() : Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                globals.myText(text: spotlight.nama, weight: 'B', size: 16),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0,5,0,5),
                  child: globals.myText(text: "Harga: Rp. "+oCcy.format(int.parse(spotlight.harga))),
                ),
                globals.myText(text: "Location: "+ spotlight.location ),
                globals.myText(text: spotlight.timeStart.substring(0, spotlight.timeStart.length -3) +" s/d "+ spotlight.timeEnd.substring(0, spotlight.timeEnd.length -3)),
                Divider(color: Colors.grey[300],),
                globals.myText(text: spotlight.deskripsi, align: TextAlign.left, ),
                SizedBox(height: 50,),
              ],
            ),
          ),
        ),
    );
  }

  Widget topImage(){
    return isLoading ? Container(
      padding: EdgeInsets.only(top: 10),
      width: 100,
      height: 100,
      child: globals.isLoading(),
    )
    : CachedNetworkImage(
      //height: globals.mw(context) * 0.5625,
      width: globals.mw(context),
      fit: BoxFit.fitHeight,
      imageUrl: publicLink + spotlight.image,
      placeholder: (context, url) => Container(
        padding: EdgeInsets.only(top: 10),
        width: 100,
        height: 100,
        child: globals.isLoading(),
      ),
      errorWidget: (context, url, error){
        print(error.toString());
        return Container(
          child: Image.asset(
            "assets/images/broken_image.png",
            fit: BoxFit.fitHeight,
            //height: globals.mw(context) * 0.5625,
            width: double.infinity,
          ),
        );
      },
    );
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      resizeToAvoidBottomInset: true,
      bottomNavigationBar: globals.bottomAppBar(context),
      floatingActionButton: globals.floatingAppBar(context),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      appBar: globals.appBar(context, "Detail SpotLight"),
      body: ListView(
          children: <Widget>[
            topImage(),
            isLoading ? globals.isLoading()
            :_buildCardTop(),
          ],
        ),
    );
  }
}
