import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:gl_zoo/globals.dart' as globals;
import 'package:gl_zoo/models/activity_api.dart';
import 'package:gl_zoo/services/activity_service.dart';
import 'package:cached_network_image/cached_network_image.dart';

class ActivityDetailPage extends StatefulWidget {
  @override
  _ActivityDetailPageState createState() => _ActivityDetailPageState();
}

class _ActivityDetailPageState extends State<ActivityDetailPage> {
  Activity activity;
  String publicLink = "http://www.glzoobackend.xyz/backend-gl-zoo/public/";

  bool isLoading;
  int needToLoad;
  _ActivityDetailPageState() {
    needToLoad = 1;
    isLoading = true;

    getAtraksiDetail("token", globals.detailActivityId).then((resp) {
      setState(() {
        activity = resp;
        needToLoad--;
        if(needToLoad == 0)isLoading = false;
      });
    }).catchError((onError) {
      globals.showDialogs(onError.toString(), context);
    });
    
  }

  Widget _buildCardTop() {
    List<String> x = activity.days.split(';');
    List<bool> days = [false,false,false,false,false,false,false];
    x.forEach((day){
      switch (day.toLowerCase()) {
        case 'senin': days[0] = true; break;
        case 'selasa': days[1] = true; break;
        case 'rabu': days[2] = true; break;
        case 'kamis': days[3] = true; break;
        case 'jumat': days[4] = true; break;
        case 'sabtu': days[5] = true; break;
        case 'minggu': days[6] = true; break;
        default:
      }
    });
    DateTime date = DateTime.now();
    int dateNum = date.weekday-1;
    Color chipColor = Colors.grey[400];
    String chip = "Tutup";
    if(days[dateNum]){
      chipColor = Theme.of(context).primaryColor;
      chip = "Buka";
    }
    return Container(
        //height: 400,
        width: globals.mw(context)-10,
        margin: EdgeInsets.fromLTRB(10, 10, 10, 30),
        child: Card(
          elevation: 5,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          child: isLoading ? Column() : Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                    child: 
                      Column(
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              globals.myText(text: activity.nama, weight: 'B', size: 16),
                              Chip(
                                padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                                backgroundColor: chipColor,
                                label: Text(chip),
                                labelStyle: TextStyle(
                                  fontSize: 12,
                                  fontWeight: FontWeight.bold
                                ),
                              ),
                            ],
                          ),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              globals.myText(text: activity.location),
                              globals.myText(text: activity.timeStart.toString().split(' ')[1].split('.')[0].split(':').sublist(0,2).join('.') +' - '+ activity.timeEnd.toString().split(' ')[1].split('.')[0].split(':').sublist(0,2).join('.'), size: 12),
                            ],
                          ),
                        ],
                      ),
                  ),
                  Divider(indent: 10, endIndent: 10, color: Colors.grey[300],),
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: globals.myText(text: activity.deskripsi, align: TextAlign.left, ),
                  ),
                  SizedBox(
                    height: 50,
                  ),
                ],
              ),
              Container(
                width: globals.mw(context) * 0.9,
                padding: EdgeInsets.only(bottom: 10),
                child: Card(
                  elevation: 0,
                  color: Colors.grey[200],
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  child: Container(
                    padding: EdgeInsets.all(10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Expanded(child: globals.myText(text: "Senin", size: 12,),),
                            days[0] ? Expanded(child: globals.myText(text: "Buka", size: 12))
                            : Expanded(child: globals.myText(text: "Tutup", size: 12, color:'unprime')),
                            Expanded(child: globals.myText(text: "Jumat", size: 12,)),
                            days[4] ? Expanded(child: globals.myText(text: "Buka", size: 12, align: TextAlign.right))
                            : Expanded(child: globals.myText(text: "Tutup", size: 12, color:'unprime', align: TextAlign.right)),
                          ],
                        ),
                        Divider(),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Expanded(child: globals.myText(text: "Selasa", size: 12,),),
                            days[1] ? Expanded(child: globals.myText(text: "Buka", size: 12))
                            : Expanded(child: globals.myText(text: "Tutup", size: 12, color:'unprime')),
                            Expanded(child: globals.myText(text: "Sabtu", size: 12,)),
                            days[5] ? Expanded(child: globals.myText(text: "Buka", size: 12, align: TextAlign.right))
                            : Expanded(child: globals.myText(text: "Tutup", size: 12, color:'unprime', align: TextAlign.right)),
                          ],
                        ),
                        Divider(),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Expanded(child: globals.myText(text: "Rabu", size: 12,),),
                            days[2] ? Expanded(child: globals.myText(text: "Buka", size: 12))
                            : Expanded(child: globals.myText(text: "Tutup", size: 12, color:'unprime')),
                            Expanded(child: globals.myText(text: "Minggu", size: 12,)),
                            days[6] ? Expanded(child: globals.myText(text: "Buka", size: 12, align: TextAlign.right))
                            : Expanded(child: globals.myText(text: "Tutup", size: 12, color:'unprime', align: TextAlign.right)),
                          ],
                        ),
                        Divider(),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Expanded(child: globals.myText(text: "Kamis", size: 12,),),
                            days[3] ? Expanded(child: globals.myText(text: "Buka", size: 12))
                            : Expanded(child: globals.myText(text: "Tutup", size: 12, color:'unprime')),
                            Expanded(child: globals.myText(text: " ", size: 12,)),
                            Expanded(child: globals.myText(text: " ", size: 12, align: TextAlign.right,)),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
    );
  }

  Widget topImage(){
    return isLoading ? Container(
      padding: EdgeInsets.only(top: 10),
      width: 100,
      height: 100,
      child: globals.isLoading(),
    )
    : CachedNetworkImage(
      //height: globals.mw(context) * 0.5625,
      width: globals.mw(context),
      fit: BoxFit.fitHeight,
      imageUrl: publicLink + activity.image,
      placeholder: (context, url) => Container(
        padding: EdgeInsets.only(top: 10),
        width: 100,
        height: 100,
        child: globals.isLoading(),
      ),
      errorWidget: (context, url, error){
        print(error.toString());
        return Container(
          child: Image.asset(
            "assets/images/broken_image.png",
            fit: BoxFit.fitHeight,
            //height: globals.mw(context) * 0.5625,
            width: double.infinity,
          ),
        );
      },
    );
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      resizeToAvoidBottomInset: true,
      bottomNavigationBar: globals.bottomAppBar(context),
      floatingActionButton: globals.floatingAppBar(context),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      appBar: globals.appBar(context, "Detail Atraksi"),
      body: ListView(
          children: <Widget>[
            Stack(
              children: <Widget>[
                topImage(),
                Positioned(
                  bottom: 10,
                  left: 10,
                  child: GestureDetector(
                    onTap: (){
                      followActivity("token", activity.id).then((res){
                        globals.showDialogs("Berhasil mengikuti!", context, title: "Success");
                      }).catchError((err){
                        globals.showDialogs(err.toString(), context);
                      });
                    },
                    child: Container(
                      width: 120,
                      height: 50,
                      child: Card(
                        elevation: 5,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0),
                        ),
                        child:Padding(
                          padding: const EdgeInsets.fromLTRB(5,0,0,0),
                          child: Row(
                            children: <Widget>[
                              Icon(Icons.bookmark, color: Theme.of(context).primaryColor, size: 30, ),
                              globals.myText(text: "Follow"),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                Positioned(
                  bottom: 10,
                  right: 10,
                  child: GestureDetector(
                    onTap: (){
                      globals.openMap(context);
                    },
                    child: Container(
                      width: 120,
                      height: 50,
                      child: Card(
                        elevation: 5,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0),
                        ),
                        child:Padding(
                          padding: const EdgeInsets.fromLTRB(5,0,0,0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Icon(Icons.location_on, color: Theme.of(context).primaryColor, size: 30, ),
                              globals.myText(text: "Tampilkan\r\ndi Peta", size: 14),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
            isLoading ? globals.isLoading()
            :_buildCardTop(),
          ],
        ),
    );
  }
}
