import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:gl_zoo/globals.dart' as globals;

// import 'package:carousel_slider/carousel_slider.dart';
// import 'package:flutter_masked_text/flutter_masked_text.dart';
// import 'package:cached_network_image/cached_network_image.dart';

//import 'package:gl_zoo/models/template_api.dart';
import 'package:gl_zoo/services/template_service.dart';

//import 'package:intl/intl.dart' show DateFormat;
// import 'package:intl/intl.dart';
// import 'package:intl/date_symbol_data_local.dart';

class TemplatePage extends StatefulWidget {
  @override
  _TemplatePageState createState() => _TemplatePageState();
}

class _TemplatePageState extends State<TemplatePage> with SingleTickerProviderStateMixin{
  // TabController controller1;
  int needToLoad = 0;
  bool isLoading = false;
  // @override
  // void initState() {
  //   super.initState();
  //   controller1 = new TabController(vsync: this, length: 3);
  // }

  // @override
  // void dispose() {
  //   controller1.dispose();
  //   super.dispose();
  // }

  _TemplatePageState() {
    needToLoad = 1;
    isLoading = true;
    getTemplateData("token").then((resp){
      
      if(resp.hasil){
        // Do Something ...
      }
      setState(() {
        needToLoad--;
        if(needToLoad == 0) isLoading = false;
      });
    });

  }
  
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.grey[200],
        bottomNavigationBar: globals.bottomAppBar(context),
        floatingActionButton: globals.floatingAppBar(context),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        appBar: AppBar(
          actions: <Widget>[
            Icon(
              Icons.search,
              size: 35,
            ),
            SizedBox(
              width: 10,
            ),
          ],
          title: globals.myText(text: "Title", color: "light", size: 20),
          leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.pop(context);
              }),
        ),
        body: ListView(
          padding: EdgeInsets.all(0),
          children: <Widget>[
            Container(
              //height: 200,
            ),
          ],
        ),
      ),
    );
  }
}
