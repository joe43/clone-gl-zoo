import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:gl_zoo/globals.dart' as globals;
import 'package:gl_zoo/models/animal_api.dart';
// import 'package:gl_zoo/models/spotlight_api.dart';
// import 'package:gl_zoo/models/static_api.dart';
import 'package:gl_zoo/services/collection_service.dart';
import 'package:cached_network_image/cached_network_image.dart';
// import 'package:gl_zoo/models/fb_profile.dart';
// import 'package:gl_zoo/services/user_services.dart';

class CollectionDetailPage extends StatefulWidget {
  @override
  _CollectionDetailPageState createState() => _CollectionDetailPageState();
}

class _CollectionDetailPageState extends State<CollectionDetailPage> {
  Animal animal;
  String publicLink = "http://www.glzoobackend.xyz/backend-gl-zoo/public/";

  // List<SpotLight> spotLights;
  // List<Widget> imageSpotlight = [];
  // FbProfile fbUser;
  // String welcomeName = "Guess";
  // bool isLogin = false;
  // String homeContact;
  // String homeVisit;
  bool isLoading;
  int needToLoad;
  _CollectionDetailPageState() {
    needToLoad = 1;
    isLoading = true;

    getAnimalDetail("token", globals.detailAnimaId).then((resp) {
      setState(() {
        animal = resp.data;
        needToLoad--;
        if(needToLoad == 0)isLoading = false;
      });
    }).catchError((onError) {
      globals.showDialogs(onError.toString(), context);
    });
    
  }

  Widget _buildCardTop() {
    return Positioned(
      top: 215,
      child: Align(
        heightFactor: 1,
        alignment: Alignment.topCenter,
        child: Container(
        height: 400,
        width: globals.mw(context)-10,
        margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
        child: Card(
          elevation: 5,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          child: isLoading ? Column() : Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
                children: <Widget>[
                  SizedBox(
                    height: 50,
                  ),
                  globals.myText(text: animal.nama.toUpperCase()),
                  Divider(indent: 10, endIndent: 10, color: Colors.grey[300],),
                  globals.myText(text: animal.deskripsi, align: TextAlign.center, ),
                  
                ],
              ),
              Container(
                width: globals.mw(context) * 0.9,
                padding: EdgeInsets.only(bottom: 10),
                child: Card(
                  elevation: 0,
                  color: Colors.grey[300],
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  child: Container(
                    padding: EdgeInsets.all(10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        globals.myText(text: "Tahukah kamu?", 
                          align: TextAlign.center,
                          size: 16,
                          weight: "M"),
                        globals.myText(text: animal.fakta, align: TextAlign.center, ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      ),
    );
  }

  Widget _buildProfileImage() {
    return Positioned.fill(
      top: 175,//globals.mh(context) * 0.12,
      child: Align(
        alignment: Alignment.topCenter,
        child: Container(
          height: 100,
          child: isLoading
          ? Container(
              padding: EdgeInsets.only(top: 10),
              width: globals.mw(context) * 0.8,
              height: 100,
              child: globals.isLoading(),
            )
          : ClipRRect(
            borderRadius: BorderRadius.circular(45),
            child: CachedNetworkImage(
              height: 100,
              width: 100,
              fit: BoxFit.fill,
              imageUrl: publicLink + animal.thumbnail,
              placeholder: (context, url) => Container(
                padding: EdgeInsets.only(top: 10),
                width: 100,
                height: 100,
                child: globals.isLoading(),
              ),
              errorWidget: (context, url, error){
                print(error.toString());
                return Container(
                  child: Image.asset('assets/images/broken_image.png',
                    height: 100,
                    width: 100,
                    colorBlendMode: BlendMode.saturation,
                    color: Colors.grey,
                  )
                );
              },
            )
          ),
        ),
      ),
    );
  }

  Widget _buildCardLeft(){
   return Positioned.fill(
      left: 25,
      top: 235,
      child: Align(
        alignment: Alignment.topLeft,
        child: Container(
          height: 40,
          width: globals.mw(context) * 0.25,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            globals.myText(text: "Kelas", color: "unprime"),
            globals.myText(text: "Not in API", size: 12),
          ],
          ),
        ),
      ),
    ); 
  }

  Widget _buildCardRight(){
   return Positioned.fill(
      right: 25,
      top: 235,
      child: Align(
        alignment: Alignment.topRight,
        child: Container(
          height: 40,
          width: globals.mw(context) * 0.25,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            globals.myText(text: "Umur", color: "unprime"),
            globals.myText(text: "Not in API", size: 12),
          ],
          ),
        ),
      ),
    ); 
  }

  Widget stackedDetail(){
    return Stack(
          overflow: Overflow.clip,
          alignment: Alignment.center,
          children: <Widget>[
            Positioned(
              top: 0,
              child: Padding(///////////////////
                padding: const EdgeInsets.only(bottom: 160),
                child: isLoading
                  ? Container(
                      padding: EdgeInsets.only(top: 10),
                      width: globals.mw(context),
                      height: globals.mw(context) * 0.5625,
                      child: globals.isLoading(),
                    )
                  : CachedNetworkImage(
                  //height: globals.mw(context) * 0.5625,
                  width: globals.mw(context),
                  fit: BoxFit.fill,
                  imageUrl: publicLink + animal.image,
                  placeholder: (context, url) => Container(
                    padding: EdgeInsets.only(top: 10),
                    width: 100,
                    height: 100,
                    child: globals.isLoading(),
                  ),
                  errorWidget: (context, url, error){
                    print(error.toString());
                    return Container(
                      child: Image.asset(
                        "assets/images/broken_image.png",
                        fit: BoxFit.fitHeight,
                        //height: globals.mw(context) * 0.5625,
                        width: double.infinity,
                      ),
                    );
                  },
                ),
              ),
            ),
            _buildCardTop(),
            _buildCardLeft(),
            _buildCardRight(),
            _buildProfileImage(),
            
          ],
        );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      resizeToAvoidBottomInset: true,
      bottomNavigationBar: globals.bottomAppBar(context),
      floatingActionButton: globals.floatingAppBar(context),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      appBar: globals.appBar(context, "Detail Satwa"),
      body: SingleChildScrollView(
        child:Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              height: 650,
              child: stackedDetail(),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(10,0,10,10),
              margin: EdgeInsets.only(bottom: 25),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Card(
                    color: Theme.of(context).primaryColor,
                    elevation: 5,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    child: Container(
                      width: globals.mw(context)/2 - 30,
                      height: 55,
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          ImageIcon(
                            AssetImage("assets/images/book_collection.png"),
                            color: Colors.white,
                          ),
                          VerticalDivider(width: 10, color: Theme.of(context).primaryColor,),
                          globals.myText(text: "Tambah Ke\r\nKoleksi", color: "light"),
                        ],
                      ),
                    )
                  ),
                  Card(
                    color: Theme.of(context).primaryColor,
                    elevation: 5,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    child: Container(
                      width: globals.mw(context)/2 - 30,
                      height: 55,
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          ImageIcon(
                            AssetImage("assets/images/question.png"),
                            color: Colors.white,
                          ),
                          VerticalDivider(width: 10, color: Theme.of(context).primaryColor,),
                          globals.myText(text: "Mainkan\r\nQuiz", color: "light"),
                        ],
                      ),
                    )
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
