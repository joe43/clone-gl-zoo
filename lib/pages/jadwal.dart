import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:gl_zoo/globals.dart' as globals;
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
// import 'package:gl_zoo/models/logo_home.dart';
// import 'package:gl_zoo/models/spotlight_api.dart';
// import 'package:gl_zoo/models/static_api.dart';
// import 'package:gl_zoo/services/static_services.dart';
//import 'package:cached_network_image/cached_network_image.dart';
import 'package:gl_zoo/models/activity_api.dart';
import 'package:gl_zoo/models/activity_type_api.dart';
// import 'package:gl_zoo/models/fb_profile.dart';
import 'package:gl_zoo/services/activity_service.dart';

import 'package:flutter_calendar_carousel/flutter_calendar_carousel.dart'
    show CalendarCarousel;
import 'package:flutter_calendar_carousel/classes/event.dart';
import 'package:flutter_calendar_carousel/classes/event_list.dart';
//import 'package:intl/intl.dart' show DateFormat;
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';

class CalendarPage extends StatefulWidget {
  @override
  _CalendarPageState createState() => _CalendarPageState();
}

class _CalendarPageState extends State<CalendarPage> with SingleTickerProviderStateMixin{
  var dewasaPriceController = MoneyMaskedTextController(
      precision: 0, leftSymbol: "Rp. ", decimalSeparator: "");
  var anakPriceController = MoneyMaskedTextController(
      precision: 0, leftSymbol: "Rp. ", decimalSeparator: "");
  var totalPriceController = MoneyMaskedTextController(
      precision: 0, leftSymbol: "Rp. ", decimalSeparator: "");
  int countDewasa = 2;
  int countAnak = 1;
  DateTime _currentDate = DateTime.now();
  DateTime _selectedDate = DateTime.now();
  bool isLoading = false;
  bool isLoadingActivity = false;
  int needToLoad = 0;
  int _current = 0;
  bool _fullDate = false;
  CalendarCarousel _calendarCarousel, _calendarCarouselNoHeader;
  String _currentMonth = '';
  String _menu;

  ActivityApi allActivity;
  ActivityTypeApi allActivityType;
  ActivityApi selectedActivityTab = ActivityApi();
  ActivityApi filteredActivityTab = ActivityApi();
  List<ActivityType> typeList = [];

  TabController controller1;

  // @override
  // void initState() {
  //   super.initState();
  //   controller1 = new TabController(vsync: this, length: 3);
  // }

  @override
  void dispose() {
    controller1.dispose();
    super.dispose();
  }

  _CalendarPageState() {
    initializeDateFormatting("id_ID");
    _currentMonth = DateFormat.yMMMM("id_ID").format(_selectedDate);
    isLoading = true;
    needToLoad = 2;
    _menu = "1";
    selectedActivityTab.data = [];
    filteredActivityTab.data = [];

    dewasaPriceController.updateValue(30000);
    anakPriceController.updateValue(15000);
    totalPriceController.updateValue(75000);
    
    getDailyPrice("token").then((resp){
      if(resp.data.length > 0){
        dewasaPriceController.updateValue(resp.data[0].price);
        anakPriceController.updateValue(resp.data[0].promoPrice);
        totalPriceController.updateValue(2*resp.data[0].price + resp.data[0].promoPrice);
      }
      setState(() {
        needToLoad--;
        if(needToLoad == 0)isLoading = false;
      });
    });

    getActivityType("token").then((resp){
      if(resp.data.length > 0){
        typeList = resp.data;
        controller1 = TabController(length: typeList.length, vsync: this);
        setState(() {
          isLoadingActivity = true;  
        });
        getActivityByType("token", typeList[0].id.toString()).then((resp){
          setState(() {
            selectedActivityTab = resp;
            filterActivities();
            isLoadingActivity = false;
          });
        });
      }else{
        typeList = [ActivityType(
          id: 0,
          nama: "No Data"
        )];
      }
      setState(() {
        needToLoad--;
        if(needToLoad == 0)isLoading = false;
      });
    });
    _calendarCarousel = CalendarCarousel<Event>(
      todayBorderColor: Colors.red,
      onDayPressed: (DateTime date, List<Event> events) {
        setState((){
          _selectedDate = date;
          _fullDate = false;
          filterActivities();
        });
        events.forEach((event) => print(event.title));
      },
      weekendTextStyle: TextStyle(
        color: Colors.red,
        fontSize: 14,
      ),
      weekdayTextStyle: TextStyle(
        fontSize: 12,
        color: Colors.grey
      ),
      thisMonthDayBorderColor: Colors.red,
      weekFormat: false,
      markedDatesMap: _markedDateMap,
      height: 300,
      selectedDateTime: _selectedDate,
      locale: "id",
      customGridViewPhysics: NeverScrollableScrollPhysics(),
      markedDateShowIcon: true,
      markedDateIconMaxShown: 2,
      markedDateMoreShowTotal:
          false, // null for not showing hidden events indicator
      showHeader: false,
      markedDateIconBuilder: (event) {
        return event.icon;
      },
      todayTextStyle: TextStyle(
        color: Colors.white,
        fontSize: 14,
      ),
      
      todayButtonColor: Colors.black12,
      selectedDayTextStyle: TextStyle(
        color: Colors.white,
        fontSize: 14,
      ),
      //minSelectedDate: _currentDate,
      maxSelectedDate: _currentDate.add(Duration(days: 60)),
      inactiveDaysTextStyle: TextStyle(
        color: Colors.grey,
        fontSize: 14,
      ),
      daysTextStyle: TextStyle(
        color: Colors.black,
        fontSize: 14,
      ),
      nextDaysTextStyle: TextStyle(
        color: Colors.black,
        fontSize: 14,
      ),
      prevDaysTextStyle: TextStyle(
        color: Colors.black,
        fontSize: 14,
      ),
      onCalendarChanged: (DateTime date) {
        setState(() => _currentMonth = DateFormat.yMMMM("id_ID").format(date));
      },
    );

    _calendarCarouselNoHeader = CalendarCarousel<Event>(
      todayBorderColor: Colors.red,
      onDayPressed: (DateTime date, List<Event> events) {
        setState((){
          _selectedDate = date;
          filterActivities();
        });
        events.forEach((event) => print(event.title));
      },

      weekendTextStyle: TextStyle(
        color: Colors.red,
        fontSize: _fullDate ? 14: 20,
      ),
      weekdayTextStyle: TextStyle(
        fontSize: 12,
        color: Colors.grey
      ),
      thisMonthDayBorderColor: Colors.red,
      weekFormat: _fullDate ? false:true,
      markedDatesMap: _markedDateMap,
      height: _fullDate ? 300: 75.0,
      selectedDateTime: _selectedDate,
      locale: "id",
      customGridViewPhysics: NeverScrollableScrollPhysics(),
      markedDateShowIcon: true,
      markedDateIconMaxShown: 2,
      markedDateMoreShowTotal:
          false, // null for not showing hidden events indicator
      showHeader: false,
      markedDateIconBuilder: (event) {
        return event.icon;
      },
      todayTextStyle: TextStyle(
        color: Colors.white,
        fontSize: _fullDate ? 14:20,
      ),
      todayButtonColor: Colors.black12,
      selectedDayTextStyle: TextStyle(
        color: Colors.white,
        fontSize: _fullDate ? 14:20,
      ),
      // minSelectedDate: _currentDate,
      maxSelectedDate: _currentDate.add(Duration(days: 60)),
      inactiveDaysTextStyle: TextStyle(
        color: Colors.grey,
        fontSize: _fullDate ? 14:20,
      ),
      daysTextStyle: TextStyle(
        color: Colors.black,
        fontSize: _fullDate ? 14:20,
      ),
      nextDaysTextStyle: TextStyle(
        color: Colors.black,
        fontSize: _fullDate ? 14:20,
      ),
      prevDaysTextStyle: TextStyle(
        color: Colors.black,
        fontSize: _fullDate ? 14:20,
      ),
      onCalendarChanged: (DateTime date) {
        setState(() => _currentMonth = DateFormat.yMMMM("id_ID").format(date));
      },
    );
  }

  static Widget _eventIcon = new Container(
    decoration: new BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(1000)),
        border: Border.all(color: Colors.blue, width: 2.0)),
    child: new Icon(
      Icons.person,
      color: Colors.amber,
    ),
  );

  EventList<Event> _markedDateMap = new EventList<Event>(
    events: {
      new DateTime(2019, 2, 10): [
        new Event(
          date: new DateTime(2019, 2, 10),
          title: 'Event 1',
          icon: _eventIcon,
        ),
        new Event(
          date: new DateTime(2019, 2, 10),
          title: 'Event 2',
          icon: _eventIcon,
        ),
        new Event(
          date: new DateTime(2019, 2, 10),
          title: 'Event 3',
          icon: _eventIcon,
        ),
      ],
    },
  );

  List<String> imageCalendar = [
    "assets/images/home-slider-1.png",
    "assets/images/galery-thin-b.jpg"
  ];
  List<Widget> dummyList = [
    Text("assets/images/home-slider-1.png"),
    Text("assets/images/galery-thin-b.jpg")
  ];

  Widget buildImageCalendar(BuildContext contex, String assetName){
    return Container(
      width: globals.mw(context),
      child: Image.asset(
        assetName,
        fit: BoxFit.fill,
        height: 150,
        width: globals.mw(context),
      ),
    );
  }

  Widget _buildCarousel(BuildContext context) {
    int idx = -1;
    return isLoading
      ? Container(
          padding: EdgeInsets.only(top: 10),
          width: globals.mw(context) * 0.8,
          height: 175,
          child: globals.isLoading(),
        )
      :Stack(
        children: <Widget>[
          Container(
            width: globals.mw(context),
            color: Colors.transparent,
            child: CarouselSlider(
              aspectRatio: 3,
              autoPlay: true,
              viewportFraction: 1.0,
              height: 175,
              enableInfiniteScroll: true,
              onPageChanged: (index) {
                setState(() {
                  _current = index;
                });
              },
              items: <Widget>[
                buildImageCalendar(context, "assets/images/home-slider-1.png"),
                buildImageCalendar(context, "assets/images/galery-thin-b.jpg"),
              ],
            ),
          ),
          Positioned(
              bottom: 5,
              width: globals.mw(context),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: dummyList.map((f) {
                  idx++;
                  return _buildDoted(idx);
                }).toList(),
              )),
        ],
      );
  }

  void filterActivities(){
    setState(() {
      filteredActivityTab = ActivityApi.fromJson(selectedActivityTab.toJson());
      filteredActivityTab.data.removeWhere((a){
        bool remove = true;
        Duration diffStart = a.timeStart.difference(_selectedDate); 
        Duration diffEnd = a.timeEnd.difference(_selectedDate); 
        print("diffStart.inMilliseconds");
        print(diffStart.inHours);
        print("diffEnd.inMilliseconds");
        print(diffEnd.inDays);
        print(a.timeStart.day);
        print(_selectedDate.day);
        if(diffStart.inDays <= 0 && diffEnd.inDays >= 0){//If passed
          if(diffEnd.inDays == 0 && (a.timeEnd.day != _selectedDate.day)){
            remove = true;
          }else{
            remove = false;
          }
        }
        return remove;
      });
    });
  }

  Widget _buildDoted(int index) {
    return Container(
      margin: EdgeInsets.fromLTRB(3, 0, 3, 0),
      height: 10,
      width: 10,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(100),
        color: index == _current ? Colors.white : Colors.black54,
      ),
    );
  }
  
  Widget getEvent(String title, String location, DateTime start, DateTime end, [int id = 0]){
    DateTime now = DateTime.now();
    String chip = "Chip";
    Color chipColor = Theme.of(context).primaryColor;
    Duration diffStart = start.difference(now);
    Duration diffEnd = end.difference(now);

    if(diffStart.inMilliseconds <= 0){
      if(diffEnd.inMilliseconds > 0){//Not End Yet
        chip = "Buka";
      }else{//Already End
        chip = "Berakhir";
        chipColor = Colors.grey[400];
      }
    }else{//Not Open Yet
      if(diffStart.inHours == 0){
        chip = "${(diffStart.inMinutes % 60) + 1} Menit Lagi";
      }else{
        chip = "${diffStart.inHours % 24} Jam Lagi";
      }
      
      chipColor = Colors.orangeAccent;
    }

    return GestureDetector(
      onTap: () => _itemTapped(id),
      child: Container(
        width: globals.mw(context)*0.8,
        child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Text(title,
            softWrap: true,
            style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.w600,
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Chip(
                backgroundColor: chipColor,
                label: Text(chip),
                labelStyle: TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.bold
                ),
              ),
              VerticalDivider(
                color: Colors.white,
              ),
              Text("Lokasi: $location \r\n${DateFormat('d MMMM HH:mm','id_ID').format(start)} s/d\r\n${DateFormat('d MMMM HH:mm','id_ID').format(end)}",
              //Text("$location (${start.hour}.${start.minute} - ${end.hour}.${end.minute})",
                style: TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.normal,
                  color: Colors.grey
                ),
              ),
            ],
          ),
          Divider()
        ]),
      ),
    );
  }

  void _itemTapped(int id){
    globals.detailActivityId = id;
    Navigator.of(context).pushNamed('/atraksi/detail');
  }
  
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.grey[200],
        bottomNavigationBar: globals.bottomAppBar(context),
        floatingActionButton: globals.floatingAppBar(context),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        appBar: AppBar(
          actions: <Widget>[
            Icon(
              Icons.search,
              size: 35,
            ),
            SizedBox(
              width: 10,
            ),
          ],
          title: globals.myText(text: "Jadwal", color: "light", size: 20),
          leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.pop(context);
              }),
        ),
        body: ListView(
          padding: EdgeInsets.all(0),
          children: <Widget>[
            _buildCarousel(context),
            Container(
              //height: 200,
              margin: EdgeInsets.fromLTRB(10, 5, 10, 5),
              child: Card(
                elevation: 5,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        height: 50,
                        padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
                        // color: Colors.red,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              _currentMonth,
                              style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.w500,
                                color: Colors.black//Theme.of(context).primaryColor,
                              ),
                            ),
                            FlatButton.icon(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              color: Theme.of(context).primaryColor,
                              icon: Icon(_fullDate ? Icons.check: Icons.event, size: 14, color: Colors.white,), //`Icon` to display
                              label: Text(_fullDate ? 'Selesai' : 'Kalendar', style: TextStyle(
                                color: Colors.white,
                              ),), //`Text` to display
                              onPressed: () {
                                setState(() {
                                  _fullDate = !_fullDate;
                                });
                                //Code to execute when Floating Action Button is clicked
                                //...
                              },
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 16.0),
                        child: _fullDate ? _calendarCarousel: _calendarCarouselNoHeader,
                      ),
                    ],
                  ),
              ),
            ),

            Container(
              //color: Colors.blue,
              margin: EdgeInsets.fromLTRB(10, 5, 10, 5),
              child: Card(
                elevation: 5,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: isLoading
                  ? Container(
                      padding: EdgeInsets.only(top: 10),
                      width: globals.mw(context) * 0.8,
                      height: 175,
                      child: globals.isLoading(),
                    )
                  :Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        height: 50,
                        padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
                        child:
                        TabBar(
                          indicator: ShapeDecoration(
                            color: Theme.of(context).primaryColor,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                          ),
                          indicatorColor: Theme.of(context).primaryColor,
                          indicatorWeight: 2.0,
                          indicatorSize: TabBarIndicatorSize.tab,
                          indicatorPadding: EdgeInsets.all(0),
                          unselectedLabelColor: Colors.grey[600],
                          labelColor: Colors.white,
                          controller: controller1,
                          isScrollable: true,
                          onTap: (idx) {
                              String typeId = typeList[idx].id.toString();
                              setState(() {
                                isLoadingActivity = true;
                              });
                              getActivityByType("token", typeId).then((resp){
                                setState(() {
                                  _menu = '$idx';
                                  selectedActivityTab = resp;
                                  filterActivities();
                                  isLoadingActivity = false;
                                });
                              });
                              },
                          tabs: List<Widget>.from(typeList.map((f){
                            return Text(f.nama);
                          })),
                        ),
                      ),

                      isLoadingActivity
                      ? Container(
                          padding: EdgeInsets.only(top: 10),
                          width: globals.mw(context),
                          height: 175,
                          child: globals.isLoading(),
                        )
                      :Container(
                        padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: filteredActivityTab.data.length>0? List<Widget>.from(
                            filteredActivityTab.data.map((a){
                              return getEvent(a.nama, a.location, a.timeStart, a.timeEnd, a.id);
                            })
                          ): <Widget>[Text("There's no Activity for this selection", style: TextStyle(
                            color: Colors.grey[600],
                            fontStyle: FontStyle.italic,
                          ),),
                            Divider(),
                          ],
                        ),
                      ),
                    ],
                ),
              ),

            ),

            Container(
              margin: EdgeInsets.fromLTRB(10, 5, 10, 35),
              child: Card(
                elevation: 5,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        height: 50,
                        padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
                        // color: Colors.red,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              "Estimasi Harga Tiket",
                              style: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w500,
                                color: Colors.grey//Theme.of(context).primaryColor,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Text("Dewasa", style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 12,
                                    ),),
                                    VerticalDivider(),
                                    Text(dewasaPriceController.text, style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 12,
                                    ),),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Text("Anak(3-15th)", style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 12,
                                    ),),
                                    VerticalDivider(),
                                    Text(anakPriceController.text, style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 12,
                                    ),),
                                  ],
                                )
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Image.asset("assets/images/adult.png",
                                      width: 40,
                                      height: 60,
                                    ),
                                    Container(
                                      padding: EdgeInsets.fromLTRB(10, 10, 0, 5),
                                      child: Text(countDewasa.toString(),style: TextStyle(
                                        color: Colors.grey[700],
                                        fontSize: 30,
                                      ),),
                                    ),
                                    Container(
                                      padding: EdgeInsets.fromLTRB(10, 10, 10, 5),
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                          IconButton(
                                            iconSize: 30,
                                            padding: EdgeInsets.all(0),
                                            icon: Icon(Icons.keyboard_arrow_up),
                                            onPressed: (){
                                              setState(() {
                                                countDewasa++;
                                                totalPriceController.updateValue( (anakPriceController.numberValue * countAnak) + (dewasaPriceController.numberValue * countDewasa) );
                                              });
                                            },
                                          ),
                                          IconButton(
                                            iconSize: 30,
                                            padding: EdgeInsets.all(0),
                                            icon: Icon(Icons.keyboard_arrow_down),
                                            onPressed: (){
                                              setState(() {
                                                if(countDewasa > 0)
                                                  countDewasa--;
                                                totalPriceController.updateValue( (anakPriceController.numberValue * countAnak) + (dewasaPriceController.numberValue * countDewasa) );
                                              });
                                            },
                                          )
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Image.asset("assets/images/kid.png",
                                      width: 40,
                                      height: 60,
                                    ),
                                    Container(
                                      padding: EdgeInsets.fromLTRB(10, 10, 0, 5),
                                      child: Text(countAnak.toString(),style: TextStyle(
                                        color: Colors.grey[700],
                                        fontSize: 30,
                                      ),),
                                    ),
                                    Container(
                                      padding: EdgeInsets.fromLTRB(10, 10, 10, 5),
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                          IconButton(
                                            iconSize: 30,
                                            padding: EdgeInsets.all(0),
                                            icon: Icon(Icons.keyboard_arrow_up),
                                            onPressed: (){
                                              setState(() {
                                                countAnak++;
                                                totalPriceController.updateValue( (anakPriceController.numberValue * countAnak) + (dewasaPriceController.numberValue * countDewasa) );
                                              });
                                            },
                                          ),
                                          IconButton(
                                            iconSize: 30,
                                            padding: EdgeInsets.all(0),
                                            icon: Icon(Icons.keyboard_arrow_down),
                                            onPressed: (){
                                              setState(() {
                                                if(countAnak > 0)
                                                  countAnak--;
                                                totalPriceController.updateValue( (anakPriceController.numberValue * countAnak) + (dewasaPriceController.numberValue * countDewasa) );
                                              });
                                            },
                                          )
                                        ],
                                      ),
                                    ),
                                  ],
                                )
                              ],
                            ),
                            Chip(
                              label: Text("Anak di bawah umur 3 tahun gratis masuk",
                              style: TextStyle(
                                color: Colors.black
                              ),),
                            ),
                            Divider(),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  DateFormat('EEEE','id_ID').format(DateTime.now()),
                                  style: TextStyle(
                                    fontSize: 16,
                                    color: Colors.grey[700],
                                  ),
                                ),
                                Text(totalPriceController.text,
                                  style: TextStyle(
                                    color: Colors.grey[700],
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16
                                  ),
                                ),
                                
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  DateFormat('dd MMMM y','id_ID').format(DateTime.now()),
                                  style: TextStyle(
                                    fontSize: 14,
                                    color: Colors.grey[700],
                                  ),
                                ),
                                Chip(
                                  label: Text(_currentDate.weekday > 5 ? "Weekend" : "Weekday", style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 12
                                  ),),
                                  backgroundColor: Colors.orangeAccent,
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
              ),
            ),
            
          ],
        ),
      ),
    );
  }
}
