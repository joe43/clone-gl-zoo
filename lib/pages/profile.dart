import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:gl_zoo/globals.dart' as globals;
import 'package:gl_zoo/models/fb_profile.dart';
import 'package:gl_zoo/models/user_api.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:gl_zoo/services/user_services.dart';
// import 'package:gl_zoo/models/fb_profile.dart';
// import 'package:gl_zoo/services/user_services.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  User animal;
  String publicLink = "http://www.glzoobackend.xyz/backend-gl-zoo/public/";
  FbProfile fbUser;
  bool isLoading;
  int needToLoad;
  _ProfilePageState() {
    needToLoad = 1;
    isLoading = true;
    
    getUser("token").then((resp) {
      setState(() {
        animal = resp.data;
        print("animal.photo");
        print(animal.toJson());

        readLocalData("userLogin").then((data){
        if(data != null && data != ""){
          setState(() {
            fbUser = FbProfile.fromJson(data);
            animal.name = fbUser.name;
            animal.email = fbUser.email;
            animal.photo = fbUser.picture.data.url;
            globals.isLogin = true;
          });
        }
      });
        needToLoad--;
        if(needToLoad == 0)isLoading = false;
      });
    }).catchError((onError) {
      globals.showDialogs(onError.toString(), context);
    });
    
  }

  Widget _buildCardTop() {
    return Positioned(
      top: 100,
      child: Align(
        heightFactor: 1,
        alignment: Alignment.topCenter,
        child: Container(
        height: 400,
        width: globals.mw(context)-10,
        margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
        child: Card(
          elevation: 5,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          child: isLoading ? Column() : Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
                children: <Widget>[
                  SizedBox(
                    height: 70,
                  ),
                  globals.myText(text: animal.name.toUpperCase(), weight: "B"),
                  // Divider(indent: 10, endIndent: 10, color: Colors.grey[300],),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    height: 70,
                    padding: const EdgeInsets.all(10.0),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            decoration: BoxDecoration(
                              border: Border(
                                right: BorderSide(color: Colors.grey[200]),
                              ),
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                globals.myText(text: "Not in API"),
                                globals.myText(text: "Quiz Jawaban Benar", align: TextAlign.center, size: 10, color: "unprime"),
                              ],
                            ),
                          ),
                        ),
                        Expanded(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              globals.myText(text: "Not in API"),
                              globals.myText(text: "Member", align: TextAlign.center, size: 10, color: "unprime"),
                            ],
                          ),
                        ),
                        Expanded(
                          child: Container(
                            decoration: BoxDecoration(
                              border: Border(
                                left: BorderSide(color: Colors.grey[200]),
                              ),
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                globals.myText(text: "Not in API"),
                                globals.myText(text: "Jumlah Kunjungan", align: TextAlign.center, size: 10, color: "unprime"),
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  // globals.myText(text: animal.email, align: TextAlign.center, ),
                  Container(
                    padding: EdgeInsets.fromLTRB(10,0,10,0),
                    child: Card(
                      elevation: 0,
                      color: Colors.grey[300],
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: Divider(height: 5, color: Colors.grey[300],),
                          ),
                          Container(
                            height: 50,
                            padding: EdgeInsets.all(15),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    ImageIcon(AssetImage("assets/images/walk.png"), size: 25, color: Colors.black38,),
                                    globals.myText(text: "Jarak/ Total", size: 12),
                                  ],
                                ),
                                
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: <Widget>[
                                    globals.myText(text: animal.meter.toString(), size: 26),
                                    globals.myText(text: "km", size: 14, align: TextAlign.end),
                                    globals.myText(text: " / ", size: 26),
                                    globals.myText(text: "?", size: 26),
                                    globals.myText(text: "km", size: 16),
                                  ],
                                )
                                
                              ],
                            ),
                          ),

                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: Divider(height: 10,),
                          ),

                          Container(
                            height: 50,
                            padding: EdgeInsets.all(15),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              // crossAxisAlignment: CrossAxisAlignment.end,
                              children: <Widget>[
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    ImageIcon(AssetImage("assets/images/fire.png"), size: 25, color: Colors.black38,),
                                    globals.myText(text: "Kalori/ Total", size: 12),
                                  ],
                                ),
                                
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: <Widget>[
                                    globals.myText(text: animal.calory.toString(), size: 26),
                                    globals.myText(text: "cal", size: 14, align: TextAlign.end),
                                    globals.myText(text: " / ", size: 26),
                                    globals.myText(text: "?", size: 26),
                                    globals.myText(text: "cal", size: 16),
                                  ],
                                )
                                
                              ],
                            ),
                          ),

                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: Divider(height: 10,),
                          ),

                          Container(
                            height: 50,
                            padding: EdgeInsets.all(15),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              // crossAxisAlignment: CrossAxisAlignment.end,
                              children: <Widget>[

                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    ImageIcon(AssetImage("assets/images/clock.png"), size: 25, color: Colors.black38,),
                                    globals.myText(text: "Durasi/ Total", size: 12),
                                  ],
                                ),
                                
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: <Widget>[
                                    globals.myText(text: animal.timeinminutes.toString(), size: 26),
                                    globals.myText(text: "mnt", size: 14, align: TextAlign.end),
                                    globals.myText(text: " / ", size: 26),
                                    globals.myText(text: "?", size: 26),
                                    globals.myText(text: "mnt", size: 16),
                                  ],
                                )
                                
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: Divider(height: 5, color: Colors.grey[300],),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              // Container(
              //   width: globals.mw(context) * 0.9,
              //   padding: EdgeInsets.only(bottom: 10),
              //   child: Card(
              //     elevation: 0,
              //     color: Colors.grey[300],
              //     shape: RoundedRectangleBorder(
              //       borderRadius: BorderRadius.circular(10.0),
              //     ),
              //     child: Container(
              //       padding: EdgeInsets.all(10),
              //       child: Column(
              //         mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              //         children: <Widget>[
              //           globals.myText(text: "Tahukah kamu?", 
              //             align: TextAlign.center,
              //             size: 16,
              //             weight: "M"),
              //           globals.myText(text: animal.phoneNumber, align: TextAlign.center, ),
              //         ],
              //       ),
              //     ),
              //   ),
              // ),
            ],
          ),
        ),
      ),
      ),
    );
  }

  Widget _buildProfileImage() {
    return Positioned.fill(
      top: 50,//globals.mh(context) * 0.12,
      child: Align(
        alignment: Alignment.topCenter,
        child: Container(
          height: 100,
          child: isLoading
          ? Container(
              padding: EdgeInsets.only(top: 10),
              width: globals.mw(context) * 0.8,
              height: 100,
              child: globals.isLoading(),
            )
          : ClipRRect(
            borderRadius: BorderRadius.circular(50),
            child: CachedNetworkImage(
              height: 100,
              width: 100,
              fit: BoxFit.fill,
              // imageUrl: publicLink + animal.photo,
              imageUrl: animal.photo,
              placeholder: (context, url) => Container(
                padding: EdgeInsets.only(top: 10),
                width: 100,
                height: 100,
                child: globals.isLoading(),
              ),
              errorWidget: (context, url, error){
                print(error.toString());
                return Container(
                  child: Image.asset('assets/images/broken_image.png',
                    height: 100,
                    width: 100,
                    colorBlendMode: BlendMode.saturation,
                    color: Colors.grey,
                  )
                );
              },
            )
          ),
        ),
      ),
    );
  }

  Widget _buildCardLeft(){
   return isLoading ? globals.isLoading(): Positioned.fill(
      left: 25,
      top: 112,
      child: Align(
        alignment: Alignment.topLeft,
        child: Container(
          height: 75,
          width: globals.mw(context) * 0.25,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            globals.myText(text: "Poin", color: "unprime"),
            Chip(
              label: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  globals.myText(text: animal.point!=null ? animal.point : "0", size: 16),
                  Icon(Icons.star, color: Color.fromRGBO(255, 200, 59, 1),)
                ],
              ),
              
              backgroundColor: Colors.grey[200],
            ),
            // globals.myText(text: "Not in API", size: 12),
          ],
          ),
        ),
      ),
    ); 
  }

  Widget _buildCardRight(){
   return isLoading ? globals.isLoading(): Positioned.fill(
      right: 25,
      top: 112,
      child: Align(
        alignment: Alignment.topRight,
        child: Container(
          height: 75,
          width: globals.mw(context) * 0.25,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            globals.myText(text: "Koleksi", color: "unprime"),
            Chip(
              label: Container(
                width: 70,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    globals.myText(text: animal.collected.toString()+" / ? ", size: 14),
                  ],
                )),
              backgroundColor: Colors.grey[200],
            ),
            
          ],
          ),
        ),
      ),
    ); 
  }

  Widget stackedDetail(){
    return Stack(
          overflow: Overflow.clip,
          alignment: Alignment.center,
          children: <Widget>[
            _buildCardTop(),
            _buildCardLeft(),
            _buildCardRight(),
            _buildProfileImage(),
            
          ],
        );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      resizeToAvoidBottomInset: true,
      bottomNavigationBar: globals.bottomAppBar(context),
      floatingActionButton: globals.floatingAppBar(context),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      // appBar: globals.appBar(context, "Profil"),
      appBar: AppBar(
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.exit_to_app,
              size: 35,
            ),
            onPressed: () async {
              bool x = await globals.confirmDialog("Anda yakin ingin logout?", context);
              if(x){
                deleteLocalData("userLogin").then((v){
                  globals.isLogin = false;
                  globals.welcomeName = "Guess";
                  globals.urlProfile = null;
                  Navigator.popUntil(context, ModalRoute.withName('/'));
                  Navigator.pop(context);
                  Navigator.pushNamed(context, '/');
                });
              }else{

              }
            },
          ),
          SizedBox(
            width: 10,
          ),
        ],
        title: globals.myText(text: "Profil", color: "light", size: 20),
        leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pop(context);
            }),
      ),
      body: SingleChildScrollView(
        child:Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              height: 530,
              child: stackedDetail(),
            ),
            // Container(
            //   padding: EdgeInsets.fromLTRB(10,0,10,10),
            //   margin: EdgeInsets.only(bottom: 25),
            //   child: Row(
            //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //     crossAxisAlignment: CrossAxisAlignment.center,
            //     children: <Widget>[
            //       Card(
            //         color: Theme.of(context).primaryColor,
            //         elevation: 5,
            //         shape: RoundedRectangleBorder(
            //           borderRadius: BorderRadius.circular(10.0),
            //         ),
            //         child: Container(
            //           width: globals.mw(context)/2 - 30,
            //           height: 55,
            //           padding: EdgeInsets.symmetric(horizontal: 10),
            //           child: Row(
            //             mainAxisAlignment: MainAxisAlignment.start,
            //             children: <Widget>[
            //               ImageIcon(
            //                 AssetImage("assets/images/book_collection.png"),
            //                 color: Colors.white,
            //                 size: 30,
            //               ),
            //               VerticalDivider(width: 10, color: Theme.of(context).primaryColor,),
            //               globals.myText(text: "Buka\r\nKoleksi Satwa", color: "light"),
            //             ],
            //           ),
            //         )
            //       ),
            //       Card(
            //         color: Theme.of(context).primaryColor,
            //         elevation: 5,
            //         shape: RoundedRectangleBorder(
            //           borderRadius: BorderRadius.circular(10.0),
            //         ),
            //         child: Container(
            //           width: globals.mw(context)/2 - 30,
            //           height: 55,
            //           padding: EdgeInsets.symmetric(horizontal: 10),
            //           child: Row(
            //             mainAxisAlignment: MainAxisAlignment.start,
            //             children: <Widget>[
            //               Image(
            //                 image: AssetImage("assets/images/qr_code.png"),
            //                 width: 30,
            //                 height: 30,
            //               ),
            //               VerticalDivider(width: 10, color: Theme.of(context).primaryColor,),
            //               globals.myText(text: "Scan\r\nQR", color: "light"),
            //             ],
            //           ),
            //         )
            //       ),
            //     ],
            //   ),
            // ),
          ],
        ),
      ),
    );
  }
}
