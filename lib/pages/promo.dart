import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:gl_zoo/globals.dart' as globals;
import 'package:intl/date_symbol_data_local.dart';

import 'package:gl_zoo/models/promo_api.dart';
import 'package:gl_zoo/services/promo_service.dart';


class PromoPage extends StatefulWidget {
  @override
  _PromoPageState createState() => _PromoPageState();
}

class _PromoPageState extends State<PromoPage> with SingleTickerProviderStateMixin{
  TabController controller1;
  int needToLoad = 0;
  bool isLoading = false;
  bool isLoadingActivity = true;
  int totalCollection = 0;
  
  List<Promo> typeList = [];
  
  String publicLink = "http://www.glzoobackend.xyz/backend-gl-zoo/public/";
  // List<Widget> collections;
  // @override
  // void initState() {
  //   super.initState();
  //   controller1 = new TabController(vsync: this, length: 3);
  // }

  @override
  void dispose() {
    controller1.dispose();
    super.dispose();
  }

  _PromoPageState() {
    needToLoad = 1;
    isLoading = true;
    initializeDateFormatting("id_ID");

    getPromo("token").then((resp){
      if(resp.data.length > 0){
        setState(() {
          typeList = resp.data;
          print("JUMLAH PROMO");
          print(typeList.length);
          print((typeList.length/2).ceil());
          controller1 = TabController(length: typeList.length, vsync: this);
          needToLoad--;
          if(needToLoad == 0)isLoading = false;
        });
      }else{
        setState(() {
          typeList = [Promo(
            id: 0,
            nama: "No Data"
          )];
          needToLoad--;
          if(needToLoad == 0)isLoading = false;
        });
      }
    });

  }

  Widget _cardCont(Promo x) {
    return isLoading? globals.isLoading() : GestureDetector(
      onTap: () => _itemTapped(x.id),
      child: Container(
        margin: EdgeInsets.fromLTRB(10, 5, 10, 5),
        child: Card(
          elevation: 5,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(bottom: 0),
                child: CachedNetworkImage(
                    height: 150,
                    width: globals.mw(context),
                    fit: BoxFit.fill,
                    imageUrl: publicLink+x.image,
                    placeholder: (context, url) => Container(
                      padding: EdgeInsets.only(top: 10),
                      width: 100,
                      height: 100,
                      child: globals.isLoading(),
                    ),
                    errorWidget: (context, url, error){
                      print(error.toString());
                      return Container(
                        child: Image.asset(
                          "assets/images/broken_image.png",
                          fit: BoxFit.fitHeight,
                          height: globals.mw(context) * 0.5625,
                          width: double.infinity,
                        ),
                      );
                    },
                  ),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    globals.myText(text: x.nama + " ["+ x.harga+" point]", weight: 'B'),
                    globals.myText(text: "Start: " + "-, End: -"),
                  ],
                ),
              ),
              
            ],
          ),
        ),
      ),
    );
  }

  void _itemTapped(int id){
    globals.detailPromoId = id;
    Navigator.of(context).pushNamed('/promo/detail');
  }

  Future<bool> _willPopCallback() async {
      // await showDialog or Show add banners or whatever
      globals.currentMenu = "Home";
      Navigator.pushReplacementNamed(context, "/");
      // then
      return false; // return true if the route to be popped
  }
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => _willPopCallback(),
      child: SafeArea(
        child: Scaffold(
          backgroundColor: Colors.grey[200],
          bottomNavigationBar: globals.bottomAppBar(context),
          floatingActionButton: globals.floatingAppBar(context),
          floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
          // appBar: globals.appBar(context, "Promo"),
          appBar: AppBar(title: globals.myText(text: "Promo", color: "light", size: 20)),
          body: ListView.builder(
            padding: EdgeInsets.all(0),
            itemCount: typeList.length,
            itemBuilder: (BuildContext context, int index) {
              return _cardCont(typeList[index]);
            },
          )
        ),
      )
    );
  }
}
