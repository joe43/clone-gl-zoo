import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:gl_zoo/globals.dart' as globals;
import 'package:gl_zoo/models/galery_api.dart';
import 'package:gl_zoo/services/galery_services.dart';

class GaleryPage extends StatefulWidget {
  @override
  _GaleryPageState createState() => _GaleryPageState();
}

class _GaleryPageState extends State<GaleryPage> {
  List<Galery> typeList = [];
  List<Galery> category = [];
  int menu = 0;
  int needToLoad = 0;
  bool isLoading = false;
  bool isLoadingGalery = true;

  String publicLink = "http://www.glzoobackend.xyz/backend-gl-zoo/public/";

  _GaleryPageState() {
    needToLoad = 2;
    isLoading = false;

    getGaleryCategories("token").then((resp){
      setState(() {
        category = resp.data;

        needToLoad--;
        if(needToLoad == 0)isLoading = false;
      });
      switchMenu(menu);
    });
    
  }

  void switchMenu(int idx){
    print("IDX CLICKED: "+idx.toString());
    isLoadingGalery = true;
    getGaleryDatas("token", category[idx].id).then((resp){
      setState(() {
        menu = idx;
        typeList = resp.data;
        isLoadingGalery = false;

        needToLoad--;
        if(needToLoad == 0)isLoading = false;
      });
    });
  }

  Widget _buildRowCategory(index) {
    print(publicLink+category[index].banner);
    return Expanded(
      child: Container(
        height: 100,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AdvancedNetworkImage(publicLink + category[index].banner,
              timeoutDuration: Duration(minutes: 1),
            ),
            fit: BoxFit.fill,
            colorFilter: ColorFilter.mode(Color.fromRGBO(0, 0, 0, 0.6), BlendMode.darken),
          ) ,
          border: index == menu ? Border.all(color: Theme.of(context).primaryColor, width: 2) : Border.all(width: 0),
        ),
        padding: EdgeInsets.all(0),
        margin: EdgeInsets.all(10),
        child: index == menu ? 
        Stack(
          children: <Widget>[
            Center(
              child: globals.myText(text: category[index].nama, align: TextAlign.center, size: 13, color: "light", weight: "B"),
            ),
            Positioned(
              bottom: 2,
              right: 3,
              child: Icon(Icons.remove_red_eye, size: 10, color: Colors.white,),
            ),
          ],
        ):
        Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 10),
              child: globals.myText(text: category[index].nama, align: TextAlign.center, size: 13, color: "light", weight: "B"),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal:20),
              child: globals.myText(text: globals.lorem.length > 90 ? globals.lorem.substring(0,90)+"..." : globals.lorem, align: TextAlign.left, size: 12, color: "light",),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildGridItems(BuildContext context, int index) {
    return GestureDetector(
      onTap: () => switchMenu(index),
      child: GridTile(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            _buildRowCategory(index),
          ],
        )
      )
    );
  }

  Widget _buildGaleryItem(int index) {
    return GestureDetector(
      onTap: () => {},
      child: GridTile(
        child:
          Container(
            padding: const EdgeInsets.fromLTRB(0,5,12,5),
            child: CachedNetworkImage(
              height: 100,
              width: globals.mw(context)/3 - 16,
              fit: BoxFit.fill,
              imageUrl: publicLink + typeList[index].image,
              placeholder: (context, url) => Container(
                width: globals.mw(context)/3 - 16,
                height: 100,
                child: globals.isLoading(),
              ),
              errorWidget: (context, url, error){
                print(error.toString());
                return Container(
                  child: Image.asset('assets/images/broken_image.png',
                    height: 100,
                    width: globals.mw(context)/3 - 16,
                    colorBlendMode: BlendMode.saturation,
                    color: Colors.grey,
                  )
                );
              },
              imageBuilder: (context, imageProvider){
                return GestureDetector(
                  onTap: (){
                    globals.currentGaleryImage = imageProvider;
                    globals.galeryImageCaption = typeList[index].deskripsi;
                    Navigator.of(context).pushNamed('/galeri/detail');
                  },
                  child: Image(
                    image: imageProvider,
                    fit: BoxFit.fill,
                    width: globals.mw(context)/3 - 16,
                    height: 100,
                    repeat: ImageRepeat.noRepeat,
                  )
                );
              },
            ),
          )
      )
    );
  }

  Widget _buildGaleryRow(int idx){
    return Row(
      children: <Widget>[
        typeList.length > 3*idx ?  _buildGaleryItem(3*idx) : Spacer(),
        typeList.length > 3*idx+1 ?  _buildGaleryItem(3*idx+1) : Spacer(),
        typeList.length > 3*idx+2 ?  _buildGaleryItem(3*idx+2) : Spacer(),
      ],
      
    );
  }
  Widget _buildGalery(){
    List<int> col = List.generate((typeList.length/3).ceil(), (int idx) => idx);
    return isLoadingGalery ? globals.isLoading() : 
    Container(
      margin: EdgeInsets.only(left: 12),
      child: Column(
        children: List<Widget>.from(col.map((idx){
          return _buildGaleryRow(idx);
        })),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.grey[200],
        bottomNavigationBar: globals.bottomAppBar(context),
        floatingActionButton: globals.floatingAppBar(context),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        appBar: globals.appBar(context, "Galery"),
        body: ListView(
          children: <Widget>[
            Container(
              margin: EdgeInsets.fromLTRB(10, 5, 10, 5),
              child: Card(
                elevation: 5,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                        child: globals.myText(text: "Kategori", color: "unprime", weight: "B"),
                      ),
                      Container(
                        margin: EdgeInsets.all(0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: List<Widget>.from(List.generate(category.length, (idx)=>idx).map((f){
                            return _buildGridItems(context, f);
                          })),
                        ),
                      ),
                    ],
                  ),
                ),

            ),
            SizedBox(
              height: 10,
            ),
            _buildGalery(),
            // Container(
            //   padding: EdgeInsets.fromLTRB(15, 10, 0, 10),
            //   child: globals.myText(text: "Terbaru", color: "disabled"),
            // ),
            // Row(
            //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //   children: <Widget>[
            //     Image.asset(
            //       "assets/images/galery-1.png",
            //       width: globals.mw(context) * 0.32,
            //     ),
            //     Image.asset(
            //       "assets/images/galery-2.png",
            //       width: globals.mw(context) * 0.32,
            //     ),
            //     Image.asset(
            //       "assets/images/galery-3.png",
            //       width: globals.mw(context) * 0.32,
            //     ),
            //   ],
            // ),
            // SizedBox(
            //   height: 10,
            // ),
            // Row(
            //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //   children: <Widget>[
            //     Image.asset(
            //       "assets/images/galery-4.png",
            //       width: globals.mw(context) * 0.32,
            //     ),
            //     Image.asset(
            //       "assets/images/galery-5.png",
            //       width: globals.mw(context) * 0.32,
            //     ),
            //     Stack(
            //       children: <Widget>[
            //         Image.asset(
            //           "assets/images/galery-6.png",
            //           width: globals.mw(context) * 0.32,
            //         ),
            //         Positioned.fill(
            //           child: Align(
            //             alignment: Alignment.center,
            //             child: Container(
            //               width: globals.mw(context) * 0.32,
            //               height: double.infinity,
            //               color: Colors.black.withOpacity(0.65),
            //             ),
            //           ),
            //         ),
            //         Positioned.fill(
            //           child: Align(
            //               alignment: Alignment.center,
            //               child: Column(
            //                 mainAxisAlignment: MainAxisAlignment.center,
            //                 children: <Widget>[
            //                   globals.myText(
            //                       text: "Lihat", color: "light", size: 12),
            //                   globals.myText(
            //                       text: "Lainnya", color: "light", size: 12),
            //                 ],
            //               )),
            //         )
            //       ],
            //     )
            //   ],
            // ),
            SizedBox(
              height: 35,
            ),
          ],
        ),
      ),
    );
  }
}
