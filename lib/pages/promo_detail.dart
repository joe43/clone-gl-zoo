import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:gl_zoo/globals.dart' as globals;
import 'package:gl_zoo/models/promo_api.dart';
import 'package:gl_zoo/models/user_api.dart';
import 'package:gl_zoo/services/promo_service.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:gl_zoo/services/user_services.dart';

class PromoDetailPage extends StatefulWidget {
  @override
  _PromoDetailPageState createState() => _PromoDetailPageState();
}

class _PromoDetailPageState extends State<PromoDetailPage> {
  Promo promo;
  List<Promo> historyPromo;
  String publicLink = "http://www.glzoobackend.xyz/backend-gl-zoo/public/";
  double sliderVal = 0;
  bool isLoading;
  bool isPosting = false;
  int needToLoad;
  double initial;
  double move= 0.0;
  double diff = 0.0;
  User userData = User();
  _PromoDetailPageState() {
    needToLoad = 2;
    isLoading = true;
    readLocalData("userData").then((data){
      print(data);
      if(data != null){
        userData = User.fromJson(data);
      }else{
        userData.point = "??";
      }
      needToLoad--;
      if(needToLoad == 0)isLoading = false;
    });

    getPromoDetail("token", globals.detailPromoId).then((resp) {
      setState(() {
        promo = resp.data2[0];
        historyPromo = resp.data;
        needToLoad--;
        if(needToLoad == 0)isLoading = false;
      });
    }).catchError((onError) {
      globals.showDialogs(onError.toString(), context);
    });
    
  }

  void postPromo(){
    tukarPromo("token", promo.id, int.parse(promo.harga)).then((res){
      // isLoading = true;
      isPosting = false;
      globals.showDialogs("Berhasil menukar point!", context, title: "Success", route: "/promo");
      // getPromoDetail("token", globals.detailPromoId).then((resp) {
      //   setState(() {
      //     promo = resp.data2[0];
      //     historyPromo = resp.data;
      //     isLoading = false;
      //   });
      // }).catchError((onError) {
      //   globals.showDialogs(onError.toString(), context);
      // });
    }).catchError((onError){
      globals.showDialogs(onError.toString(), context);
    });
  }

  Widget _buildCardTop() {
    return Container(
        //height: 400,
        width: globals.mw(context)-10,
        margin: EdgeInsets.fromLTRB(10, 10, 10, 30),
        child: Card(
          elevation: 5,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: isLoading ? Column() : Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                globals.myText(text: promo.nama, weight: 'B', size: 16),
                globals.myText(text: "["+promo.harga+" Point]"),
                Divider(color: Colors.grey[300],),
                globals.myText(text: promo.deskripsi, align: TextAlign.left, ),
                SizedBox(height: 50,),

                Container(
                  width: globals.mw(context) - 50,
                  child: globals.myText(text: "Sisa Point Anda: "+userData.point,align: TextAlign.center,)),

                Container(
                  width: globals.mw(context) - 50,
                  child: globals.myText(text: "Geser Tombol untuk tukar Promo!",align: TextAlign.center)),
                
                Container(
                  width: globals.mw(context) - 50,
                  child: globals.myText(text: "*Penggeseran akan mengurangi jumlah point anda!",align: TextAlign.center, color: "danger", size: 12)),

                isPosting? globals.isLoading() : Container(
                  alignment: Alignment.centerLeft,
                  margin: EdgeInsets.fromLTRB(15,5,15,0),
                  width: globals.mw(context) - 50,
                  height: 50,
                  decoration: new BoxDecoration(
                      color: Colors.grey[400],
                      borderRadius: BorderRadius.all(Radius.circular(25)),
                  ),
                  child: GestureDetector(
                    onPanStart: (detail){
                      if(initial == null) initial = detail.globalPosition.dx;
                      print(initial);
                    },
                    onPanUpdate: (detail){
                      move = detail.globalPosition.dx;
                      setState(() {
                        diff = (move - initial) < 0 ? 0 : ( (move - initial) > globals.mw(context)-150 ? (globals.mw(context)-150) : (move - initial));
                      });
                      print(move);
                    },
                    onPanEnd: (detail){
                      setState(() {
                        diff = globals.mw(context)-150;
                        isPosting = true;
                      });
                      print("end");
                      postPromo();
                    },
                    child: Container(
                      padding: EdgeInsets.only(left: diff),
                      height: 50.0,
                      child: Chip(
                        backgroundColor: Theme.of(context).primaryColor,
                        label: Text("  >>  "),
                        labelStyle: TextStyle(
                          fontSize: 35,
                          color: Theme.of(context).primaryColorDark,
                        ),
                      ),
                    ),
                  ),
                ),
                
              ],
            ),
          ),
        ),
    );
  }

  Widget topImage(){
    return isLoading ? Container(
      padding: EdgeInsets.only(top: 10),
      width: 100,
      height: 100,
      child: globals.isLoading(),
    )
    : CachedNetworkImage(
      //height: globals.mw(context) * 0.5625,
      width: globals.mw(context),
      fit: BoxFit.contain,
      imageUrl: publicLink + promo.image,
      placeholder: (context, url) => Container(
        padding: EdgeInsets.only(top: 10),
        width: 100,
        height: 100,
        child: globals.isLoading(),
      ),
      errorWidget: (context, url, error){
        print(error.toString());
        return Container(
          child: Image.asset(
            "assets/images/broken_image.png",
            fit: BoxFit.fitHeight,
            //height: globals.mw(context) * 0.5625,
            width: double.infinity,
          ),
        );
      },
    );
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      resizeToAvoidBottomInset: true,
      bottomNavigationBar: globals.bottomAppBar(context),
      floatingActionButton: globals.floatingAppBar(context),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      appBar: globals.appBar(context, "Detail Promo"),
      body: ListView(
          children: <Widget>[
            topImage(),
            isLoading ? globals.isLoading()
            :_buildCardTop(),
            
          ],
        ),
    );
  }
}
