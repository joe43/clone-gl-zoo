import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:gl_zoo/globals.dart' as globals;
import 'package:package_info/package_info.dart';

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  String version = "v1.0.0";

  @override
  void initState() {
    super.initState();
    startTime();
  }

  void getVersion() {
    PackageInfo.fromPlatform().then((PackageInfo packageInfo) {
      setState(() {
        // version = packageInfo.version;
      });
    });
  }

  startTime() async {
    var _duration = Duration(seconds: 2);
    return Timer(_duration, navigationPage);
  }

  void navigationPage() {
    Navigator.of(context).pushReplacementNamed('/intro');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Stack(
          children: <Widget>[
            Container(
              color: Colors.green,
              height: double.infinity,
              width: double.infinity,
            ),
            Positioned(
              left: 20,
              right: 20,
              top: globals.mh(context) * 0.4,
              child: Image.asset(
                'assets/images/logo.png',
                height: 100,
              ),
            ),
            Positioned(
                left: 20,
                right: 20,
                top: globals.mh(context) * 0.7,
                child: Center(child: CircularProgressIndicator(backgroundColor: Colors.white,),)),
            Positioned(
                bottom: 0,
                child: Container(
                    height: 150,
                    width: globals.mw(context),
                    child: Image.asset(
                      "assets/images/grass.png",
                      fit: BoxFit.fitWidth,
                    ))),
            Positioned(
                left: 20,
                right: 20,
                bottom: 30,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    globals.myText(
                        text: "Copyright @ 2019",
                        color: "light",
                        size: 10,
                        weight: "M"),
                    globals.myText(
                        text: "Gembira Loka Zoo",
                        color: "light",
                        size: 18,
                        weight: "SB"),
                    globals.myText(text: version, color: "light", size: 9),
                  ],
                ))
          ],
        ),
      ),
    );
  }
}
