import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gl_zoo/services/user_services.dart';
import 'package:gl_zoo/globals.dart' as globals;
import 'package:cached_network_image/cached_network_image.dart';
import '../models/slider_api.dart';
import '../services/static_services.dart';

class IntroPage extends StatefulWidget {
  @override
  _IntroPageState createState() => _IntroPageState();
}

class _IntroPageState extends State<IntroPage> {
  int count = 0;

  List<IntroSlider> listSlider;

  bool isLoadingSlider = true;
  _IntroPageState() {
    getSlider("token").then((onValue) {
      SliderApi apiRespons = onValue;
      listSlider = apiRespons.data;
      
      setState(() {
        isLoadingSlider = false;
      });

    }).catchError((onError) {
      globals.showDialogs(onError.toString(), context);
    });
  }

  @override
  Widget build(BuildContext context) {
    count = 0;
    //SystemChrome.setEnabledSystemUIOverlays([]);
    return WillPopScope(
      onWillPop: () {
        SystemChannels.platform.invokeMethod('SystemNavigator.pop');
        return;
      },
      child: isLoadingSlider ? Container(
          padding: EdgeInsets.only(top: 10),
          width: globals.mw(context) * 0.8,
          child: globals.isLoading(),
        ) : DefaultTabController(
        length: listSlider.length,
        child: Builder(
            builder: (BuildContext context) => Stack(
                  children: <Widget>[
                    Container(
                      height: globals.mh(context),
                      width: globals.mw(context),
                      child: TabBarView(
                        children: listSlider.map((f) {
                          count++;
                          return Stack(
                            alignment: Alignment.center,
                            children: <Widget>[
                              Container(
                                color: Theme.of(context).primaryColor,
                                height: double.infinity,
                                width: double.infinity,
                              ),
                              Positioned.fill(
                                  top: globals.mh(context) * 0.1,
                                  child: Align(
                                    alignment: Alignment.topCenter,
                                    child: CachedNetworkImage(
                                        height: 260,
                                        width: globals.mw(context),
                                        imageUrl: f.image,
                                        placeholder: (context, url) => Container(
                                          padding: EdgeInsets.only(top: 10),
                                          width: 100,
                                          height: 100,
                                          child: CircularProgressIndicator(
                                            backgroundColor: Colors.redAccent,
                                          ),
                                        ),
                                        errorWidget: (context, url, error){
                                          print(error.toString());
                                          return Container(
                                            child: Image.asset(
                                              "assets/images/broken_image.png",
                                              fit: BoxFit.contain,
                                              height: 250,
                                            ),
                                          );
                                        },
                                      ),
                                  )),
                              Positioned(
                                  bottom: 0,
                                  child: Container(
                                      child: Image.asset(
                                    "assets/images/grass.png",
                                    fit: BoxFit.cover,
                                    height: globals.mh(context) * 0.5,
                                    width: globals.mw(context),
                                  ))),
                              // Caption
                              Positioned(
                                bottom: globals.mh(context) * 0.2,
                                child: Container(
                                  width: globals.mw(context) * 0.9,
                                  child: Column(
                                    children: <Widget>[
                                      globals.myText(
                                          text: f.title,
                                          color: "light",
                                          weight: "B",
                                          size: 20),
                                      SizedBox(height: 20),
                                      globals.myText(
                                          text: f.caption,
                                          color: "light",
                                          weight: "L",
                                          size: 12,
                                          align: TextAlign.center),
                                    ],
                                  ),
                                ),
                              ),
                              count == listSlider.length
                                  ? Positioned(
                                      bottom: 8,
                                      right: 20,
                                      child: GestureDetector(
                                        onTap: () {
                                          Navigator.pop(context);
                                          saveLocalData('isNewUser', "false");
                                          //saveLocalData('isNew', "true");
                                          globals.isNewUser = false;
                                          Navigator.of(context)
                                              .pushReplacementNamed('/');
                                        },
                                        child: Container(
                                            child: Image.asset(
                                          "assets/images/next.png",
                                          height: 60,
                                        )),
                                      ))
                                  : Container(),
                            ],
                          );
                        }).toList(),
                      ),
                    ),
                    Positioned(
                      bottom: 30,
                      left: 50,
                      right: 50,
                      child: Center(
                          child: TabPageSelector(
                        selectedColor: Theme.of(context).primaryColor,
                        indicatorSize: 10,
                        color: Color.fromRGBO(0, 49, 1, 1),
                      )),
                    )
                  ],
                )),
      ),
    );
  }
}
