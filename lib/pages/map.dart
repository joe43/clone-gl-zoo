import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:gl_zoo/globals.dart' as globals;
import 'package:webview_flutter/webview_flutter.dart';


class MapPage extends StatefulWidget {
  final url;
  MapPage(this.url);
  @override
  _MapPageState createState() => _MapPageState(this.url);
}

class _MapPageState extends State<MapPage> with SingleTickerProviderStateMixin{
  var _url;
  final _key = UniqueKey();
  _MapPageState(this._url);

  Future<bool> _willPopCallback() async {
      // await showDialog or Show add banners or whatever
      globals.currentMenu = "Home";
      Navigator.pushReplacementNamed(context, "/");
      // then
      return false; // return true if the route to be popped
  }
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => _willPopCallback(),
      child: SafeArea(
        child: Scaffold(
          backgroundColor: Colors.grey[200],
          bottomNavigationBar: globals.bottomAppBar(context),
          floatingActionButton: globals.floatingAppBar(context),
          floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
          //appBar: globals.appBar(context, "Gembira Loka Zoo"),
          body: Column(
            children: [
              Expanded(
                child: WebView(
                  key: _key,
                  javascriptMode: JavascriptMode.unrestricted,
                  initialUrl: _url
                )
              )
            ],
          )
        ),
      )
    );
  }
}
