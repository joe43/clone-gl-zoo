import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:gl_zoo/globals.dart' as globals;
import 'package:gl_zoo/services/user_services.dart';

class KontakKamiPage extends StatefulWidget {
  @override
  _KontakKamiPageState createState() => _KontakKamiPageState();
}

class _KontakKamiPageState extends State<KontakKamiPage> {
  final _formKey = GlobalKey<FormState>();
  String selectedTopic;
  Map<String, dynamic> _data = {
  "userid" : "0",
  "nama" : "Anonym",
	"email" : "test@email.com",
	"notelp" : "+6281234567890",
  "topik" : "{ topik }",
	"pesan" : "{ pesan }",
};
  Widget _buildNama() {
    return Container(
      margin: EdgeInsets.fromLTRB(40, 20, 40, 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          globals.myText(text: "Nama"),
          SizedBox(
            height: 8,
          ),
          TextFormField(
            style: TextStyle(color: Colors.black),
            keyboardType: TextInputType.emailAddress,
            decoration: InputDecoration(
                contentPadding: EdgeInsets.all(13),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10))),
            
            onSaved: (String value) {
              this._data["nama"] = value;
            },
          )
        ],
      ),
    );
  }

  Widget _buildPhone() {
    return Container(
      margin: EdgeInsets.fromLTRB(40, 20, 40, 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          globals.myText(text: "No HP"),
          SizedBox(
            height: 8,
          ),
          TextFormField(
            style: TextStyle(color: Colors.black),
            keyboardType: TextInputType.phone,
            decoration: InputDecoration(
                contentPadding: EdgeInsets.all(13),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10))),
            
            onSaved: (String value) {
              this._data["notelp"] = value;
            },
          )
        ],
      ),
    );
  }

  Widget _buildEmail() {
    return Container(
      margin: EdgeInsets.fromLTRB(40, 20, 40, 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          globals.myText(text: "Email"),
          SizedBox(
            height: 8,
          ),
          TextFormField(
            style: TextStyle(color: Colors.black),
            keyboardType: TextInputType.emailAddress,
            decoration: InputDecoration(
                contentPadding: EdgeInsets.all(13),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10))),
            validator: (text) {
              String ret;
              Pattern pattern =
                  r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
              RegExp regex = new RegExp(pattern);
              if (text == "" || text == null) {
                ret = "Email tidak boleh kosong";
              }
              if (!regex.hasMatch(text)) {
                ret = 'Email tidak valid';
              }
              return ret;
            },
            onSaved: (String value) {
              this._data["email"] = value;
            },
          )
        ],
      ),
    );
  }

  Widget _buildTopik() {
    List<String> item = [
      "Jam Buka",
      "Booking Tiket",
      "Call Center",
    ];
    return Container(
      margin: EdgeInsets.fromLTRB(40, 0, 40, 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          globals.myText(text: "Topik"),
          SizedBox(
            height: 8,
          ),
          DropdownButtonFormField<String>(
              value: selectedTopic,
              items: item.map((mapIS) {
                return DropdownMenuItem(
                  value: mapIS,
                  child: globals.myText(text: mapIS),
                );
              }).toList(),
              decoration: InputDecoration(
                isDense: true,
                labelText: "Pilih salah satu",
                labelStyle: TextStyle(
                  color: Color.fromRGBO(0, 0, 0, 0.8),
                  fontSize: 14,
                ),
                border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
              ),
              validator: (String selected) {
                String ret = "";
                if (selected == null) {
                  ret= "Topik tidak boleh kosong!";
                }else{
                  ret = null;
                }
                return ret;
              },
              onSaved: (String value) {
                this._data["topik"] = value;
              },
              onChanged: (value) {
                setState(() {
                  selectedTopic = value;
                });
              }),
              // onSaved: (String value) {
              //   this._data.pesan = value;
              // }
        ],
      ),
    );
  }

  Widget _buildIsi() {
    return Container(
      margin: EdgeInsets.fromLTRB(40, 0, 40, 0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          globals.myText(text: "Isi"),
          SizedBox(
            height: 8,
          ),
          TextFormField(
            style: TextStyle(color: Colors.black),
            maxLines: 5,
            decoration: InputDecoration(
                contentPadding: EdgeInsets.all(13),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10))),
            validator: (text) {
              String ret = "";
              if (text == "" || text == null) {
                ret = "Isi tidak boleh kosong";
              }else{
                ret = null;
              }
              return ret;
            },
            onSaved: (String value) {
              this._data["pesan"] = value;
            }
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          actions: <Widget>[
            Icon(
              Icons.search,
              size: 35,
            ),
            SizedBox(
              width: 10,
            ),
          ],
          title: globals.myText(text: "Kontak Kami", color: "light", size: 20),
          leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.pop(context);
              }),
        ),
        bottomNavigationBar: globals.bottomAppBar(context),
        floatingActionButton: globals.floatingAppBar(context),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        body: ListView(
          children: <Widget>[
            Card(
              margin: EdgeInsets.fromLTRB(15, 20, 15, 20),
              child: Column(
                children: <Widget>[
                  Container(
                    child: Image.asset(
                      "assets/images/kontak-kami.png",
                      height: 225,
                    ),
                  ),
                  Form(
                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        _buildNama(),
                        _buildPhone(),
                        _buildEmail(),
                        _buildTopik(),
                        _buildIsi()
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(40, 0, 40, 20),
                    child: RaisedButton(
                      color: Theme.of(context).primaryColor,
                      onPressed: () async {
                        _formKey.currentState.save();
                        if (_formKey.currentState.validate()) {
                          postMessage("token", _data).then((res){
                            globals.showDialogs(
                              "Terima kasih atas masukannya, Kami akan merespon secepatnya!",
                                context, route: "/");
                          }).catchError((onError){
                            globals.showDialogs(onError.toString(), context);
                          });
                          
                        }
                      },
                      child: globals.myText(text: "Kirim", color: "light"),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
