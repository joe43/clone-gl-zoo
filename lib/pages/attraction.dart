import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:gl_zoo/globals.dart' as globals;
import 'package:gl_zoo/models/activity_api.dart';
import 'package:gl_zoo/models/activity_type_api.dart';
import 'package:gl_zoo/models/my_collection_api.dart';
import 'package:gl_zoo/services/activity_service.dart';

import 'package:intl/date_symbol_data_local.dart';

class AttractionPage extends StatefulWidget {
  @override
  _AttractionPageState createState() => _AttractionPageState();
}

class _AttractionPageState extends State<AttractionPage> with SingleTickerProviderStateMixin{
  TabController controller1;
  int needToLoad = 0;
  bool isLoading = false;
  bool isLoadingActivity = true;
  int totalCollection = 0;
  MyCollectionApi myCollections;
  List<Anima> collections = [];
  
  ActivityApi allActivity;
  ActivityTypeApi allActivityType;
  ActivityApi selectedActivityTab = ActivityApi();
  ActivityApi filteredActivityTab = ActivityApi();
  List<ActivityType> typeList = [];

  String _menu;

  String publicLink = "http://www.glzoobackend.xyz/backend-gl-zoo/public/";
  // List<Widget> collections;
  // @override
  // void initState() {
  //   super.initState();
  //   controller1 = new TabController(vsync: this, length: 3);
  // }

  @override
  void dispose() {
    controller1.dispose();
    super.dispose();
  }

  _AttractionPageState() {
    needToLoad = 1;
    isLoading = true;
    _menu = "0";
    initializeDateFormatting("id_ID");

    getActivityType("token").then((resp){
      if(resp.data.length > 0){
        typeList = resp.data;
        controller1 = TabController(length: typeList.length, vsync: this);
        setState(() {
          isLoadingActivity = true;  
        });
        getActivityByType("token", typeList[0].id.toString()).then((resp){
          setState(() {
            selectedActivityTab = resp;
            //filteredActivityTab = ActivityApi.fromJson(selectedActivityTab.toJson());
            filterActivities();
            isLoadingActivity = false;
          });
        });
      }else{
        typeList = [ActivityType(
          id: 0,
          nama: "No Data"
        )];
      }
      setState(() {
        needToLoad--;
        if(needToLoad == 0)isLoading = false;
      });
    });

  }

  void filterActivities(){
    setState(() {
      filteredActivityTab = ActivityApi.fromJson(selectedActivityTab.toJson());
      filteredActivityTab.data.removeWhere((a){
        bool remove = true;
        // Duration diffStart = a.timeStart.difference(_selectedDate); 
        // Duration diffEnd = a.timeEnd.difference(_selectedDate); 
        // print("diffStart.inMilliseconds");
        // print(diffStart.inHours);
        // print("diffEnd.inMilliseconds");
        // print(diffEnd.inDays);
        // print(a.timeStart.day);
        // print(_selectedDate.day);
        // if(diffStart.inDays <= 0 && diffEnd.inDays >= 0){//If passed
        //   if(diffEnd.inDays == 0 && (a.timeEnd.day != _selectedDate.day)){
        //     remove = true;
        //   }else{
        //     remove = false;
        //   }
        // }
        print('a.namagroup');
        print(a.namagroup);
        print(typeList[int.parse(_menu)].nama);
        if(a.namagroup == typeList[int.parse(_menu)].nama){//If passed
          
          return false;
        }
        return remove;
      });
    });
  }

  Widget getEvent(String title, String location, DateTime start, DateTime end, String imageUrl, [int id = 0, String chip="Tutup", String description=""]){
    Color chipColor = Colors.grey[400];
    if(chip == "Buka"){
      chipColor = Theme.of(context).primaryColor;
    }

    return GestureDetector(
      onTap: () => _itemTapped(id),
      child: Container(
        padding: EdgeInsets.fromLTRB(8,8,0,8),
        decoration: BoxDecoration(
          color: chip == "Tutup" ? Colors.grey[300] : Colors.white,
          border: Border(
            bottom: BorderSide(color: Colors.grey[400]),
          ),
        ),
        width: globals.mw(context)*0.9,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                ClipRRect(
                  borderRadius: BorderRadius.circular(40),
                  child: CachedNetworkImage(
                    height: 75,
                    width: 75,
                    fit: BoxFit.fill,
                    imageUrl: publicLink + imageUrl,
                    placeholder: (context, url) => Container(
                      width: 75,
                      height: 75,
                      child: globals.isLoading(),
                    ),
                    errorWidget: (context, url, error){
                      print(error.toString());
                      return Container(
                        child: Image.asset('assets/images/broken_image.png',
                          height: 75,
                          width: 75,
                          colorBlendMode: BlendMode.saturation,
                          color: Colors.grey,
                        )
                      );
                    },
                  )
                ),
                SizedBox(
                  width: 10,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text(title,
                      softWrap: true,
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Text("Lokasi: $location",
                        //Text("$location (${start.hour}.${start.minute} - ${end.hour}.${end.minute})",
                          style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.normal,
                            color: Colors.grey
                          ),
                        ),
                      ],
                    ),
                    Container(
                      width: globals.mw(context)*0.9 - 121,
                      
                      child: Text(description.length > 90 ? description.substring(0,90)+"[. . .]": description,
                        softWrap: true,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 12,
                        ),
                      ),
                    ),

                    Row(
                      children: <Widget>[
                        Container(
                          height: 25,
                          padding: EdgeInsets.all(0),
                          margin: EdgeInsets.only(top: 10),
                          child: Transform(
                            transform: new Matrix4.identity()..scale(0.9),
                            child: Chip(
                              labelPadding: EdgeInsets.all(0),
                              padding: EdgeInsets.symmetric(horizontal: 15),
                              backgroundColor: chipColor,
                              label: Text(chip),
                              labelStyle: TextStyle(
                                height: 0,
                                fontSize: 12,
                                fontWeight: FontWeight.bold
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(0,7,0,0),
                          child: globals.myText(text: start.toString().split(" ")[1].split(".")[0] + " - " + end.toString().split(" ")[1].split(".")[0]),
                        ),
                      ],
                    ),
                  ]
                ),
                Spacer(),
              ],
            ),
          ],
        ),
      ),
    );
  }

  void _itemTapped(int id){
    globals.detailActivityId = id;
    Navigator.of(context).pushNamed('/atraksi/detail');
  }
  
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.grey[200],
        bottomNavigationBar: globals.bottomAppBar(context),
        floatingActionButton: globals.floatingAppBar(context),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        appBar: globals.appBar(context, "Kegiatan"),
        body: ListView(
          padding: EdgeInsets.all(0),
          children: <Widget>[
            //_cardCont("book_collection.png", 5, totalCollection),
            Container(
              margin: EdgeInsets.fromLTRB(10, 5, 10, 5),
              child: Card(
                elevation: 5,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: Column(
                  children: <Widget>[
                    Container(
                      height: 60,
                      padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
                      child:isLoading
                      ? Container(
                          child: globals.isLoading(),
                        )
                      : 
                      
                      TabBar(
                        //isScrollable: true,
                        indicator: ShapeDecoration(
                          color: Theme.of(context).primaryColor,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20.0),
                          ),
                        ),
                        indicatorColor: Theme.of(context).primaryColor,
                        indicatorWeight: 2.0,
                        indicatorSize: TabBarIndicatorSize.tab,
                        indicatorPadding: EdgeInsets.all(0),
                        unselectedLabelColor: Colors.grey[600],
                        labelColor: Colors.white,
                        controller: controller1,
                        isScrollable: true,
                        onTap: (idx) {
                            String typeId = typeList[idx].id.toString();
                            setState(() {
                              isLoadingActivity = true;
                            });
                            getActivityByType("token", typeId).then((resp){
                              setState(() {
                                _menu = '$idx';
                                selectedActivityTab = resp;
                                filterActivities();
                                isLoadingActivity = false;
                              });
                            });
                        },
                        tabs: List<Widget>.from(typeList.map((f){
                          return Text(f.nama, textAlign: TextAlign.center);
                        })),
                      ),
                    ),
                    isLoadingActivity
                    ? Container(
                        padding: EdgeInsets.only(top: 10),
                        width: globals.mw(context),
                        height: 175,
                        child: globals.isLoading(),
                      )
                    :Container(
                      padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: filteredActivityTab.data.length>0? List<Widget>.from(
                          filteredActivityTab.data.map((a){
                            List<String> x = a.days.split(';');
                            List<bool> days = [false,false,false,false,false,false,false];
                            x.forEach((day){
                              switch (day.toLowerCase()) {
                                case 'senin': days[0] = true; break;
                                case 'selasa': days[1] = true; break;
                                case 'rabu': days[2] = true; break;
                                case 'kamis': days[3] = true; break;
                                case 'jumat': days[4] = true; break;
                                case 'sabtu': days[5] = true; break;
                                case 'minggu': days[6] = true; break;
                                default:
                              }
                            });
                            DateTime date = DateTime.now();
                            int dateNum = date.weekday-1;
                            String chip = "Tutup";
                            if(days[dateNum]){chip = "Buka";}
                            return getEvent(a.nama, a.location, a.timeStart, a.timeEnd, a.thumbnail??'', a.id, chip, a.deskripsi);
                          })
                        ): <Widget>[Text("There's no Activity for this selection", style: TextStyle(
                          color: Colors.grey[600],
                          fontStyle: FontStyle.italic,
                        ),),
                          Divider(),
                        ],
                      ),
                    ),
                  ],
                )
              )
            ),
            
          ],
        ),
      ),
    );
  }
}
