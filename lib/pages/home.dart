import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:gl_zoo/globals.dart' as globals;
import 'package:carousel_slider/carousel_slider.dart';
import 'package:gl_zoo/models/logo_home.dart';
import 'package:gl_zoo/models/spotlight_api.dart';
import 'package:gl_zoo/models/static_api.dart';
import 'package:gl_zoo/models/user_api.dart';
import 'package:gl_zoo/services/static_services.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:gl_zoo/models/fb_profile.dart';
import 'package:gl_zoo/services/user_services.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;

import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';

// import 'package:share/share.dart';

import 'kontak_kami.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  LogoHome listLogo;
  HomeData homeData;
  List<SpotLight> spotLights;
  List<Widget> imageSpotlight = [];
  FbProfile fbUser;
  
  // bool isLogin = false;
  String homeContact;
  String homeVisit;
  bool isLoadingLogo = true;
  int needToLoad = 3;
  User userData;
  
  @override
  void initState() {
    super.initState();
    initDynamicLinks(context);
  }

  _HomePageState() {
    print("action - page - id 3");
    readLocalData("userLogin").then((data){
      if(data != null && data != ""){
        setState(() {
          fbUser = FbProfile.fromJson(data);
          globals.welcomeName = fbUser.name;
          globals.isLogin = true;
          getUser("token").then((val){
            setState(() {
              userData = val.data;
              saveLocalData("userData", userData.toJson());
            });    
          });
        });
      }
    });
    getIconHome("token").then((onValue) {
      listLogo = onValue;
      setState(() {
        needToLoad--;
        if(needToLoad == 0)isLoadingLogo = false;
      });
    }).catchError((onError) {
      globals.showDialogs(onError.toString(), context);
    });

    getStatic("token").then((onValue) {
      if(onValue.data.length > 0){
        homeData = onValue.data[0];
        setState(() {
          globals.wifiPass = homeData.pass;
          globals.lat = homeData.lat;
          globals.long = homeData.lng;
          homeContact = "Phone: " + ((homeData.phone != "" && homeData.phone != null ? homeData.phone: "-"));
          homeContact += "\nEmail: " + ((homeData.email != "" && homeData.email != null ? homeData.email: "-"));
          homeContact += "\n\t ";
          homeVisit = "Address: " + ((homeData.address != "" && homeData.address != null ? homeData.address: "-"));
          // homeVisit += "\nFacebook: " + ((homeData.facebook != "" && homeData.facebook != null ? homeData.facebook: "-"));
          // homeVisit += "\nWhatsApp: " + ((homeData.whatsapp != "" && homeData.whatsapp != null ? homeData.whatsapp: "-"));
          // homeVisit += "\nInstagram: " + ((homeData.instagram != "" && homeData.instagram != null ? homeData.instagram: "-"));
          homeVisit += "\n\t ";
          needToLoad--;
          if(needToLoad == 0)isLoadingLogo = false;
        });
      }else{
        setState(() {
          homeData = HomeData();
          homeData.background = "-";
          homeData.logo = "-";
          homeContact = "Phone: -";
          homeContact += "\nEmail: -";
          homeContact += "\n\t ";
          homeVisit = "Address: -";
          // homeVisit += "\nFacebook: -";
          // homeVisit += "\nWhatsApp: -";
          // homeVisit += "\nInstagram: -";
          homeVisit += "\n\t ";
          needToLoad--;
          if(needToLoad == 0)isLoadingLogo = false;
        });
      }
      
    }).catchError((onError) {
      globals.showDialogs(onError.toString(), context);
        homeContact = "Phone: -";
        homeContact += "\nEmail: -";
        homeContact += "\n\t ";
        homeVisit = "Address: -";
        // homeVisit += "\nFacebook: -";
        // homeVisit += "\nWhatsApp: -";
        // homeVisit += "\nInstagram: -";
        homeVisit += "\n\t ";
    });

    getSpotlight("token").then((onValue) {
      spotLights = onValue.data;
      if(spotLights.length > 0){
        spotLights.forEach((f){
          int find = 1;
          Widget tamp = Column(
            children: <Widget>[
              GestureDetector(
                onTap: () => _itemTapped(f.id),
                child: Stack(
                  children: <Widget>[
                    CachedNetworkImage(
                      // height: 50,
                      fit: BoxFit.fill,
                      imageUrl: f.image,
                      height: 225,
                      width: double.infinity,
                      placeholder: (context, url) => Container(
                        width: double.infinity,
                        height: 225,
                        child: globals.isLoading(),
                      ),
                      errorWidget: (context, url, error){
                        print(error.toString());
                        return Container(
                          child: Image.asset(
                            "assets/images/home-slider-1.png",
                            height: 225,
                            width: double.infinity,
                            fit: BoxFit.fill,
                          ),
                        );
                      },
                    ),
                    
                    // Image.asset(
                    //   "assets/images/home-slider-1.png",
                    //   fit: BoxFit.fill,
                    // ),
                    Positioned(
                        bottom: 0,
                        left: 0.0,
                        right: 0.0,
                        child: Container(
                          color: Color.fromRGBO(91, 176, 91, 1),
                          height: 65,
                        )),
                    Positioned(
                      bottom: 20,
                      left: 10,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          globals.myText(
                              text: f.nama,
                              color: "light",
                              weight: "XB",
                              size: 12),
                          globals.myText(
                              text: f.deskripsi.length > 100 ? f.deskripsi.substring(0,50)+'... [More]' : f.deskripsi,
                              color: "light",
                              size: 11),
                          globals.myText(
                              text: "From: "+f.timeStart +" To: " + f.timeEnd,
                              color: "light",
                              size: 11),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          );
          setState(() {
            imageSpotlight.add(tamp);
          });
        });
      }else{
        setState(() {
          imageSpotlight = imageHome;  
        });
      }
      setState(() {
        needToLoad--;
        if(needToLoad == 0)isLoadingLogo = false;
      });
    }).catchError((onError) {
      globals.showDialogs(onError.toString(), context);
    });
  }

  void _itemTapped(int id){
    globals.detailSpotLightId = id;
    Navigator.of(context).pushNamed('/event/detail');
  }

  int _current = 0;
  List<Widget> imageHome = [
    Column(
      children: <Widget>[
        Stack(
          children: <Widget>[
            Image.asset(
              "assets/images/home-slider-1.png",
              fit: BoxFit.cover,
            ),
            Positioned(
                bottom: 0,
                left: 0.0,
                right: 0.0,
                child: Container(
                  color: Color.fromRGBO(91, 176, 91, 1),
                  height: 50,
                )),
            Positioned(
              bottom: 10,
              left: 10,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  globals.myText(
                      text: "Menunggangi Gajah",
                      color: "light",
                      weight: "M",
                      size: 12),
                  globals.myText(
                      text: "Lorem Ipsum dolor si amet",
                      color: "light",
                      size: 11)
                ],
              ),
            ),
          ],
        )
      ],
    ),
    Column(
      children: <Widget>[
        Stack(
          children: <Widget>[
            Image.asset(
              "assets/images/home-slider-1.png",
              fit: BoxFit.cover,
            ),
            Positioned(
                bottom: 0,
                left: 0.0,
                right: 0.0,
                child: Container(
                  color: Color.fromRGBO(91, 176, 91, 1),
                  height: 50,
                )),
            Positioned(
              bottom: 10,
              left: 10,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  globals.myText(
                      text: "Menunggangi Gajah",
                      color: "light",
                      weight: "M",
                      size: 12),
                  globals.myText(
                      text: "Lorem Ipsum dolor si amet",
                      color: "light",
                      size: 11)
                ],
              ),
            ),
          ],
        )
      ],
    ),
  ];

  Widget _contIconCard(Datum logo) {
    return GestureDetector(
      onTap: () {
        switch (logo.name) {
          case "galeri":
            Navigator.of(context).pushNamed('/galeri');
            break;
          case "jadwal":
            Navigator.of(context).pushNamed('/jadwal');
          break;
          case "koleksi":
            Navigator.of(context).pushNamed('/koleksi');
          break;
          case "aktivitas":
            Navigator.of(context).pushNamed('/atraksi');
          break;
          case "feeding":
            Navigator.of(context).pushNamed('/feeding');
          break;
          case "fasilitas":
            Navigator.of(context).pushNamed('/facility');
          break;
          case "profile":
            if(globals.isLogin)
              Navigator.of(context).pushNamed('/profile');
            else{
              globals.loginDialogs(context, 'profile').then((login){
                if(login != null && login){
                  print("Login Success");
                  globals.isLogin = true;
                  Navigator.of(context).pushNamed('/profile');
                }else{
                  globals.showDialogs("Login Failed", context);
                  print("Login Fail");
                }
              });
            }
          break;
          case "poin":
            Navigator.of(context).pushNamed('/promo');
          break;

          default:
            //Share.share('check out my website https://gembiraloka.page.link/map');
            globals.showDialogs("Maaf, menu tidak terdaftar!", context);
        }
      },
      child: Container(
          decoration: BoxDecoration(
              border: Border.all(width: 0.5, color: Colors.grey[100])),
          padding: EdgeInsets.fromLTRB(20, 3, 20, 3),
          child: Column(
            children: <Widget>[
              CachedNetworkImage(
                height: 35,
                width: 40,
                imageUrl: logo.image,
                placeholder: (context, url) => Container(
                  padding: EdgeInsets.only(left: 2.5),
                  width: 35,
                  height: 35,
                  child: globals.isLoading(),
                ),
                errorWidget: (context, url, error) => Icon(
                  Icons.error,
                  size: 40,
                ),
              ),
              SizedBox(
                height: 5,
              ),
              globals.myText(text: logo.name.toUpperCase(), size: 9)
            ],
          )),
    );
  }

  Widget _buildCardTop() {
    return Positioned(
      bottom: 0,
      child: Container(
        margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
        child: Card(
          elevation: 5,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: 50,
              ),
              globals.myText(text: "Selamat datang, ${globals.welcomeName}!"),
              SizedBox(
                height: 10,
              ),
              isLoadingLogo
                  ? Container(
                      padding: EdgeInsets.only(top: 10),
                      width: globals.mw(context) * 0.8,
                      child: globals.isLoading(),
                    )
                  : Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        _contIconCard(listLogo.data[7]),
                        _contIconCard(listLogo.data[6]),
                        _contIconCard(listLogo.data[5]),
                        _contIconCard(listLogo.data[4]),
                      ],
                    ),
              isLoadingLogo
                  ? Container(
                      height: 70,
                    )
                  : Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        _contIconCard(listLogo.data[3]),
                        _contIconCard(listLogo.data[2]),
                        _contIconCard(listLogo.data[1]),
                        _contIconCard(listLogo.data[0]),
                      ],
                    ),
              SizedBox(
                height: 10,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildProfileImage() {
    return Positioned.fill(
      top: 120,//globals.mh(context) * 0.12,
      child: Align(
        alignment: Alignment.topCenter,
        child: Container(
            height: 100, child: globals.urlProfile == null?
              Image.asset('assets/images/profile_image.png'):
              ClipRRect(
                  borderRadius: BorderRadius.circular(50),
                  child:Image(
                    image: AdvancedNetworkImage(globals.urlProfile),
                  ),
                ),
              ),
      ),
    );
  }

  Widget _buildCarousel() {
    int idx = -1;
    return ClipRRect(
        borderRadius: BorderRadius.circular(10),
        child: isLoadingLogo
          ? Container(
              padding: EdgeInsets.only(top: 10),
              width: globals.mw(context) * 0.8,
              height: 225,
              child: globals.isLoading(),
            )
          :Stack(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(bottom: 0),
              child: CarouselSlider(
                aspectRatio: 3,
                autoPlay: true,
                viewportFraction: 1.0,
                height: 225,
                enableInfiniteScroll: true,
                onPageChanged: (index) {
                  setState(() {
                    _current = index;
                  });
                },
                items: imageSpotlight,
              ),
            ),
            Positioned(
                bottom: 5,
                right: 10,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: imageSpotlight.map((f) {
                    idx++;
                    return _buildDoted(idx);
                  }).toList(),
                )),
          ],
        ));
  }

  Widget _buildDoted(int index) {
    return Container(
      margin: EdgeInsets.fromLTRB(3, 0, 3, 0),
      height: 10,
      width: 10,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(100),
        color: index == _current ? Colors.white : Colors.green[800],
      ),
    );
  }

  Widget _cardCont(String title, String subtitle, String image) {
    double elevate = 5;
    // if(title == "Kunjungi Kami"){
    //   elevate = 0;
    // }
    return GestureDetector(
      onTap: () {
        if (title == "Kontak Kami") {
          Navigator.of(context).pushNamed('/kontak-kami');
        } else if(title == "Kunjungi Kami" || title == "SosMed"){
          openGLMap();
        }else if(title == "Telpon Darurat"){
          String no = homeData.phone??'+6281234567890';
          UrlLauncher.launch("tel:$no");
        }else{
          globals.showDialogs("Sedang dalam tahap pengembangan", context);
        }
      },
      child: Container(
        margin: EdgeInsets.fromLTRB(15, 5, 15, 5),
        child: Card(
          elevation: elevate,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          child: title=='SosMed'?
          Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              GestureDetector(
                onTap: () => UrlLauncher.launch(homeData.facebook),
                child: Container(
                  padding: EdgeInsets.symmetric(vertical: 10),
                  child: Image.asset(
                          "assets/images/001-facebook.png",
                          height: 30,
                        ),
                ),
              ),
              GestureDetector(
                onTap: () => UrlLauncher.launch("https://www.youtube.com"),
                child: Container(
                  padding: EdgeInsets.symmetric(vertical: 5),
                  child: Image.asset(
                          "assets/images/002-youtube.png",
                          height: 35,
                        ),
                ),
              ),
              GestureDetector(
                onTap: () => UrlLauncher.launch(homeData.instagram),
                child: Container(
                  padding: EdgeInsets.symmetric(vertical: 10),
                  child: Image.asset(
                          "assets/images/003-instagram.png",
                          height: 30,
                        ),
                ),
              ),
            ],
          )
          // Column(
          //   mainAxisSize: MainAxisSize.min,
          //   mainAxisAlignment: MainAxisAlignment.start,
          //   crossAxisAlignment: CrossAxisAlignment.start,
          //   children: <Widget>[
          //     GestureDetector(
          //       onTap: () => UrlLauncher.launch(homeData.facebook),
          //       child: ListTile(
          //         leading: Image.asset(
          //           "assets/images/001-facebook.png",
          //           height: 30,
          //         ),
          //         title: globals.myText(text: "Facebook", weight: "B"),
          //         subtitle:
          //           globals.myText(text: homeData.facebook, size: 11, color: "unprime"),
          //       ),
          //     ),
          //     // GestureDetector(
          //     //   onTap: () => UrlLauncher.launch(homeData.instagram),
          //     //   child:
          //         ListTile(
          //           leading: Image.asset(
          //             "assets/images/002-youtube.png",
          //             height: 30,
          //           ),
          //           title: globals.myText(text: "Youtube", weight: "B"),
          //           subtitle:
          //             globals.myText(text: '-', size: 11, color: "unprime"),
          //         ),
          //     // ),
          //     GestureDetector(
          //       onTap: () => UrlLauncher.launch(homeData.instagram),
          //       child:
          //       ListTile(
          //         leading: Image.asset(
          //           "assets/images/003-instagram.png",
          //           height: 30,
          //         ),
          //         title: globals.myText(text: "Instagram", weight: "B"),
          //         subtitle:
          //           globals.myText(text: homeData.instagram, size: 11, color: "unprime"),
          //       ),
          //     ),
          //   ],
          // )
          :ListTile(
            leading: Image.asset(
              "assets/images/$image",
              height: 40,
            ),
            title: globals.myText(text: title, weight: "B"),
            subtitle:
                globals.myText(text: subtitle, size: 11, color: "unprime"),
          ),
        ),
      ),
    );
  }

  Widget _footer(image){
    return GestureDetector(
      onTap: (){
        openGLMap();
      },
      child: Container(
        color: Theme.of(context).primaryColor,
        margin: EdgeInsets.only(top: 10),
        padding: EdgeInsets.fromLTRB(15, 5, 15, 30),
        child: Card(
          color: Theme.of(context).primaryColorDark,
          elevation: 0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          child: ListTile(
            leading: ImageIcon(
              AssetImage("assets/images/$image"),
              color: Colors.white,
              size: 40,
            ),
            // Image.asset(
            //   "assets/images/$image",
            //   height: 40,
            // ),
            title: globals.myText(text: "Kunjungi Kami", weight: "B", color: 'light'),
            subtitle:
                globals.myText(text: homeVisit, size: 11, color: 'light'),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => globals.willExit(context),
      child: Scaffold(
      backgroundColor: Colors.grey[200],
      resizeToAvoidBottomInset: true,
      bottomNavigationBar: globals.bottomAppBar(context),
      floatingActionButton: globals.floatingAppBar(context),
      // floatingActionButton: FloatingActionButton(
      //   onPressed: () {
      //     if(!isLogin){
      //       globals.loginDialogs(context).then((login){
      //         if(login != null && login){
      //           print("Login Success");
      //           readLocalData("userLogin").then((data){
      //             setState(() {
      //               print(data);
      //               fbUser = FbProfile.fromJson(data);
      //               welcomeName = fbUser.name;
      //               isLogin = true;
      //               print(fbUser);
      //             });
      //           });
      //         }else{
      //           globals.showDialogs("Login Failed", context);
      //           print("Login Fail");
      //         }
      //       });
      //     }else{
      //       globals.showDialogs("Zoo Map is Under Development, please be patient", context);
      //     }
      //   },
      //   backgroundColor: Theme.of(context).primaryColor,
      //   child: Icon(
      //     Icons.location_on,
      //     color: Colors.white,
      //     size: 40,
      //   ),
      // ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      body: ListView(
        children: <Widget>[
          Stack(
            alignment: Alignment.center,
            children: <Widget>[
              Padding(///////////////////
                padding: const EdgeInsets.only(bottom: 160),
                child: isLoadingLogo
                  ? Container(
                      width: 220,
                      height: 220,
                      child: globals.isLoading(),
                    )
                  : CachedNetworkImage(
                  height: 220,
                  width: globals.mw(context),
                  fit: BoxFit.fill,
                  imageUrl: homeData.background,
                  placeholder: (context, url) => Container(
                    width: 220,
                    height: 220,
                    child: globals.isLoading(),
                  ),
                  errorWidget: (context, url, error){
                    print(error.toString());
                    return Container(
                      child: Image.asset(
                        "assets/images/home1.jpg",
                        fit: BoxFit.fitHeight,
                        height: 220,
                        width: double.infinity,
                      ),
                    );
                  },
                ),
                // Image.asset(
                //   "assets/images/home1.jpg",
                //   height: 220,
                //   width: double.infinity,
                //   fit: BoxFit.fitHeight,
                // ),
              ),
              _buildCardTop(),
              _buildProfileImage(),
              Positioned.fill(
                top: 20,
                left: 20,
                child: Align(
                  alignment: Alignment.topLeft,
                  child: isLoadingLogo
                  ? Container(
                      width: 50,
                      height: 50,
                      child: globals.isLoading(),
                    )
                  : CachedNetworkImage(
                    height: 50,
                    imageUrl: homeData.logo,
                    placeholder: (context, url) => Container(
                      width: 50,
                      height: 50,
                      child: globals.isLoading(),
                    ),
                    errorWidget: (context, url, error){
                      print(error.toString());
                      return Container(
                        child: Image.asset(
                          'assets/images/logo.png',
                          height: 50,
                        ),
                      );
                    },
                  ),
                  // Container(
                  //   child: Image.asset(
                  //     'assets/images/logo.png',
                  //     height: 50,
                  //   ),
                  // ),
                ),
              ),
              // Positioned.fill(
              //   top: 20,
              //   right: 20,
              //   child: Align(
              //     alignment: Alignment.topRight,
              //     child: Container(
              //         child: Icon(
              //       Icons.search,
              //       color: Colors.white,
              //       size: 35,
              //     )),
              //   ),
              // )
            ],
          ),
          Container(
              margin: EdgeInsets.fromLTRB(20, 20, 20, 10),
              child: _buildCarousel()),
          _cardCont("Kontak Kami", homeContact,
              "home-kontak.png"),
          _cardCont("Telpon Darurat", 'Telpon kami jika anda mengalami keadaan darurat!',
              "emergency.png"),
          isLoadingLogo
          ? Container(
              padding: EdgeInsets.only(top: 10),
              width: globals.mw(context) * 0.8,
              height: 220,
              child: globals.isLoading(),
            )
          : _cardCont("SosMed", '',""),
          // _footer("visit_us.png"),
          _cardCont("Kunjungi Kami", homeVisit, "visit_us.png"),
          // _cardCont("Tersedia Kursi Roda", "Kami menyediakan fasilitas pendukung untuk Anda yang memiliki kebutuhan khusus.",
          //     "home-kursi-roda.png"),
          SizedBox(
            height: 25,
          ),
        ],
      ),
    ));
  }

  void initDynamicLinks(context) async {
    final PendingDynamicLinkData data = await FirebaseDynamicLinks.instance.getInitialLink();
    final Uri deepLink = data?.link;

    if (deepLink != null) {
      print("init deepLink.path");
      print(deepLink.path);
      Navigator.pushNamed(context, deepLink.path);
    }

    FirebaseDynamicLinks.instance.onLink(
      onSuccess: (PendingDynamicLinkData dynamicLink) async {
        final Uri deepLink = dynamicLink?.link;

        if (deepLink != null) {
          print("onlink deepLink.path");
          print(deepLink.query);
          print(deepLink.path);
          // Navigator.pushNamed(context, '/map');
          pushRouteAppLink(context, deepLink.path);
        }
      },
      onError: (OnLinkErrorException e) async {
        print('onLinkError');
        print(e.message);
      }
    );
  }
  //-7.8056242,110.3967592
  //-7.8041254,110.3980212
  //-7.8041252,110.3958328
  static Future<void> openGLMap([double latitude=-7.8041254, double longitude=110.3980212]) async {
    // String googleUrl = 'https://www.google.com/maps/search/?api=1&query=$latitude,$longitude';
    String glZooUrl = 'https://www.google.com/maps/search/?api=1&query='+ globals.lat + ',' + globals.long;
    if (await UrlLauncher.canLaunch(glZooUrl)) {
      await UrlLauncher.launch(glZooUrl);
    } else {
      throw 'Could not open the map.';
    }
  }

  void pushRouteAppLink(BuildContext context, String page, [String action="view", int id = 0]) {
    if (action == "view") {
      switch (page) {
        case "/map":
          print("$action - $page - $id 1");
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (BuildContext context) => KontakKamiPage()
            )
          );
          print("$action - $page - $id 2");
          break;
        default:
      }
    }
  }
}
