import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:gl_zoo/globals.dart' as globals;
// import 'package:carousel_slider/carousel_slider.dart';
// import 'package:flutter_masked_text/flutter_masked_text.dart';
// import 'package:gl_zoo/models/logo_home.dart';
// import 'package:gl_zoo/models/spotlight_api.dart';
// import 'package:gl_zoo/models/static_api.dart';
// import 'package:gl_zoo/services/static_services.dart';
// import 'package:cached_network_image/cached_network_image.dart';

import 'package:gl_zoo/models/my_collection_api.dart';
import 'package:gl_zoo/services/collection_service.dart';
import 'package:percent_indicator/percent_indicator.dart';

class CollectionPage extends StatefulWidget {
  @override
  _CollectionPageState createState() => _CollectionPageState();
}

class _CollectionPageState extends State<CollectionPage> with SingleTickerProviderStateMixin{
  TabController controller1;
  int needToLoad = 0;
  bool isLoading = false;
  int totalCollection = 0;
  MyCollectionApi myCollections;
  List<Anima> collections = [];
  List<String> typeList = [];
  String publicLink = "http://www.glzoobackend.xyz/backend-gl-zoo/public/";
  // List<Widget> collections;
  // @override
  // void initState() {
  //   super.initState();
  //   controller1 = new TabController(vsync: this, length: 3);
  // }

  @override
  void dispose() {
    controller1.dispose();
    super.dispose();
  }

  _CollectionPageState() {
    needToLoad = 2;
    isLoading = true;
    
    getTotalCollection("token").then((resp){
      if(resp.hasil){
        // Do Something ...
        setState(() {
          totalCollection = resp.data;
        });
      }
      setState(() {
        needToLoad--;
        if(needToLoad == 0){ 
          isLoading = false;
          initCollectionTab("Semua");
          initTabName(0);
        }
      });
    });

    getMyCollection("token").then((resp){
      if(resp.result == "ok"){
        // Do Something ...
        setState(() {
          myCollections = resp;
        });
      }
      setState(() {
        needToLoad--;
        if(needToLoad == 0){ 
          isLoading = false;
          initCollectionTab("All");
          initTabName(0);
          controller1 = TabController(length: typeList.length, vsync: this);
        }
      });
    });

  }
  void initTabName(idx){
    typeList = ["Semua"];
    if(myCollections.animas.length > 0){
      myCollections.animas.forEach((anima){
        if(!typeList.contains(anima.categoryname)){
          typeList.add(anima.categoryname);
        }
      });
    }
    // typeList.add("Unggas");
    // typeList.add("Reptile");
    // typeList.add("Hewan Pengerat");
    // typeList.add("Hewan Buas");
    // typeList.add("Hewan Laut");
    
    typeList[idx] = typeList[idx] + "\r\n" + collections.length.toString() + "/" + totalCollection.toString();
  }
  void initCollectionTab(String tab){
    if(tab == "Semua"){
      collections = myCollections.animas;
    }else{
      collections = [];
      myCollections.animas.forEach((anima){
        if(anima.categoryname == tab)
        collections.add(anima);
      });
    }
    // switch (tab) {
    //   case "1":
    //     collections = List<Anima>.from(myCollections.dataarray.unggas.whereType());
    //     break;
    //   case "2":
    //     collections = List<Anima>.from(myCollections.dataarray.reptile.whereType());
    //     break;
    //   case "3":
    //     collections = List<Anima>.from(myCollections.dataarray.hewanPengerat.whereType());
    //     break;
    //   case "4":
    //     collections = List<Anima>.from(myCollections.dataarray.hewanBuas.whereType());
    //     break;
    //   case "5":
    //     collections = List<Anima>.from(myCollections.dataarray.hewanLaut.whereType());
    //     break;
    //   default:
    //   print("collections.length");
    //   print(collections.length);
    //     collections = List<Anima>.from(myCollections.dataarray.unggas.whereType())
    //     ..addAll(myCollections.dataarray.hewanPengerat.whereType())
    //     ..addAll(myCollections.dataarray.hewanBuas.whereType())
    //     ..addAll(myCollections.dataarray.hewanLaut.whereType())
    //     ..addAll(myCollections.dataarray.reptile.whereType());
    //     print(collections.length);
    // }
  }

  List<Widget> collectionTab(int active, int total){
    var tamp = <Widget>[];
    var count = active;
    for (var i = 0; i < total; i++) {
      if(count > 0){

        count--;
      }else{
        tamp.add(
          Container(
            margin: EdgeInsets.fromLTRB(15, 5, 15, 5),
            child: ImageIcon(
              AssetImage("assets/images/profile_image.png"),
              color: Colors.grey,
              size: 80,
            )
          ),
        );
      }
    } 
    return tamp;
  }

  Widget _cardCont(String assetIcon, int collected, int total, [String satuan="satwa"]) {
    double elevate = 5;
    bool complete = false;
    if(collected == total){
      complete = true;
    }

    return Container(
      margin: EdgeInsets.fromLTRB(10, 5, 10, 5),
      child: Card(
        elevation: elevate,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: ListTile(
          contentPadding: EdgeInsets.fromLTRB(5,10,5,10),
          dense: true,
          leading: ImageIcon(
            AssetImage("assets/images/$assetIcon"),
            size: 50,
            color: complete? Theme.of(context).primaryColor: Colors.grey,
          ),
          title: isLoading
            ? Container(
                child: globals.isLoading(),
              )
            : Container(
              padding: EdgeInsets.all(0),
              margin: EdgeInsets.all(0),
              // color: Colors.green,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.fromLTRB(7,0,7,0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Text(
                        (100*collected / total).toStringAsFixed(2) + "%",
                        style: TextStyle(
                          fontSize: 24,
                          color: Theme.of(context).primaryColor
                        ),
                      ),
                      Text(
                        collected.toString() +"/"+ total.toString() + " $satuan",
                        style: TextStyle(
                          fontSize: 12,
                          wordSpacing: 3,
                          color: Colors.grey
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  width: double.infinity,
                  child: LinearPercentIndicator(
                    //width: 100.0,
                    lineHeight: 5.0,
                    percent: (collected/total) > 1 ? 1 : (collected/total),
                    progressColor: Theme.of(context).primaryColor,
                  ),
                ),
                
              ],
            ),
          ),
          // subtitle:
          //     globals.myText(text: subtitle, size: 11, color: "unprime"),
        ),
      ),
    );
  }

  Widget _buildGridItems(BuildContext context, int index) {
    return GestureDetector(
      onTap: () => _gridItemTapped(index),
      child: GridTile(
        child: Container(
          padding: EdgeInsets.all(0),
          margin: EdgeInsets.all(0),
          child: Center(
            child: _buildGridItem(index),
          ),
        ),
      ),
    );
  }

  Widget _buildGridItem(int idx) {
    if(idx >= collections.length){
      return ClipRRect(
        borderRadius: BorderRadius.circular(45),
        child: Image.asset('assets/images/profile_image.png',
          height: 90,
          width: 90,
          colorBlendMode: BlendMode.color,
          color: Colors.grey[700],
        )
      );
    }else{
      return ClipRRect(
        borderRadius: BorderRadius.circular(45),
        child: CachedNetworkImage(
          height: 90,
          width: 90,
          fit: BoxFit.fill,
          imageUrl: publicLink + collections[idx].thumbnail,
          placeholder: (context, url) => Container(
            width: 90,
            height: 90,
            child: globals.isLoading(),
          ),
          errorWidget: (context, url, error){
            print(error.toString());
            return Container(
              child: Image.asset('assets/images/broken_image.png',
                height: 90,
                width: 90,
                colorBlendMode: BlendMode.saturation,
                color: Colors.grey,
              )
            );
          },
        )
      );
    }
  }

  void _gridItemTapped(idx){
    if(idx < collections.length){
      print(collections[idx].animaid);
      globals.detailAnimaId = collections[idx].animaid;
      Navigator.of(context).pushNamed('/koleksi/detail');
    }
  }
  
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.grey[200],
        bottomNavigationBar: globals.bottomAppBar(context),
        floatingActionButton: globals.floatingAppBar(context),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        appBar: globals.appBar(context, "Koleksi Satwa"),
        body: ListView(
          padding: EdgeInsets.all(0),
          children: <Widget>[
            _cardCont("book_collection.png", collections.length, totalCollection),
            Container(
              margin: EdgeInsets.fromLTRB(10, 5, 10, 5),
              child: Card(
                elevation: 5,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: Column(
                  children: <Widget>[
                    Container(
                      height: 60,
                      padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
                      child:isLoading
                      ? Container(
                          child: globals.isLoading(),
                        )
                      : 
                      TabBar(
                        isScrollable: true,
                        indicator: ShapeDecoration(
                          color: Theme.of(context).primaryColor,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                        ),
                        indicatorColor: Theme.of(context).primaryColor,
                        indicatorWeight: 2.0,
                        indicatorSize: TabBarIndicatorSize.tab,
                        indicatorPadding: EdgeInsets.all(0),
                        unselectedLabelColor: Colors.grey[600],
                        labelColor: Colors.white,
                        controller: controller1,
                        onTap: (idx) {
                            // String typeId = typeList[idx].id.toString();
                            setState(() {
                              print(idx);
                              initCollectionTab(typeList[idx]);
                              initTabName(idx);
                            });
                            // getActivityByType("token", typeId).then((resp){
                            //   setState(() {
                            //     _menu = '$idx';
                            //     selectedActivityTab = resp;
                            //     filterActivities();
                            //     isLoadingActivity = false;
                            //   });
                            // });
                        },
                        tabs: List<Widget>.from(typeList.map((f){
                          return Text(f, textAlign: TextAlign.center);
                        })),
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.all(0),
                      margin: const EdgeInsets.all(15),
                      // decoration: BoxDecoration(
                      //   border: Border.all(color: Colors.black, width: 2.0)
                      // ),
                      height: globals.mh(context) - 300,
                      child: GridView.builder(
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 3,
                        ),
                        itemBuilder: _buildGridItems,
                        itemCount: totalCollection,
                      ),
                    ),
                  ]
                )
              )
            ),
            
          ],
        ),
      ),
    );
  }
}
