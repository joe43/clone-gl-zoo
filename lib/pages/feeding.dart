import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:gl_zoo/globals.dart' as globals;
import 'package:gl_zoo/models/activity_api.dart';
import 'package:gl_zoo/models/activity_type_api.dart';

import 'package:gl_zoo/models/my_collection_api.dart';
import 'package:gl_zoo/services/activity_service.dart';
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';

import 'feeding_detail.dart';

class FeedingPage extends StatefulWidget {
  @override
  _FeedingPageState createState() => _FeedingPageState();
}

class _FeedingPageState extends State<FeedingPage> with SingleTickerProviderStateMixin{
  TabController controller1;
  int needToLoad = 0;
  bool isLoading = false;
  bool isLoadingActivity = true;
  int totalCollection = 0;
  MyCollectionApi myCollections;
  List<Anima> collections = [];


  DateTime _selectedDate = DateTime.now();

  ActivityApi allActivity;
  ActivityTypeApi allActivityType;
  // ActivityApi selectedActivityTab = ActivityApi();
  ActivityApi filteredActivityTab = ActivityApi();
  List<String> typeList = [];
  String _menu;

  String publicLink = "http://www.glzoobackend.xyz/backend-gl-zoo/public/";
  // List<Widget> collections;
  // @override
  // void initState() {
  //   super.initState();
  //   controller1 = new TabController(vsync: this, length: 3);
  // }

  @override
  void dispose() {
    controller1.dispose();
    super.dispose();
  }

  _FeedingPageState() {
    needToLoad = 1;
    isLoading = true;
    _menu = "0";
    initializeDateFormatting("id_ID");
    
    controller1 = TabController(length: 2, vsync: this);
    typeList = ['Hari Ini', 'Minggu Ini'];
    // typeList.firstWhere(test)
    isLoadingActivity = true;
    filterActivities();
  }
  
  void filterActivities(){
    getFeeding("token", _menu).then((resp){
      setState(() {
        filteredActivityTab = resp;
        isLoadingActivity = false;
        needToLoad--;
        if(needToLoad == 0)isLoading = false;
      });
    });
    // setState(() {
    //   filteredActivityTab = ActivityApi.fromJson(selectedActivityTab.toJson());
    //   filteredActivityTab.data.removeWhere((a){
    //     bool remove = true;
    //     Duration diffStart = a.timeStart.difference(_selectedDate); 
    //     Duration diffEnd = a.timeEnd.difference(_selectedDate); 
    //     print("diffStart.inMilliseconds");
    //     print(diffStart.inHours);
    //     print("diffEnd.inMilliseconds");
    //     print(diffEnd.inDays);
    //     print(a.timeStart.day);
    //     print(_selectedDate.day);
    //     if(diffStart.inDays <= 0 && diffEnd.inDays >= 0){//If passed
    //       if(diffEnd.inDays == 0 && (a.timeEnd.day != _selectedDate.day)){
    //         remove = true;
    //       }else{
    //         remove = false;
    //       }
    //     }

    //     return remove;
    //   });
    // });
  }

  Widget getEvent(String title, String location, DateTime start, DateTime end, String imageUrl, [int id=0]){
    DateTime now = DateTime.now();
    String chip = "Berakhir";
    Color chipColor = Theme.of(context).primaryColor;
    Duration diffStart = start.difference(now);
    Duration diffEnd = end.difference(now);

    if(diffStart.inMilliseconds <= 0){
      if(diffEnd.inMilliseconds > 0){//Not End Yet
        chip = "Buka";
      }else{//Already End
        chip = "Berakhir";
        chipColor = Colors.grey[400];
      }
    }else{//Not Open Yet
      if(diffStart.inHours == 0){
        chip = "${(diffStart.inMinutes % 60) + 1} Menit Lagi";
      }else{
        chip = "${diffStart.inHours % 24} Jam Lagi";
      }
      
      chipColor = Colors.orangeAccent;
    }

    return GestureDetector(
      onTap: () => _itemTapped(id, chip, chipColor),
      child: Container(
        padding: EdgeInsets.fromLTRB(8,8,0,8),
        decoration: BoxDecoration(
          color: chip == "Berakhir" ? Colors.grey[300] : Colors.white,
          border: Border(
            bottom: BorderSide(color: Colors.grey[400]),
          ),
        ),
        width: globals.mw(context)*0.8,
      //   child: Column(
      //   crossAxisAlignment: CrossAxisAlignment.start,
      //   mainAxisAlignment: MainAxisAlignment.start,
      //   children: <Widget>[
      //   Text(title,
      //     softWrap: true,
      //     style: TextStyle(
      //       color: Colors.black,
      //       fontWeight: FontWeight.w600,
      //     ),
      //   ),
      //   Row(
      //     mainAxisAlignment: MainAxisAlignment.start,
      //     children: <Widget>[
      //       Chip(
      //         backgroundColor: chipColor,
      //         label: Text(chip),
      //         labelStyle: TextStyle(
      //           fontSize: 12,
      //           fontWeight: FontWeight.bold
      //         ),
      //       ),
      //       VerticalDivider(
      //         color: Colors.white,
      //       ),
      //       Text("Lokasi: $location \r\n${DateFormat('d MMMM HH:mm','id_ID').format(start)} s/d\r\n${DateFormat('d MMMM HH:mm','id_ID').format(end)}",
      //       //Text("$location (${start.hour}.${start.minute} - ${end.hour}.${end.minute})",
      //         style: TextStyle(
      //           fontSize: 12,
      //           fontWeight: FontWeight.normal,
      //           color: Colors.grey
      //         ),
      //       ),
      //     ],
      //   ),
      //   Divider()
      // ])
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                ClipRRect(
                  borderRadius: BorderRadius.circular(40),
                  child: CachedNetworkImage(
                    height: 75,
                    width: 75,
                    fit: BoxFit.fill,
                    imageUrl: publicLink + imageUrl,
                    placeholder: (context, url) => Container(
                      width: 75,
                      height: 75,
                      child: globals.isLoading(),
                    ),
                    errorWidget: (context, url, error){
                      print(error.toString());
                      return Container(
                        child: Image.asset('assets/images/broken_image.png',
                          height: 75,
                          width: 75,
                          colorBlendMode: BlendMode.saturation,
                          color: Colors.grey,
                        )
                      );
                    },
                  )
                ),
                SizedBox(
                  width: 10,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text(title,
                      softWrap: true,
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Text("Lokasi: $location \r\n",
                        //Text("$location (${start.hour}.${start.minute} - ${end.hour}.${end.minute})",
                          style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.normal,
                            color: Colors.grey
                          ),
                        ),
                      ],
                    ),

                    Container(
                      height: 25,
                      padding: EdgeInsets.symmetric(horizontal: 0),
                      child: Chip(
                        padding: EdgeInsets.symmetric(horizontal: 10),
                        backgroundColor: chipColor,
                        label: Text(chip),
                        labelStyle: TextStyle(
                          height: 0,
                          fontSize: 10,
                          fontWeight: FontWeight.bold
                        ),
                      ),
                    ),
                  ]
                ),
                Spacer(),
              ],
            ),
          ],
        ),
      )
    );
  }

  void _itemTapped(int id, String chip, Color chipColor){
    globals.detailFeedingId = id;
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (BuildContext context) => FeedingDetailPage(
          chip: chip,
          chipColor: chipColor,
      ))
    );
  }
  
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.grey[200],
        bottomNavigationBar: globals.bottomAppBar(context),
        floatingActionButton: globals.floatingAppBar(context),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        appBar: globals.appBar(context, "Feeding"),
        body: ListView(
          padding: EdgeInsets.all(0),
          children: <Widget>[
            //_cardCont("book_collection.png", 5, totalCollection),
            Container(
              margin: EdgeInsets.fromLTRB(10, 5, 10, 5),
              child: Card(
                elevation: 5,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: Column(
                  children: <Widget>[
                    Container(
                      height: 60,
                      padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
                      child:isLoading
                      ? Container(
                          child: globals.isLoading(),
                        )
                      : 
                      
                      TabBar(
                        //isScrollable: true,
                        indicator: ShapeDecoration(
                          color: Theme.of(context).primaryColor,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20.0),
                          ),
                        ),
                        indicatorColor: Theme.of(context).primaryColor,
                        indicatorWeight: 2.0,
                        indicatorSize: TabBarIndicatorSize.tab,
                        indicatorPadding: EdgeInsets.all(0),
                        unselectedLabelColor: Colors.grey[600],
                        labelColor: Colors.white,
                        controller: controller1,
                        
                        onTap: (idx) {
                          setState(() {
                            _menu = '$idx';
                            isLoadingActivity = true;
                          });
                          filterActivities();
                        },
                        tabs: List<Widget>.from(typeList.map((f){
                          return Text(f, textAlign: TextAlign.center);
                        })),
                      ),
                    ),
                    isLoadingActivity
                    ? Container(
                        padding: EdgeInsets.only(top: 10),
                        width: globals.mw(context),
                        height: 175,
                        child: globals.isLoading(),
                      )
                    :Container(
                      padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: filteredActivityTab.data.length>0? List<Widget>.from(
                          filteredActivityTab.data.map((a){
                            return getEvent(a.nama, a.location, a.timeStart, a.timeEnd, a.thumbnail, a.id);
                          })
                        ): <Widget>[Text("There's no Activity for this selection", style: TextStyle(
                          color: Colors.grey[600],
                          fontStyle: FontStyle.italic,
                        ),),
                          Divider(),
                        ],
                      ),
                    ),
                  ]
                )
              )
            ),
            
          ],
        ),
      ),
    );
  }
}
