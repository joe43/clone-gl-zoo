import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:gl_zoo/globals.dart' as globals;
import 'package:photo_view/photo_view.dart';

class GaleryDetailPage extends StatefulWidget {
  @override
  _GaleryDetailPageState createState() => _GaleryDetailPageState();
}

class _GaleryDetailPageState extends State<GaleryDetailPage> with SingleTickerProviderStateMixin{
  // TabController controller1;
  ImageProvider ip;
  

  _GaleryDetailPageState() {
    ip = globals.currentGaleryImage;
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Stack(
        children: <Widget>[
          Container(
            child: PhotoView(
              imageProvider: ip,
            )
          ),
          Positioned(
            top: 30,
            left: 5,
            child: IconButton(
              icon: Icon(Icons.arrow_back),
              color: Colors.white,
              onPressed: () {
                Navigator.pop(context);
              }
            ),
          ),
          Positioned(
            bottom: 50,
            child: Container(
              width: globals.mw(context),
                child: globals.myText(text: globals.galeryImageCaption ==""? "No Caption" : globals.galeryImageCaption, size: 12, color: "light", align: TextAlign.center),
            )
            
          ),
        ],
      ),
    );
    
    
  }
}
