import 'package:flutter/material.dart';
import 'package:gl_zoo/pages/activity_detail.dart';
import 'package:gl_zoo/pages/attraction.dart';
import 'package:gl_zoo/pages/collection.dart';
import 'package:gl_zoo/pages/collection_detail.dart';
import 'package:gl_zoo/pages/facility.dart';
import 'package:gl_zoo/pages/feeding.dart';
import 'package:gl_zoo/pages/feeding_detail.dart';
import 'package:gl_zoo/pages/galery.dart';
import 'package:gl_zoo/pages/galery_detail.dart';
import 'package:gl_zoo/pages/home.dart';
import 'package:gl_zoo/pages/intro.dart';
import 'package:gl_zoo/pages/jadwal.dart';
import 'package:gl_zoo/pages/kontak_kami.dart';
import 'package:gl_zoo/pages/map.dart';
import 'package:gl_zoo/pages/profile.dart';
import 'package:gl_zoo/pages/promo.dart';
import 'package:gl_zoo/pages/promo_detail.dart';
import 'package:gl_zoo/pages/reminder.dart';
import 'package:gl_zoo/pages/splash_app.dart';
import 'package:gl_zoo/pages/spotlight_detail.dart';
import 'package:gl_zoo/themes.dart';
import 'package:gl_zoo/globals.dart' as G;

import 'services/user_services.dart';

//void main() => runApp(MyApp());
void main() async {
  print("Main : " + G.isNewUser.toString());
  String x = await readLocalData("isNewUser");
  print(x);
  G.isNewUser = (x == "false") ? false: true;
  
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    // initDynamicLinks(context);
    print("MyApp : " + G.isNewUser.toString());
    //SystemChrome.setEnabledSystemUIOverlays([]);
    return MaterialApp(
      title: 'GL-Zoo',
      debugShowCheckedModeBanner: false,
      theme: buildThemeData(),
      //home: SplashPage(),
      initialRoute: '/',
      routes: <String, WidgetBuilder>{
        //Root Page
        '/': (BuildContext context) =>
            G.isNewUser? SplashPage() : HomePage(),
        // '/': (BuildContext context) => GaleryPage(),
        // '/home': (BuildContext context) => HomePage(),
        '/intro': (BuildContext context) => IntroPage(),
        '/galeri': (BuildContext context) => GaleryPage(),
        '/galeri/detail': (BuildContext context) => GaleryDetailPage(),
        '/kontak-kami': (BuildContext context) => KontakKamiPage(),
        '/jadwal': (BuildContext context) => CalendarPage(),
        '/event/detail': (BuildContext context) => SpotLightDetailPage(),
        '/koleksi': (BuildContext context) => CollectionPage(),
        '/koleksi/detail': (BuildContext context) => CollectionDetailPage(),
        '/atraksi': (BuildContext context) => AttractionPage(),
        '/atraksi/detail': (BuildContext context) => ActivityDetailPage(),
        '/feeding': (BuildContext context) => FeedingPage(),
        '/feeding/detail': (BuildContext context) => FeedingDetailPage(),
        '/facility': (BuildContext context) => FacilityPage(),
        '/promo': (BuildContext context) => PromoPage(),
        '/reminder': (BuildContext context) => ReminderPage(),
        '/promo/detail': (BuildContext context) => PromoDetailPage(),
        '/profile': (BuildContext context) => ProfilePage(),
        '/map': (BuildContext context) => MapPage('https://glzoobackend.xyz/frontend-gl-zoo/public/'),//For Dynamic Link Testing
      },
    );
  }
}
