// To parse this JSON data, do
//
//     final facilityApi = facilityApiFromJson(jsonString);

import 'dart:convert';

class FacilityApi {
    List<Facility> data;
    bool hasil;

    FacilityApi({
        this.data,
        this.hasil,
    });

    factory FacilityApi.fromJson(String str) => FacilityApi.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory FacilityApi.fromMap(Map<String, dynamic> json) => FacilityApi(
        data: json["data"] == null ? null : List<Facility>.from(json["data"].map((x) => Facility.fromMap(x))),
        hasil: json["hasil"] == null ? null : json["hasil"],
    );

    Map<String, dynamic> toMap() => {
        "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toMap())),
        "hasil": hasil == null ? null : hasil,
    };
}

class Facility {
    int id;
    String nama;
    String deskripsi;
    String timeStart;
    String timeEnd;
    String image;
    String thumbnail;
    String harga;
    String location;
    String createdAt;
    String updatedAt;
    String deletedAt;

    Facility({
        this.id,
        this.nama,
        this.deskripsi,
        this.timeStart,
        this.timeEnd,
        this.image,
        this.thumbnail,
        this.harga,
        this.location,
        this.createdAt,
        this.updatedAt,
        this.deletedAt,
    });

    factory Facility.fromJson(String str) => Facility.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory Facility.fromMap(Map<String, dynamic> json) => Facility(
        id: json["id"] == null ? null : json["id"],
        nama: json["nama"] == null ? null : json["nama"],
        deskripsi: json["deskripsi"] == null ? null : json["deskripsi"],
        timeStart: json["time_start"] == null ? null : json["time_start"],
        timeEnd: json["time_end"] == null ? null : json["time_end"],
        image: json["image"] == null ? null : json["image"],
        thumbnail: json["thumbnail"] == null ? null : json["thumbnail"],
        harga: json["harga"] == null ? null : json["harga"],
        location: json["location"] == null ? null : json["location"],
        createdAt: json["created_at"] == null ? null : json["created_at"],
        updatedAt: json["updated_at"],
        deletedAt: json["deleted_at"],
    );

    Map<String, dynamic> toMap() => {
        "id": id == null ? null : id,
        "nama": nama == null ? null : nama,
        "deskripsi": deskripsi == null ? null : deskripsi,
        "time_start": timeStart == null ? null : timeStart,
        "time_end": timeEnd == null ? null : timeEnd,
        "image": image == null ? null : image,
        "thumbnail": thumbnail == null ? null : thumbnail,
        "harga": harga == null ? null : harga,
        "location": location == null ? null : location,
        "created_at": createdAt == null ? null : createdAt,
        "updated_at": updatedAt,
        "deleted_at": deletedAt,
    };
}
