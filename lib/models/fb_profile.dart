import 'dart:convert';

class FbProfile {
    String name;
    Picture picture;
    String firstName;
    String lastName;
    String email;
    String id;

    FbProfile({
        this.name,
        this.picture,
        this.firstName,
        this.lastName,
        this.email,
        this.id,
    });

    factory FbProfile.fromJson(String str) => FbProfile.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory FbProfile.fromMap(Map<String, dynamic> json) => new FbProfile(
        name: json["name"] == null ? null : json["name"],
        picture: json["picture"] == null ? null : Picture.fromMap(json["picture"]),
        firstName: json["first_name"] == null ? null : json["first_name"],
        lastName: json["last_name"] == null ? null : json["last_name"],
        email: json["email"] == null ? null : json["email"],
        id: json["id"] == null ? null : json["id"],
    );

    Map<String, dynamic> toMap() => {
        "name": name == null ? null : name,
        "picture": picture == null ? null : picture.toMap(),
        "first_name": firstName == null ? null : firstName,
        "last_name": lastName == null ? null : lastName,
        "email": email == null ? null : email,
        "id": id == null ? null : id,
    };
}

class Picture {
    Data data;

    Picture({
        this.data,
    });

    factory Picture.fromJson(String str) => Picture.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory Picture.fromMap(Map<String, dynamic> json) => Picture(
        data: json["data"] == null ? null : Data.fromMap(json["data"]),
    );

    Map<String, dynamic> toMap() => {
        "data": data == null ? null : data.toMap(),
    };
}

class Data {
    int height;
    bool isSilhouette;
    String url;
    int width;

    Data({
        this.height,
        this.isSilhouette,
        this.url,
        this.width,
    });

    factory Data.fromJson(String str) => Data.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory Data.fromMap(Map<String, dynamic> json) => Data(
        height: json["height"] == null ? null : json["height"],
        isSilhouette: json["is_silhouette"] == null ? null : json["is_silhouette"],
        url: json["url"] == null ? null : json["url"],
        width: json["width"] == null ? null : json["width"],
    );

    Map<String, dynamic> toMap() => {
        "height": height == null ? null : height,
        "is_silhouette": isSilhouette == null ? null : isSilhouette,
        "url": url == null ? null : url,
        "width": width == null ? null : width,
    };
}