import 'dart:convert';

class SpotlightApi {
    int apiStatus;
    String apiMessage;
    String apiAuthorization;
    List<SpotLight> data;

    SpotlightApi({
        this.apiStatus,
        this.apiMessage,
        this.apiAuthorization,
        this.data,
    });

    factory SpotlightApi.fromJson(String str) => SpotlightApi.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory SpotlightApi.fromMap(Map<String, dynamic> json) => SpotlightApi(
        apiStatus: json["api_status"] == null ? null : json["api_status"],
        apiMessage: json["api_message"] == null ? null : json["api_message"],
        apiAuthorization: json["api_authorization"] == null ? null : json["api_authorization"],
        data: json["data"] == null ? null : List<SpotLight>.from(json["data"].map((x) => SpotLight.fromMap(x))),
    );

    Map<String, dynamic> toMap() => {
        "api_status": apiStatus == null ? null : apiStatus,
        "api_message": apiMessage == null ? null : apiMessage,
        "api_authorization": apiAuthorization == null ? null : apiAuthorization,
        "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toMap())),
    };
}

class SpotLight {
    int id;
    String nama;
    String deskripsi;
    String timeStart;
    String timeEnd;
    String image;
    String thumbnail;
    String harga;
    String location;

    SpotLight({
        this.id,
        this.nama,
        this.deskripsi,
        this.timeStart,
        this.timeEnd,
        this.image,
        this.thumbnail,
        this.harga,
        this.location,
    });

    factory SpotLight.fromJson(String str) => SpotLight.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory SpotLight.fromMap(Map<String, dynamic> json) => SpotLight(
        id: json["id"] == null ? null : json["id"],
        nama: json["nama"] == null ? null : json["nama"],
        deskripsi: json["deskripsi"] == null ? null : json["deskripsi"],
        timeStart: json["time_start"] == null ? null : json["time_start"],
        timeEnd: json["time_end"] == null ? null : json["time_end"],
        image: json["image"] == null ? null : json["image"],
        thumbnail: json["thumbnail"] == null ? null : json["thumbnail"],
        harga: json["harga"] == null ? null : json["harga"],
        location: json["location"] == null ? null : json["location"],
    );

    Map<String, dynamic> toMap() => {
        "id": id == null ? null : id,
        "nama": nama == null ? null : nama,
        "deskripsi": deskripsi == null ? null : deskripsi,
        "time_start": timeStart == null ? null : timeStart,
        "time_end": timeEnd == null ? null : timeEnd,
        "image": image == null ? null : image,
        "thumbnail": thumbnail == null ? null : thumbnail,
        "harga": harga == null ? null : harga,
        "location": location == null ? null : location,
    };
}