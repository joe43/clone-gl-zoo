// To parse this JSON data, do
//
//     final dailyPriceApi = dailyPriceApiFromJson(jsonString);

import 'dart:convert';

class DailyPriceApi {
    String result;
    List<DailyPrice> data;

    DailyPriceApi({
        this.result,
        this.data,
    });

    factory DailyPriceApi.fromJson(String str) => DailyPriceApi.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory DailyPriceApi.fromMap(Map<String, dynamic> json) => DailyPriceApi(
        result: json["result"] == null ? null : json["result"],
        data: json["data"] == null ? null : List<DailyPrice>.from(json["data"].map((x) => DailyPrice.fromMap(x))),
    );

    Map<String, dynamic> toMap() => {
        "result": result == null ? null : result,
        "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toMap())),
    };
}

class DailyPrice {
    int id;
    String createdAt;
    String updatedAt;
    String type;
    String notes;
    double price;
    double promoPrice;
    DateTime periodStart;
    DateTime periodEnd;
    String deletedAt;

    DailyPrice({
        this.id,
        this.createdAt,
        this.updatedAt,
        this.type,
        this.notes,
        this.price,
        this.promoPrice,
        this.periodStart,
        this.periodEnd,
        this.deletedAt,
    });

    factory DailyPrice.fromJson(String str) => DailyPrice.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory DailyPrice.fromMap(Map<String, dynamic> json) => DailyPrice(
        id: json["id"] == null ? null : json["id"],
        createdAt: json["created_at"] == null ? null : json["created_at"],
        updatedAt: json["updated_at"] == null ? null : json["updated_at"],
        type: json["type"] == null ? null : json["type"],
        notes: json["notes"] == null ? null : json["notes"],
        price: json["price"] == null ? null : double.parse(json["price"].toString()),
        promoPrice: json["promo_price"] == null ? null : double.parse(json["promo_price"].toString()),
        periodStart: json["period_start"] == null ? null : DateTime.parse(json["period_start"]),
        periodEnd: json["period_end"] == null ? null : DateTime.parse(json["period_end"]),
        deletedAt: json["deleted_at"],
    );

    Map<String, dynamic> toMap() => {
        "id": id == null ? null : id,
        "created_at": createdAt == null ? null : createdAt,
        "updated_at": updatedAt == null ? null : updatedAt,
        "type": type == null ? null : type,
        "notes": notes == null ? null : notes,
        "price": price == null ? null : price,
        "promo_price": promoPrice == null ? null : promoPrice,
        "period_start": periodStart == null ? null : periodStart.toIso8601String(),
        "period_end": periodEnd == null ? null : periodEnd.toIso8601String(),
        "deleted_at": deletedAt,
    };
}
