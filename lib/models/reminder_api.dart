// To parse this JSON data, do
//
//     final promoApi = promoApiFromJson(jsonString);

import 'dart:convert';

class ReminderApi {
    List<Reminder> data;
    List<Reminder> data2;
    bool hasil;

    ReminderApi({
        this.data,
        this.data2,
        this.hasil,
    });

    factory ReminderApi.fromJson(String str) => ReminderApi.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory ReminderApi.fromMap(Map<String, dynamic> json) => ReminderApi(
        data: json["data"] == null ? null : List<Reminder>.from(json["data"].map((x) => Reminder.fromMap(x))),
        data2: json["data2"] == null ? null : List<Reminder>.from(json["data2"].map((x) => Reminder.fromMap(x))),
        hasil: json["hasil"] == null ? null : json["hasil"],
    );

    Map<String, dynamic> toMap() => {
        "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toMap())),
        "data2": data2 == null ? null : List<dynamic>.from(data2.map((x) => x.toMap())),
        "hasil": hasil == null ? null : hasil,
    };
}

class Reminder {
    int id;
    String createdAt;
    dynamic updatedAt;
    String nama;
    String deskripsi;
    String image;
    String thumbnail;
    String harga;
    dynamic deletedAt;

    Reminder({
        this.id,
        this.createdAt,
        this.updatedAt,
        this.nama,
        this.deskripsi,
        this.image,
        this.thumbnail,
        this.harga,
        this.deletedAt,
    });

    factory Reminder.fromJson(String str) => Reminder.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory Reminder.fromMap(Map<String, dynamic> json) => Reminder(
        id: json["id"] == null ? null : json["id"],
        createdAt: json["created_at"] == null ? null : json["created_at"],
        updatedAt: json["updated_at"],
        nama: json["nama"] == null ? null : json["nama"],
        deskripsi: json["deskripsi"] == null ? null : json["deskripsi"],
        image: json["image"] == null ? null : json["image"],
        thumbnail: json["thumbnail"] == null ? null : json["thumbnail"],
        harga: json["harga"] == null ? null : json["harga"],
        deletedAt: json["deleted_at"],
    );

    Map<String, dynamic> toMap() => {
        "id": id == null ? null : id,
        "created_at": createdAt == null ? null : createdAt,
        "updated_at": updatedAt,
        "nama": nama == null ? null : nama,
        "deskripsi": deskripsi == null ? null : deskripsi,
        "image": image == null ? null : image,
        "thumbnail": thumbnail == null ? null : thumbnail,
        "harga": harga == null ? null : harga,
        "deleted_at": deletedAt,
    };
}
