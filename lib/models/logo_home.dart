// To parse this JSON data, do
//
//     final logoHome = logoHomeFromJson(jsonString);

import 'dart:convert';

LogoHome logoHomeFromJson(String str) => LogoHome.fromJson(json.decode(str));

String logoHomeToJson(LogoHome data) => json.encode(data.toJson());

class LogoHome {
    int apiStatus;
    String apiMessage;
    String apiAuthorization;
    List<Datum> data;

    LogoHome({
        this.apiStatus,
        this.apiMessage,
        this.apiAuthorization,
        this.data,
    });

    factory LogoHome.fromJson(Map<String, dynamic> json) => LogoHome(
        apiStatus: json["api_status"] == null ? null : json["api_status"],
        apiMessage: json["api_message"] == null ? null : json["api_message"],
        apiAuthorization: json["api_authorization"] == null ? null : json["api_authorization"],
        data: json["data"] == null ? null : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "api_status": apiStatus == null ? null : apiStatus,
        "api_message": apiMessage == null ? null : apiMessage,
        "api_authorization": apiAuthorization == null ? null : apiAuthorization,
        "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toJson())),
    };
}

class Datum {
    int id;
    String name;
    String text;
    String image;

    Datum({
        this.id,
        this.name,
        this.text,
        this.image,
    });

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        text: json["text"] == null ? null : json["text"],
        image: json["image"] == null ? null : json["image"],
    );

    Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "text": text == null ? null : text,
        "image": image == null ? null : image,
    };
}
