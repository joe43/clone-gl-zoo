import 'dart:convert';

class MenuApi {
    int apiStatus;
    String apiMessage;
    String apiAuthorization;
    List<Menu> data;

    MenuApi({
        this.apiStatus,
        this.apiMessage,
        this.apiAuthorization,
        this.data,
    });

    factory MenuApi.fromJson(String str) => MenuApi.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory MenuApi.fromMap(Map<String, dynamic> json) => MenuApi(
        apiStatus: json["api_status"] == null ? null : json["api_status"],
        apiMessage: json["api_message"] == null ? null : json["api_message"],
        apiAuthorization: json["api_authorization"] == null ? null : json["api_authorization"],
        data: json["data"] == null ? null : List<Menu>.from(json["data"].map((x) => Menu.fromMap(x))),
    );

    Map<String, dynamic> toMap() => {
        "api_status": apiStatus == null ? null : apiStatus,
        "api_message": apiMessage == null ? null : apiMessage,
        "api_authorization": apiAuthorization == null ? null : apiAuthorization,
        "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toMap())),
    };
}

class Menu {
    int id;
    String name;
    String text;
    String image;

    Menu({
        this.id,
        this.name,
        this.text,
        this.image,
    });

    factory Menu.fromJson(String str) => Menu.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory Menu.fromMap(Map<String, dynamic> json) => Menu(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        text: json["text"] == null ? null : json["text"],
        image: json["image"] == null ? null : json["image"],
    );

    Map<String, dynamic> toMap() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "text": text == null ? null : text,
        "image": image == null ? null : image,
    };
}