// To parse this JSON data, do
//
//     final promoApi = promoApiFromJson(jsonString);

import 'dart:convert';

class PromoApi {
    List<Promo> data;
    List<Promo> data2;
    bool hasil;

    PromoApi({
        this.data,
        this.data2,
        this.hasil,
    });

    factory PromoApi.fromJson(String str) => PromoApi.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory PromoApi.fromMap(Map<String, dynamic> json) => PromoApi(
        data: json["data"] == null ? null : List<Promo>.from(json["data"].map((x) => Promo.fromMap(x))),
        data2: json["data2"] == null ? null : List<Promo>.from(json["data2"].map((x) => Promo.fromMap(x))),
        hasil: json["hasil"] == null ? null : json["hasil"],
    );

    Map<String, dynamic> toMap() => {
        "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toMap())),
        "data2": data2 == null ? null : List<dynamic>.from(data2.map((x) => x.toMap())),
        "hasil": hasil == null ? null : hasil,
    };
}

class Promo {
    int id;
    String createdAt;
    dynamic updatedAt;
    String nama;
    String deskripsi;
    String image;
    String thumbnail;
    String harga;
    dynamic deletedAt;

    Promo({
        this.id,
        this.createdAt,
        this.updatedAt,
        this.nama,
        this.deskripsi,
        this.image,
        this.thumbnail,
        this.harga,
        this.deletedAt,
    });

    factory Promo.fromJson(String str) => Promo.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory Promo.fromMap(Map<String, dynamic> json) => Promo(
        id: json["id"] == null ? null : json["id"],
        createdAt: json["created_at"] == null ? null : json["created_at"],
        updatedAt: json["updated_at"],
        nama: json["nama"] == null ? null : json["nama"],
        deskripsi: json["deskripsi"] == null ? null : json["deskripsi"],
        image: json["image"] == null ? null : json["image"],
        thumbnail: json["thumbnail"] == null ? null : json["thumbnail"],
        harga: json["harga"] == null ? null : json["harga"],
        deletedAt: json["deleted_at"],
    );

    Map<String, dynamic> toMap() => {
        "id": id == null ? null : id,
        "created_at": createdAt == null ? null : createdAt,
        "updated_at": updatedAt,
        "nama": nama == null ? null : nama,
        "deskripsi": deskripsi == null ? null : deskripsi,
        "image": image == null ? null : image,
        "thumbnail": thumbnail == null ? null : thumbnail,
        "harga": harga == null ? null : harga,
        "deleted_at": deletedAt,
    };
}
