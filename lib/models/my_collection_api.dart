// To parse this JSON dataarray, do
//
//     final myCollectionApi = myCollectionApiFromJson(jsonString);

import 'dart:convert';

class MyCollectionApi {
    List<Anima> animas;
    Data dataarray;
    String result;

    MyCollectionApi({
        this.animas,
        this.dataarray,
        this.result,
    });

    factory MyCollectionApi.fromJson(String str) => MyCollectionApi.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory MyCollectionApi.fromMap(Map<String, dynamic> json) => MyCollectionApi(
        animas: json["dataarraynongroup"] == null ? [] : List<Anima>.from(json["dataarraynongroup"].map((x) => Anima.fromMap(x))),
        // animas: json["dataarraynongroup"] == null ? [] : List<Anima>.from(json["dataarraynongroup"].map((x) => Anima.fromJson(x))),
        dataarray: json["dataarray"] == null ? null : Data.fromMap(json["dataarray"]),
        result: json["result"] == null ? null : json["result"],
    );

    Map<String, dynamic> toMap() => {
        "dataarraynongroup": animas == null ? null : List<dynamic>.from(animas.map((x) => x.toMap())),
        "dataarray": dataarray == null ? null : dataarray.toMap(),
        "result": result == null ? null : result,
    };
}

class Data {
    List<Anima> unggas;
    List<Anima> hewanBuas;
    List<Anima> hewanPengerat;
    List<Anima> hewanLaut;
    List<Anima> reptile;

    Data({
        this.unggas,
        this.hewanBuas,
        this.hewanPengerat,
        this.hewanLaut,
        this.reptile,
    });

    factory Data.fromJson(String str) => Data.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory Data.fromMap(Map<String, dynamic> json) => Data(
        unggas: json["Unggas"] == null ? [] : List<Anima>.from(json["Unggas"].map((x) => Anima.fromMap(x))),
        hewanBuas: json["hewan buas"] == null ? [] : List<Anima>.from(json["hewan buas"].map((x) => Anima.fromMap(x))),
        hewanPengerat: json["hewan pengerat"] == null ? [] : List<Anima>.from(json["hewan pengerat"].map((x) => Anima.fromMap(x))),
        hewanLaut: json["hewan laut"] == null ? [] : List<Anima>.from(json["hewan laut"].map((x) => Anima.fromMap(x))),
        reptile: json["Reptile"] == null ? [] : List<Anima>.from(json["Reptile"].map((x) => Anima.fromMap(x))),
    );

    Map<String, dynamic> toMap() => {
        "Unggas": unggas == null || unggas == [] ? [] : List<dynamic>.from(unggas.map((x) => x.toMap())),
        "hewan buas": hewanBuas == null || hewanBuas == [] ? [] : List<dynamic>.from(hewanBuas.map((x) => x.toMap())),
        "hewan pengerat": hewanPengerat == null || hewanPengerat == [] ? [] : List<dynamic>.from(hewanPengerat.map((x) => x.toMap())),
        "hewan laut": hewanLaut == null || hewanLaut == [] ? [] : List<dynamic>.from(hewanPengerat.map((x) => x.toMap())),
        "Reptile": reptile == null ||reptile == [] ? [] : List<dynamic>.from(reptile.map((x) => x.toMap())),
    };
}

class Anima {
    int id;
    int collected;
    String nama;
    String categoryname;
    String thumbnail;
    int animaid;

    Anima({
        this.id,
        this.collected,
        this.nama,
        this.categoryname,
        this.thumbnail,
        this.animaid,
    });

    factory Anima.fromJson(String str) => Anima.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory Anima.fromMap(Map<String, dynamic> json) => Anima(
        id: json["id"] == null ? null : json["id"],
        collected: json["collected"] == null ? null : json["collected"],
        nama: json["nama"] == null ? null : json["nama"],
        categoryname: json["categoryname"] == null ? null : json["categoryname"],
        thumbnail: json["thumbnail"] == null ? null : json["thumbnail"],
        animaid: json["animaid"] == null ? null : json["animaid"],
    );

    Map<String, dynamic> toMap() => {
        "id": id == null ? null : id,
        "collected": collected == null ? null : collected,
        "nama": nama == null ? null : nama,
        "categoryname": categoryname == null ? null : categoryname,
        "thumbnail": thumbnail == null ? null : thumbnail,
        "animaid": animaid == null ? null : animaid,
    };
}
