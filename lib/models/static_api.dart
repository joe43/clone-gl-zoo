import 'dart:convert';

class StaticApi {
    int apiStatus;
    String apiMessage;
    String apiAuthorization;
    List<HomeData> data;

    StaticApi({
        this.apiStatus,
        this.apiMessage,
        this.apiAuthorization,
        this.data,
    });

    factory StaticApi.fromJson(String str) => StaticApi.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory StaticApi.fromMap(Map<String, dynamic> json) => StaticApi(
        apiStatus: json["api_status"] == null ? null : json["api_status"],
        apiMessage: json["api_message"] == null ? null : json["api_message"],
        apiAuthorization: json["api_authorization"] == null ? null : json["api_authorization"],
        data: json["data"] == null ? null : List<HomeData>.from(json["data"].map((x) => HomeData.fromMap(x))),
    );

    Map<String, dynamic> toMap() => {
        "api_status": apiStatus == null ? null : apiStatus,
        "api_message": apiMessage == null ? null : apiMessage,
        "api_authorization": apiAuthorization == null ? null : apiAuthorization,
        "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toMap())),
    };
}

class HomeData {
    int id;
    String background;
    String logo;
    String address;
    String phone;
    String email;
    String facebook;
    String instagram;
    String whatsapp;
    String openHour;
    String closeHour;
    String pass;
    String lat;
    String lng;

    HomeData({
        this.id,
        this.background,
        this.logo,
        this.address,
        this.phone,
        this.email,
        this.facebook,
        this.instagram,
        this.whatsapp,
        this.openHour,
        this.closeHour,
        this.pass,
        this.lat,
        this.lng,
    });

    factory HomeData.fromJson(String str) => HomeData.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory HomeData.fromMap(Map<String, dynamic> json) => HomeData(
        id: json["id"] == null ? null : json["id"],
        background: json["background"] == null ? null : json["background"],
        logo: json["logo"] == null ? null : json["logo"],
        address: json["address"] == null ? null : json["address"],
        phone: json["phone"] == null ? null : json["phone"],
        email: json["email"] == null ? null : json["email"],
        facebook: json["facebook"] == null ? null : json["facebook"],
        instagram: json["instagram"] == null ? null : json["instagram"],
        whatsapp: json["whatsapp"] == null ? null : json["whatsapp"],
        openHour: json["open_hour"] == null ? null : json["open_hour"],
        closeHour: json["close_hour"] == null ? null : json["close_hour"],
        pass: json["pass"] == null ? null : json["pass"],
        lat: json["lat"] == null ? null : json["lat"],
        lng: json["lng"] == null ? null : json["lng"],
    );

    Map<String, dynamic> toMap() => {
        "id": id == null ? null : id,
        "background": background == null ? null : background,
        "logo": logo == null ? null : logo,
        "address": address == null ? null : address,
        "phone": phone == null ? null : phone,
        "email": email == null ? null : email,
        "facebook": facebook == null ? null : facebook,
        "instagram": instagram == null ? null : instagram,
        "whatsapp": whatsapp == null ? null : whatsapp,
        "open_hour": openHour == null ? null : openHour,
        "close_hour": closeHour == null ? null : closeHour,
        "pass": pass == null ? null : pass,
        "lat": lat == null ? null : lat,
        "lng": lng == null ? null : lng,
    };
}