// To parse this JSON data, do
//
//     final sliderApi = sliderApiFromJson(jsonString);

import 'dart:convert';

class SliderApi {
    int apiStatus;
    String apiMessage;
    String apiAuthorization;
    List<IntroSlider> data;

    SliderApi({
        this.apiStatus,
        this.apiMessage,
        this.apiAuthorization,
        this.data,
    });

    factory SliderApi.fromJson(String str) => SliderApi.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory SliderApi.fromMap(Map<String, dynamic> json) => SliderApi(
        apiStatus: json["api_status"] == null ? null : json["api_status"],
        apiMessage: json["api_message"] == null ? null : json["api_message"],
        apiAuthorization: json["api_authorization"] == null ? null : json["api_authorization"],
        data: json["data"] == null ? null : List<IntroSlider>.from(json["data"].map((x) => IntroSlider.fromMap(x))),
    );

    Map<String, dynamic> toMap() => {
        "api_status": apiStatus == null ? null : apiStatus,
        "api_message": apiMessage == null ? null : apiMessage,
        "api_authorization": apiAuthorization == null ? null : apiAuthorization,
        "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toMap())),
    };
}

class IntroSlider {
    int id;
    String image;
    String caption;
    String title;

    IntroSlider({
        this.id,
        this.image,
        this.caption,
        this.title,
    });

    factory IntroSlider.fromJson(String str) => IntroSlider.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory IntroSlider.fromMap(Map<String, dynamic> json) => IntroSlider(
        id: json["id"] == null ? null : json["id"],
        image: json["image"] == null ? null : json["image"],
        caption: json["caption"] == null ? null : json["caption"],
        title: json["title"] == null ? null : json["title"],
    );

    Map<String, dynamic> toMap() => {
        "id": id == null ? null : id,
        "image": image == null ? null : image,
        "caption": caption == null ? null : caption,
        "title": title == null ? null : title,
    };
}
