// To parse this JSON data, do
//
//     final activityTypeApi = activityTypeApiFromJson(jsonString);

import 'dart:convert';

class ActivityTypeApi {
    String result;
    List<ActivityType> data;

    ActivityTypeApi({
        this.result,
        this.data,
    });

    factory ActivityTypeApi.fromJson(String str) => ActivityTypeApi.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory ActivityTypeApi.fromMap(Map<String, dynamic> json) => ActivityTypeApi(
        result: json["result"] == null ? null : json["result"],
        data: json["data"] == null ? null : List<ActivityType>.from(json["data"].map((x) => ActivityType.fromMap(x))),
    );

    Map<String, dynamic> toMap() => {
        "result": result == null ? null : result,
        "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toMap())),
    };
}

class ActivityType {
    int id;
    String nama;
    String createdAt;
    String updatedAt;
    String deletedAt;

    ActivityType({
        this.id,
        this.nama,
        this.createdAt,
        this.updatedAt,
        this.deletedAt,
    });

    factory ActivityType.fromJson(String str) => ActivityType.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory ActivityType.fromMap(Map<String, dynamic> json) => ActivityType(
        id: json["id"] == null ? null : json["id"],
        nama: json["nama"] == null ? null : json["nama"],
        createdAt: json["created_at"] == null ? null : json["created_at"],
        updatedAt: json["updated_at"] == null ? null : json["created_at"],
        deletedAt: json["deleted_at"] == null ? null : json["created_at"],
    );

    Map<String, dynamic> toMap() => {
        "id": id == null ? null : id,
        "nama": nama == null ? null : nama,
        "created_at": createdAt == null ? null : createdAt,
        "updated_at": updatedAt == null ? null : createdAt,
        "deleted_at": deletedAt == null ? null : createdAt,
    };
}
