// To parse this JSON data, do
//
//     final wrapper = wrapperFromJson(jsonString);

import 'dart:convert';

Wrapper wrapperFromJson(String str) => Wrapper.fromJson(json.decode(str));

String wrapperToJson(Wrapper data) => json.encode(data.toJson());

class Wrapper {
    int apiStatus;
    String apiMessage;
    String apiAuthorization;
    String data;

    Wrapper({
        this.apiStatus,
        this.apiMessage,
        this.apiAuthorization,
        this.data,
    });

    factory Wrapper.fromJson(Map<String, dynamic> json) => new Wrapper(
        apiStatus: json["api_status"] == null ? null : json["api_status"],
        apiMessage: json["api_message"] == null ? null : json["api_message"],
        apiAuthorization: json["api_authorization"] == null ? null : json["api_authorization"],
        data: json["data"] == null ? null : json["data"].toString(),
    );

    Map<String, dynamic> toJson() => {
        "api_status": apiStatus == null ? null : apiStatus,
        "api_message": apiMessage == null ? null : apiMessage,
        "api_authorization": apiAuthorization == null ? null : apiAuthorization,
        "data": data == null ? null : data,
    };
}
