// To parse this JSON data, do
//
//     final galeryApi = galeryApiFromJson(jsonString);

import 'dart:convert';

class GaleryApi {
    List<Galery> data;
    bool hasil;

    GaleryApi({
        this.data,
        this.hasil,
    });

    factory GaleryApi.fromJson(String str) => GaleryApi.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory GaleryApi.fromMap(Map<String, dynamic> json) => GaleryApi(
        data: json["data"] == null ? null : List<Galery>.from(json["data"].map((x) => Galery.fromMap(x))),
        hasil: json["hasil"] == null ? null : json["hasil"],
    );

    Map<String, dynamic> toMap() => {
        "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toMap())),
        "hasil": hasil == null ? null : hasil,
    };
}

class Galery {
    int id;
    String createdAt;
    String updatedAt;
    String nama;
    String deskripsi;
    String banner;
    String image;
    String harga;
    String deletedAt;

    Galery({
        this.id,
        this.createdAt,
        this.updatedAt,
        this.nama,
        this.deskripsi,
        this.banner,
        this.image,
        this.harga,
        this.deletedAt,
    });

    factory Galery.fromJson(String str) => Galery.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory Galery.fromMap(Map<String, dynamic> json) => Galery(
        id: json["id"] == null ? null : json["id"],
        createdAt: json["created_at"] == null ? null : json["created_at"],
        updatedAt: json["updated_at"] == null ? null : json["updated_at"],
        nama: json["nama"] == null ? null : json["nama"],
        deskripsi: json["deskripsi"] == null ? null : json["deskripsi"],
        banner: json["banner"] == null ? null : json["banner"],
        image: json["image"] == null ? null : json["image"],
        harga: json["harga"] == null ? null : json["harga"],
        deletedAt: json["deleted_at"] == null ? null : json["deleted_at"],
    );

    Map<String, dynamic> toMap() => {
        "id": id == null ? null : id,
        "created_at": createdAt == null ? null : createdAt,
        "updated_at": updatedAt == null ? null : updatedAt,
        "nama": nama == null ? null : nama,
        "deskripsi": deskripsi == null ? null : deskripsi,
        "banner": banner == null ? null : banner,
        "image": image == null ? null : image,
        "harga": harga == null ? null : harga,
        "deleted_at": deletedAt == null ? null : deletedAt,
    };
}
