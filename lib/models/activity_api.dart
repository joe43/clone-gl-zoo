// To parse this JSON data, do
//
//     final activityApi = activityApiFromJson(jsonString);

import 'dart:convert';

class ActivityApi {
    String result;
    List<Activity> data;

    ActivityApi({
        this.result,
        this.data,
    });

    factory ActivityApi.fromJson(String str) => ActivityApi.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory ActivityApi.fromMap(Map<String, dynamic> json) => ActivityApi(
        result: json["result"] == null ? null : json["result"],
        data: json["data"] == null ? null : List<Activity>.from(json["data"].map((x) => Activity.fromMap(x))),
    );

    Map<String, dynamic> toMap() => {
        "result": result == null ? null : result,
        "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toMap())),
    };
}

class Activity {
    int id;
    String nama;
    String deskripsi;
    DateTime timeStart;
    DateTime timeEnd;
    String image;
    String thumbnail;
    double harga;
    String location;
    String createdAt;
    String updatedAt;
    String deletedAt;
    String type;
    String days;
    String namagroup;
    String activitytype;

    Activity({
        this.id,
        this.nama,
        this.deskripsi,
        this.timeStart,
        this.timeEnd,
        this.image,
        this.thumbnail,
        this.harga,
        this.location,
        this.createdAt,
        this.updatedAt,
        this.deletedAt,
        this.type,
        this.days,
        this.namagroup,
        this.activitytype,
    });

    factory Activity.fromJson(String str) => Activity.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory Activity.fromMap(Map<String, dynamic> json) => Activity(
        id: json["id"] == null ? null : json["id"],
        nama: json["nama"] == null ? null : json["nama"],
        deskripsi: json["deskripsi"] == null ? null : json["deskripsi"],
        timeStart: json["time_start"] == null ? null : DateTime.parse(json["time_start"]),
        timeEnd: json["time_end"] == null ? null : DateTime.parse(json["time_end"]),
        image: json["image"] == null ? null : json["image"],
        thumbnail: json["thumbnail"] == null ? null : json["thumbnail"],
        harga: json["harga"] == null ? null : double.parse(json["harga"]),
        location: json["location"] == null ? null : json["location"],
        createdAt: json["created_at"] == null ? null : json["created_at"],
        updatedAt: json["updated_at"] == null ? null : json["updated_at"],
        deletedAt: json["deleted_at"],
        type: json["type"] == null ? null : json["type"],
        days: json["days"] == null ? null : json["days"],
        namagroup: json["namagroup"] == null ? null : json["namagroup"],
        activitytype: json["activitytype"] == null ? null : json["activitytype"],
    );

    Map<String, dynamic> toMap() => {
        "id": id == null ? null : id,
        "nama": nama == null ? null : nama,
        "deskripsi": deskripsi == null ? null : deskripsi,
        "time_start": timeStart == null ? null : timeStart.toIso8601String(),
        "time_end": timeEnd == null ? null : timeEnd.toIso8601String(),
        "image": image == null ? null : image,
        "thumbnail": thumbnail == null ? null : thumbnail,
        "harga": harga == null ? null : harga.toStringAsFixed(2),
        "location": location == null ? null : location,
        "created_at": createdAt == null ? null : createdAt,
        "updated_at": updatedAt == null ? null : updatedAt,
        "deleted_at": deletedAt,
        "type": type == null ? null : type,
        "days": days == null ? null : days,
        "namagroup": namagroup == null ? null : namagroup,
        "activitytype": activitytype == null ? null : activitytype,
    };
}
