// To parse this JSON data, do
//
//     final userApi = userApiFromJson(jsonString);

import 'dart:convert';

class UserApi {
    User data;
    String result;

    UserApi({
        this.data,
        this.result,
    });

    factory UserApi.fromJson(String str) => UserApi.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory UserApi.fromMap(Map<String, dynamic> json) => UserApi(
        data: json["data"] == null ? null : User.fromMap(json["data"]),
        result: json["result"] == null ? null : json["result"],
    );

    Map<String, dynamic> toMap() => {
        "data": data == null ? null : data.toMap(),
        "result": result == null ? null : result,
    };
}

class User {
    int id;
    String name;
    String email;
    String gender;
    String description;
    String username;
    String password;
    String phoneNumber;
    String address;
    String photo;
    int regencyId;
    int blacklisted;
    String firebaseToken;
    dynamic deletedAt;
    String createdAt;
    dynamic updatedAt;
    String rememberToken;
    int collected;
    dynamic point;
    int calory;
    int meter;
    dynamic timeinminutes;

    User({
        this.id,
        this.name,
        this.email,
        this.gender,
        this.description,
        this.username,
        this.password,
        this.phoneNumber,
        this.address,
        this.photo,
        this.regencyId,
        this.blacklisted,
        this.firebaseToken,
        this.deletedAt,
        this.createdAt,
        this.updatedAt,
        this.rememberToken,
        this.collected,
        this.point,
        this.calory,
        this.meter,
        this.timeinminutes,
    });

    factory User.fromJson(String str) => User.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory User.fromMap(Map<String, dynamic> json) => User(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        email: json["email"] == null ? null : json["email"],
        gender: json["gender"] == null ? null : json["gender"],
        description: json["description"] == null ? null : json["description"],
        username: json["username"] == null ? null : json["username"],
        password: json["password"] == null ? null : json["password"],
        phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
        address: json["address"] == null ? null : json["address"],
        photo: json["photo"] == null ? null : json["photo"],
        regencyId: json["regency_id"] == null ? null : json["regency_id"],
        blacklisted: json["blacklisted"] == null ? null : json["blacklisted"],
        firebaseToken: json["firebase_token"] == null ? null : json["firebase_token"],
        deletedAt: json["deleted_at"],
        createdAt: json["created_at"] == null ? null : json["created_at"],
        updatedAt: json["updated_at"],
        rememberToken: json["remember_token"] == null ? null : json["remember_token"],
        collected: json["collected"] == null ? null : json["collected"],
        point: json["point"],
        calory: json["calory"] == null ? null : json["calory"],
        meter: json["meter"] == null ? null : json["meter"],
        timeinminutes: json["timeinminutes"],
    );

    Map<String, dynamic> toMap() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "email": email == null ? null : email,
        "gender": gender == null ? null : gender,
        "description": description == null ? null : description,
        "username": username == null ? null : username,
        "password": password == null ? null : password,
        "phone_number": phoneNumber == null ? null : phoneNumber,
        "address": address == null ? null : address,
        "photo": photo == null ? null : photo,
        "regency_id": regencyId == null ? null : regencyId,
        "blacklisted": blacklisted == null ? null : blacklisted,
        "firebase_token": firebaseToken == null ? null : firebaseToken,
        "deleted_at": deletedAt,
        "created_at": createdAt == null ? null : createdAt,
        "updated_at": updatedAt,
        "remember_token": rememberToken == null ? null : rememberToken,
        "collected": collected == null ? null : collected,
        "point": point,
        "calory": calory == null ? null : calory,
        "meter": meter == null ? null : meter,
        "timeinminutes": timeinminutes,
    };
}
