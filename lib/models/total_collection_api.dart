// To parse this JSON data, do
//
//     final totalCollectionApi = totalCollectionApiFromJson(jsonString);

import 'dart:convert';

class TotalCollectionApi {
    int data;
    bool hasil;

    TotalCollectionApi({
        this.data,
        this.hasil,
    });

    factory TotalCollectionApi.fromJson(String str) => TotalCollectionApi.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory TotalCollectionApi.fromMap(Map<String, dynamic> json) => TotalCollectionApi(
        data: json["data"] == null ? null : json["data"],
        hasil: json["hasil"] == null ? null : json["hasil"],
    );

    Map<String, dynamic> toMap() => {
        "data": data == null ? null : data,
        "hasil": hasil == null ? null : hasil,
    };
}
