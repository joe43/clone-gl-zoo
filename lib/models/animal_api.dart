// To parse this JSON data, do
//
//     final animalApi = animalApiFromJson(jsonString);

import 'dart:convert';

class AnimalApi {
    Animal data;
    bool hasil;

    AnimalApi({
        this.data,
        this.hasil,
    });

    factory AnimalApi.fromJson(String str) => AnimalApi.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory AnimalApi.fromMap(Map<String, dynamic> json) => AnimalApi(
        data: json["data"] == null ? null : Animal.fromMap(json["data"]),
        hasil: json["hasil"] == null ? null : json["hasil"],
    );

    Map<String, dynamic> toMap() => {
        "data": data == null ? null : data.toMap(),
        "hasil": hasil == null ? null : hasil,
    };
}

class Animal {
    int id;
    String nama;
    String deskripsi;
    String fakta;
    String status;
    String image;
    String thumbnail;
    String keterangan;
    String parameter;
    String location;
    int animalsCategoriesId;
    String createdAt;
    String updatedAt;
    String deletedAt;
    String slug;

    Animal({
        this.id,
        this.nama,
        this.deskripsi,
        this.fakta,
        this.status,
        this.image,
        this.thumbnail,
        this.keterangan,
        this.parameter,
        this.location,
        this.animalsCategoriesId,
        this.createdAt,
        this.updatedAt,
        this.deletedAt,
        this.slug,
    });

    factory Animal.fromJson(String str) => Animal.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory Animal.fromMap(Map<String, dynamic> json) => Animal(
        id: json["id"] == null ? null : json["id"],
        nama: json["nama"] == null ? null : json["nama"],
        deskripsi: json["deskripsi"] == null ? null : json["deskripsi"],
        fakta: json["fakta"] == null ? null : json["fakta"],
        status: json["status"] == null ? null : json["status"],
        image: json["image"] == null ? null : json["image"],
        thumbnail: json["thumbnail"] == null ? null : json["thumbnail"],
        keterangan: json["keterangan"] == null ? null : json["keterangan"],
        parameter: json["parameter"] == null ? null : json["parameter"],
        location: json["location"] == null ? null : json["location"],
        animalsCategoriesId: json["animals_categories_id"] == null ? null : json["animals_categories_id"],
        createdAt: json["created_at"] == null ? null : json["created_at"],
        updatedAt: json["updated_at"] == null ? null : json["updated_at"],
        deletedAt: json["deleted_at"] == null ? null : json["deleted_at"],
        slug: json["slug"] == null ? null : json["slug"],
    );

    Map<String, dynamic> toMap() => {
        "id": id == null ? null : id,
        "nama": nama == null ? null : nama,
        "deskripsi": deskripsi == null ? null : deskripsi,
        "fakta": fakta == null ? null : fakta,
        "status": status == null ? null : status,
        "image": image == null ? null : image,
        "thumbnail": thumbnail == null ? null : thumbnail,
        "keterangan": keterangan == null ? null : keterangan,
        "parameter": parameter == null ? null : parameter,
        "location": location == null ? null : location,
        "animals_categories_id": animalsCategoriesId == null ? null : animalsCategoriesId,
        "created_at": createdAt == null ? null : createdAt,
        "updated_at": updatedAt == null ? null : updatedAt,
        "deleted_at": deletedAt == null ? null : deletedAt,
        "slug": slug == null ? null : slug,
    };
}
