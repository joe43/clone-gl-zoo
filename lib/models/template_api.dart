// To parse this JSON data, do
//
//     final totalCollectionApi = totalCollectionApiFromJson(jsonString);

import 'dart:convert';

class TemplateApi {
    int data;
    bool hasil;

    TemplateApi({
        this.data,
        this.hasil,
    });

    factory TemplateApi.fromJson(String str) => TemplateApi.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory TemplateApi.fromMap(Map<String, dynamic> json) => TemplateApi(
        data: json["data"] == null ? null : json["data"],
        hasil: json["hasil"] == null ? null : json["hasil"],
    );

    Map<String, dynamic> toMap() => {
        "data": data == null ? null : data,
        "hasil": hasil == null ? null : hasil,
    };
}
